#!/bin/sh

edddyDir=$(
  cd $(dirname "$0")
  pwd
)

sudo apt-get update
sudo apt-get install --no-upgrade make cmake build-essential libc-dev-bin libx11-dev libxrandr-dev libasound2-dev libegl1-mesa-dev libwayland-dev wayland-protocols qt5-default qtbase5-dev qt5-qmake

git submodule update --init --recursive
cd Dry/
./script/cmake_generic.sh . -DDRY_TOOLS=0 -DDRY_SAMPLES=0 -DDRY_PLAYER=0 -DDRY_2D=0 -DDRY_IK=0 -DDRY_NETWORK=0 -DDRY_NAVIGATION=0
make
cd -

if [ ! -d ../Edddy-build ]
then
    mkdir ../Edddy-build/
fi

cd ../Edddy-build/
qmake "$edddyDir"/Edddy.pro -spec linux-g++ && make

if [ ! -d EditorResources ]
then
    ln -s "$edddyDir"/EditorResources/
fi
if [ ! -d Data ]
then
    ln -s "$edddyDir"/Dry/bin/Data/
fi
if [ ! -d CoreData ]
then
    ln -s "$edddyDir"/Dry/bin/CoreData/
fi

./edddy

