lessThan(QT_VERSION, 4.2) {
    error("Edddy requires Qt 4.2 or greater")
}

include(src/Edddy.pri)

TARGET = edddy

QT += core gui widgets

LIBS += \
    $$(DRY_HOME)/lib/libDry.a \
    -lpthread \
    -ldl \
    -lGL

QMAKE_CXXFLAGS += -std=c++20 -O2

INCLUDEPATH += \
    $$(DRY_HOME)/include \
    $$(DRY_HOME)/include/Dry/ThirdParty \

TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle

RESOURCES += \
    resources.qrc

#INSTALLS += \
    #binary
    #mime types
    #icons

unix
{
    isEmpty(DATADIR) DATADIR = $$(XDG_DATA_HOME)
    isEmpty(DATADIR) DATADIR = $$(HOME)/.local/share

    target.path = /usr/bin/
    INSTALLS += target

    resources.path = $$DATADIR/luckey/edddy/
    resources.files = EditorResources/*
    INSTALLS += resources

    icon.path = $$DATADIR/icons/
    icon.files = edddy.svg
    INSTALLS += icon

    desktop.path = $$(HOME)/.local/share/applications/
    desktop.files = edddy.desktop
    INSTALLS += desktop

    DEFINES += RESOURCEPATH=\\\"$${resources.path}\\\"
}
