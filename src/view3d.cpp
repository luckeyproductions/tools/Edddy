/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "view3d.h"
#include "viewsplitter.h"

#include "mainwindow.h"
#include <QDebug>
#include <QWheelEvent>
#include <QPainter>
#include <QComboBox>
#include <QGraphicsEffect>
#include <QLayout>
#include "blockmap/blockmap.h"
#include "docks/blockspanel.h"
#include "edddycursor.h"
#include "edddycam.h"
#include "editmaster.h"
#include "inputmaster.h"
#include "mastercontrol.h"

bool View3D::simulatedMiddleMouse_{ false };
bool View3D::flyMode_{ false };
Vector<View3D*> View3D::views_{};
View3D* View3D::active_{};
IntVector3 View3D::keyPan_{};

View3D::View3D(Context* context): QWidget(), Object(context),
    toPan_{},
    wheelAccumulator_{},
    mapBox_{},
    previousMousePos_{ width() / 2, height() / 2 },
    renderTexture_{},
    image_{},
    cameraNode_{},
    edddyCam_{},
    blockMap_{}
{
    setMinimumSize(80, 45);
    setFocusPolicy(Qt::StrongFocus);
    setMouseTracking(true);

    mapBox_ = new QComboBox{ this };
    mapBox_->move(1, 2);
    mapBox_->setSizeAdjustPolicy(QComboBox::AdjustToContents);
    mapBox_->setVisible(false);

    QGraphicsOpacityEffect* transparent{ new QGraphicsOpacityEffect{ mapBox_ } };
    transparent->setOpacity(.8);
    mapBox_->setGraphicsEffect(transparent);

    connect(mapBox_, SIGNAL(currentIndexChanged(int)), this, SLOT(SetMap(int)));

    SubscribeToEvent(E_BEGINFRAME, DRY_HANDLER(View3D, CreateCamera));
    SubscribeToEvent(E_CURSORSTEP, DRY_HANDLER(View3D, HandleCursorStep));
    SubscribeToEvent(E_CURRENTBLOCKCHANGED, DRY_HANDLER(View3D, HandleCursorStep));

    views_.Push(this);

    if (!active_)
        active_ = this;

    pixmap_ = { width(), height() };
    pixmap_.fill(Qt::transparent);
}

View3D::~View3D()
{
    views_.Remove(this);

    if (IsActive())
    {
        if (!views_.IsEmpty())
            active_ = views_.Back();
        else
            active_ = nullptr;
    }
}

void View3D::UpdateMapBox()
{
    mapBox_->blockSignals(true);
    mapBox_->clear();
    int i{ 0 };
    int index{ 0 };

    for (BlockMap* bm: EM->GetBlockMaps())
    {
        mapBox_->addItem(QIcon{ ":/Map" }, bm->displayName(), { QMetaType::QObjectStar, &bm });

        if (bm == blockMap_)
            index = i;

        ++i;
    }

    if (mapBox_->count())
    {
        mapBox_->blockSignals(false);
        mapBox_->updateGeometry();
        mapBox_->setCurrentIndex(index);
        BlockMap* bm{ qobject_cast<BlockMap*>(qvariant_cast<QObject*>(mapBox_->currentData(Qt::UserRole))) };

        if (blockMap_ != bm)
            SetMap(bm);

        if (mapBox_->isVisible() == false)
            mapBox_->setVisible(true);
    }
    else
    {
        mapBox_->setVisible(false);
        SetMap(nullptr);
    }
}

void View3D::SetMap(int index)
{
    SetMap(EM->GetBlockMap(index));

    Activate();
}

void View3D::SetMap(BlockMap* blockMap)
{
    if (blockMap_ == blockMap)
        return;

    if (blockMap_ && edddyCam_)
        cameraTransforms_[blockMap_] = edddyCam_->GetNode()->GetWorldTransform();

    blockMap_ = blockMap;

    if (blockMap_ == nullptr)
        return;

    Scene* mapScene{ blockMap_->GetScene() };

    if (mapScene == nullptr || edddyCam_->GetScene() == mapScene)
        return;

    edddyCam_->HandleMapChange(blockMap_);
    if (cameraTransforms_.contains(blockMap_))
        edddyCam_->GetNode()->SetWorldTransform(cameraTransforms_.at(blockMap_));
    else
        resetCameraPosition();

    UpdateViewport();

    if (IsActive())
        EM->SetActiveBlockMap(blockMap_);

    Activate();

    for (int i{ 0 }; i < mapBox_->count(); ++i)
    {
        if (qobject_cast<BlockMap*>(qvariant_cast<QObject*>(mapBox_->itemData(i))) == blockMap_)
        {
            mapBox_->setCurrentIndex(i);
            break;
        }
    }
}

void View3D::CreateCamera(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    CreateCamera();
}

void View3D::CreateCamera()
{
    cameraNode_ = new Node{ context_ };
    edddyCam_ = cameraNode_->CreateComponent<EdddyCam>();
    edddyCam_->SetView(this);

    CreateRenderTexture();

    UnsubscribeFromEvent(E_BEGINFRAME);
}

void View3D::resetCameraPosition()
{
    edddyCam_->resetPosition(blockMap_);
    UpdateViewport();
}

void View3D::setCameraTransform(const Matrix3x4& transform)
{
    edddyCam_->GetNode()->SetWorldTransform(transform);
}

void View3D::CreateRenderTexture()
{
    if (renderTexture_)
        renderTexture_->GetRenderSurface()->Release();

    renderTexture_ = new Texture2D{ context_ };
    renderTexture_->SetSize(width(), height(), Graphics::GetRGBAFormat(), TEXTURE_RENDERTARGET);
    renderTexture_->GetRenderSurface()->SetUpdateMode(SURFACE_MANUALUPDATE);

    UpdateViewport();
}

void View3D::UpdateViewport()
{
    if (!blockMap_)
        return;

    RenderSurface* renderSurface{ renderTexture_->GetRenderSurface() };

    if (renderSurface->GetViewport(0))
    {
        renderSurface->GetViewport(0)->SetScene(blockMap_->GetScene());
    }
    else
    {
        SharedPtr<Viewport> viewport{ new Viewport{ context_, blockMap_->GetScene(), edddyCam_ }};
        renderSurface->SetViewport(0, viewport);
    }

    UpdateView();
}

void View3D::HandleCursorStep(const StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    if (EdddyCursor::cursor_->GetBlockMap() == blockMap_)
        UpdateView();
}

void View3D::UpdateView(StringHash, VariantMap&)
{
    UpdateView();
}

void View3D::UpdateView()
{
    if (!renderTexture_)
        return;

    renderTexture_->GetRenderSurface()->QueueUpdate();
    MC->RequestUpdate();

    SubscribeToEvent(E_ENDRENDERING, DRY_HANDLER(View3D, PaintView));
}

void View3D::resizeEvent(QResizeEvent* /*event*/)
{
    MasterControl* masterControl{ MC };
    if (!masterControl)
        return;

    CreateRenderTexture();
}

void View3D::paintEvent(QPaintEvent* /*event*/)
{
    if (blockMap_ == nullptr)
        return;

    PaintView();
    PaintBorder();

    if ((IsActive() && keyPan_ != IntVector3::ZERO) || !Equals(toPan_.LengthSquared(), 0.f))
        Pan();
}

void View3D::Pan()
{
    const float t{ Min( 1.f, TIME->GetTimeStep()) };
    const float m{ (QGuiApplication::keyboardModifiers() & Qt::ShiftModifier ? 1.7f : .42f) };
    const Vector3 pan{ Min(1.f, 2e1f * t) * toPan_.x_,
                       Min(1.f, 3e1f * t) * toPan_.y_,
                       Min(1.f, 2e1f * t) * toPan_.z_ };
    edddyCam_->Move({ pan.x_, pan.y_, pan.z_ }, (flyMode_ ? MT_KEYPAN : MT_MOUSEPAN));
    toPan_ -= pan;
    toPan_ = Lerp(toPan_, Vector3{ keyPan_ }.NormalizedOrDefault() * m, t);
    if (toPan_.LengthSquared() < M_EPSILON)
        toPan_ = Vector3::ZERO;

    UpdateView();
}

void View3D::PaintView(const StringHash, VariantMap&)
{
    repaint();
}

void View3D::PaintView()
{
    UpdatePixmap();

    QPainter p{ this };

    int drawWidth{ static_cast<int>(ceil(pixmap_.width() * (static_cast<float>(height()) / pixmap_.height()))) };
    int dX{ width() - drawWidth };

   if (blockMap_)
   {
       if (dX > 0)
           p.fillRect(rect(), blockMap_->GetBackgroundQColor());

       p.drawPixmap(QRect{ dX / 2, 0, drawWidth, height() }, pixmap_);
   }
}

void View3D::UpdatePixmap()
{
    if (!blockMap_)
        return;

    Image* image{ renderTexture_->GetImage() };
    if (!image || renderTexture_->GetRenderSurface()->IsUpdateQueued())
        return;

    if (image_ != image)
    {
        image_ = renderTexture_->GetImage();
        pixmap_ = ImageToPixmap(image_);

        UnsubscribeFromEvent(E_ENDRENDERING);
    }
}

void View3D::PaintBorder()
{
    if (views_.Size() == 1u || !IsActive())
        return;

    QPainter p{ this };

    p.setCompositionMode(QPainter::CompositionMode_Difference);
    p.setRenderHint(QPainter::Antialiasing);
    QPen pen{ QColor{ 32, 160, 192, 192 } };
    pen.setWidth(7);
    p.setPen(pen);
    p.drawRoundedRect(-1, -1, width() + 2, height() + 2, 6.66, 6.66);

    p.setCompositionMode(QPainter::CompositionMode_SourceOver);
    pen.setWidth(6);
    p.setPen(pen);
    p.drawRoundedRect(-1, -1, width() + 2, height() + 2, 6.66, 6.66);
}

void View3D::Activate()
{
    if (IsActive())
        return;

    View3D* lastActive{ active_ };

    active_ = this;
    EM->SetActiveBlockMap(blockMap_);

    repaint();

    //Remove border from previously active view
    if (lastActive)
        lastActive->repaint();
}

void View3D::Split()
{
    if (views_.Size() >= 5)
        return;

    View3D* newView{ new View3D{ context_ } };
    QWidget* parent{ parentWidget() };
    ViewSplitter* splitter{ new ViewSplitter{} };
    splitter->setParent(parentWidget());

    ViewSplitter* parentSplitter{ qobject_cast<ViewSplitter*>(parent) };
    QList<int> sizes{};

    if (parentSplitter)
    {
        sizes = parentSplitter->sizes();
        parentSplitter->insertWidget(parentSplitter->indexOf(this), splitter);
    }
    else
    {
        parent->layout()->addWidget(splitter);
    }

    splitter->addWidget(newView);
    splitter->addWidget(this);

    if (parentSplitter)
        parentSplitter->setSizes(sizes);

    splitter->show();
    this->show();

    newView->CreateCamera();
    newView->UpdateMapBox();
    newView->SetMap(blockMap_);
    newView->setCameraTransform(edddyCam_->GetNode()->GetWorldTransform());
}

void View3D::EnableFlyMode()
{
    flyMode_ = true;
    setFocus(Qt::ShortcutFocusReason);
    grabMouse();
    keyPan_ = IntVector3::ZERO;
    toPan_ = Vector3::ZERO;
    setCursor(Qt::BlankCursor);
    QCursor::setPos(mapToGlobal(QPoint{ width() / 2, height() / 2 }));
    previousMousePos_ = QCursor::pos();
}

void View3D::mouseMoveEvent(QMouseEvent* event)
{
    if (!flyMode_)
    {
        if (blockMap_)
            setFocus();
    }
    else if (!IsActive())
    {
        active_->setFocus(Qt::OtherFocusReason);
        return;
    }

    EdddyCursor* cursor{ EdddyCursor::cursor_ };

    const Qt::KeyboardModifiers modifiers{ event->modifiers() };
    const Vector2 posNormalized{ static_cast<float>(event->pos().x()) / (width()  - 1),
                                 static_cast<float>(event->pos().y()) / (height() - 1)};
    const QPoint dPos{ QCursor::pos() - previousMousePos_ };
    const Vector2 dVec{ dPos.x() * .00125f,
                        dPos.y() * .00125f };

    cursor->SetBlockMap(blockMap_);

    if (!blockMap_)
        return;

    if (edddyCam_)
        cursor->SetMouseRay(edddyCam_->GetScreenRay(posNormalized));
    else
        return;

    if (flyMode_)
    {
        Node* camNode{ edddyCam_->GetNode() };
        camNode->Yaw(dVec.x_ * 55);
        camNode->Pitch(edddyCam_->ClampedPitch(dVec.y_ * 42));
        camNode->LookAt(camNode->GetWorldPosition() + camNode->GetWorldDirection());
        cursor->HandleMouseMove();
        QCursor::setPos(mapToGlobal(QPoint{ width() / 2, height() / 2 }));
        UpdateView();
    }
    else if (QApplication::mouseButtons() & Qt::MiddleButton || simulatedMiddleMouse_)
    {
        Activate();

        //Change field of view
        if (modifiers & Qt::ShiftModifier && modifiers & Qt::ControlModifier)
            edddyCam_->Move(Vector3::ONE * dVec.y_, MT_FOV);
        //Pan camera
        else if (modifiers & Qt::ShiftModifier)
            edddyCam_->Move({ dVec.x_, dVec.y_, .0f }, MT_MOUSEPAN);
        //Zoom camera
        else if (modifiers & Qt::ControlModifier)
            edddyCam_->Move(Vector3::FORWARD * -dVec.y_, MT_MOUSEPAN);
        //Rotate camera
        else
            edddyCam_->Move({ dVec.x_, dVec.y_, .0f }, MT_ROTATE);

        //Wrap cursor
        if (event->pos().x() <= 0)
            QCursor::setPos(QCursor::pos() + QPoint(width() - 2, 0));
        else if (event->pos().x() >= width() - 1)
            QCursor::setPos(QCursor::pos() - QPoint(width() - 2, 0));
        else if (event->pos().y() <= 0)
            QCursor::setPos(QCursor::pos() + QPoint(0, height() - 2));
        else if (event->pos().y() >= height() - 1)
            QCursor::setPos(QCursor::pos() - QPoint(0, height() - 2));
    }
    else
    {
        cursor->HandleMouseMove();
    }

    if (!simulatedMiddleMouse_)
    {
        if (QApplication::mouseButtons() & Qt::RightButton)
        {
            Activate();
            EM->PickBlock();
        }
        else if (QApplication::mouseButtons() & Qt::LeftButton)
        {
            Activate();
            EM->ApplyTool(modifiers & Qt::ShiftModifier,
                          modifiers & Qt::ControlModifier,
                          modifiers & Qt::AltModifier);
        }
    }

    previousMousePos_ = QCursor::pos();
}

void View3D::mousePressEvent(QMouseEvent* event)
{
    Qt::KeyboardModifiers modifiers{ event->modifiers() };

    if (event->buttons() & Qt::RightButton)
    {
        Activate();

        if (modifiers & Qt::AltModifier)
            simulatedMiddleMouse_ = true;
        else
            EM->PickBlock();
    }
    else if (event->buttons() & Qt::LeftButton)
    {
        Activate();
        EM->ApplyTool(modifiers & Qt::ShiftModifier,
                      modifiers & Qt::ControlModifier,
                      modifiers & Qt::AltModifier);
    }
    else if (event->buttons() & Qt::MiddleButton)
    {
        Activate();
    }
}

void View3D::mouseReleaseEvent(QMouseEvent* event)
{
    if (event->button() == Qt::RightButton)
        simulatedMiddleMouse_ = false;

    EM->ReleaseTool();
}

void View3D::keyPressEvent(QKeyEvent* event)
{
    if (event->key() == Qt::Key_Control ||
        event->key() == Qt::Key_Alt ||
        event->key() == Qt::Key_Shift)
    {
        GetSubsystem<InputMaster>()->UpdateModifierKeys(event->modifiers());
    }

    if (flyMode_)
    {
        IntVector3 oldPan{ keyPan_ };
        switch (event->key())
        {
        case Qt::Key_A: keyPan_.x_ = Min(keyPan_.x_ + 1,  1); break;
        case Qt::Key_D: keyPan_.x_ = Max(keyPan_.x_ - 1, -1); break;
        case Qt::Key_E: keyPan_.y_ = Min(keyPan_.y_ + 1,  1); break;
        case Qt::Key_Q: keyPan_.y_ = Max(keyPan_.y_ - 1, -1); break;
        case Qt::Key_W: keyPan_.z_ = Min(keyPan_.z_ + 1,  1); break;
        case Qt::Key_S: keyPan_.z_ = Max(keyPan_.z_ - 1, -1); break;
        default: break;
        }

        if (oldPan == IntVector3::ZERO && keyPan_ != IntVector3::ZERO)
            Pan();
    }
}

void View3D::keyReleaseEvent(QKeyEvent* event)
{
    if (event->key() == Qt::Key_Control ||
        event->key() == Qt::Key_Alt ||
        event->key() == Qt::Key_Shift)
    {
        GetSubsystem<InputMaster>()->UpdateModifierKeys(event->modifiers());
    }

    if (flyMode_)
    {
        switch (event->key())
        {
        case Qt::Key_A: keyPan_.x_ = Max(keyPan_.x_ - 1, 0); break;
        case Qt::Key_D: keyPan_.x_ = Min(keyPan_.x_ + 1, 0); break;
        case Qt::Key_E: keyPan_.y_ = Max(keyPan_.y_ - 1, 0); break;
        case Qt::Key_Q: keyPan_.y_ = Min(keyPan_.y_ + 1, 0); break;
        case Qt::Key_W: keyPan_.z_ = Max(keyPan_.z_ - 1, 0); break;
        case Qt::Key_S: keyPan_.z_ = Min(keyPan_.z_ + 1, 0); break;
        default: break;
        }
    }
}

void View3D::wheelEvent(QWheelEvent* event)
{
    QWheelEvent* wheelEvent{ static_cast<QWheelEvent*>(event) };
    Qt::KeyboardModifiers modifiers{ wheelEvent->modifiers() };
    const int wheel{ (modifiers & Qt::AltModifier ? wheelEvent->angleDelta().x()
                                                  : wheelEvent->angleDelta().y()) };

    if (wheel != 0)
    {
        wheelAccumulator_ += wheel;
        const int step{ 235 };
        const int w{ wheelAccumulator_ };
        if (Abs(wheelAccumulator_) >= step)
            wheelAccumulator_ = 0;

        EdddyCursor* cursor{ EdddyCursor::cursor_ };

        if (modifiers & Qt::ControlModifier)
        {
            if (w >= step)
                cursor->Rotate(true);
            else if (w <= -step)
                cursor->Rotate(false);
        }
        else if (modifiers & Qt::AltModifier)
        {
            if (w >= step)
                EM->NextBlock();
            else if (w <= -step)
                EM->PreviousBlock();
        }
        else
        {
            if (w >= step)
                cursor->Step(IntVector3::FORWARD);
            else if (w <= -step)
                cursor->Step(IntVector3::BACK);
        }
    }
}
