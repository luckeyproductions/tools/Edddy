/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef PROJECT_H
#define PROJECT_H

#include "luckey.h"

#include <QMessageBox>

using MapAttributes = Vector<Pair<String, Variant>>;

class ModifiedBox: public QMessageBox, public Object
{
    Q_OBJECT
    DRY_OBJECT(ModifiedBox, Object)

public:
    ModifiedBox(Context* context, QWidget* parent = nullptr);
};

class Project: public QObject, public Serializable
{
    Q_OBJECT
    DRY_OBJECT(Project, Serializable)

public:
    explicit Project(Context* context);

    static void RegisterObject(Context* context);
    bool Load(const String& fileName);
    bool LoadXML(const XMLElement& source) override;

    bool Save();
    bool SaveXML(XMLElement& dest) const override;
    void saveSettings() const;

    bool SaveDefaultAttributes() const override { return true; }

    String fullPath();
    QString trimmedName() const;
    QString displayName() const;
    String resourceFolder(bool absolute = false) const;
    String command() const;
    QStringList& hiddenFolders() { return hiddenFolders_; }

    bool setDefaultMapSize(const IntVector3& size)
    {
        if (defaultMapSize_ == size)
            return false;

        defaultMapSize_ = size;
        unsavedChanges_ = true;
        return true;
    }

    bool setDefaultBlockSize(const Vector3& size)
    {
        if (defaultBlockSize_ == size)
            return false;

        defaultBlockSize_ = size;
        unsavedChanges_ = true;
        return true;
    }

    bool setMapAttributes(const MapAttributes& attributes)
    {
        if (mapAttributes_ == attributes)
            return false;

        mapAttributes_ = attributes;
        unsavedChanges_ = true;

        emit defaultMapAttributesChanged();
        return true;
    }

    const IntVector3& defaultMapSize() const { return defaultMapSize_; }
    const Vector3& defaultBlockSize()  const { return defaultBlockSize_; }
    const MapAttributes& mapAttributes() const { return mapAttributes_; }
    bool isValidMapAttribute(const Pair<String, Variant>& attribute)
    {
        const String& name{ attribute.first_ };
        const VariantType type{ attribute.second_.GetType() };

        for (Pair<String, Variant> a: mapAttributes_)
        {
            if (name == a.first_ && type == a.second_.GetType())
                return true;
        }

        return false;
    }

    String location() const;
    void OnResourcesFolderSet();
    bool Close();
    bool isModified() const;
    bool Unsaved() const { return unsavedChanges_; }

    bool inResourceFolder(const String& fileName) const;
    String localizedPath(const String& path) const;

signals:
    void defaultMapAttributesChanged();

private:
    void OnFolderSet();
    void OnDisplayNameSet();
    void OnCommandSet();

    void saveMapAttributes(XMLElement& dest) const;
    void loadMapAttributes(const XMLElement& source);
    void saveSettings(XMLElement& dest) const;

    String displayName_;
    String fileName_;
    String location_;
    String previousResourceFolder_;
    String resourceFolder_;
    String command_;
    QStringList hiddenFolders_;

    IntVector3 defaultMapSize_;
    Vector3 defaultBlockSize_;
    MapAttributes mapAttributes_;

    bool unsavedChanges_;
};

#endif // PROJECT_H
