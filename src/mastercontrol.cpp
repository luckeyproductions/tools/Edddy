/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "mastercontrol.h"
#include "mainwindow.h"

#include <QThread>
#include <QTimer>
#include <QPainter>
#include <QDebug>
#include <QSettings>
#include <QDesktopWidget>

#include "cyberplasm/cyberplasm.h"
#include "inputmaster.h"
#include "castmaster.h"
#include "effectmaster.h"
#include "editmaster.h"
#include "project.h"

#include "view3d.h"
#include "centers.h"
#include "edddycam.h"
#include "edddycursor.h"

#include "blockmap/blockmap.h"
#include "blockmap/block.h"
#include "blockmap/tileinstance.h"
#include "blockmap/freeblock.h"

MasterControl::MasterControl(int& argc, char** argv, Context* context): QApplication(argc, argv), Application(context),
    argument_{},
    mainWindow_{ nullptr },
    project_{ nullptr },
    requireUpdate_{ true },
    drawDebug_{ false }
{
    QCoreApplication::setOrganizationName("LucKey Productions");
    QCoreApplication::setOrganizationDomain("luckey.games");
    QCoreApplication::setApplicationName("Edddy");

    if (argc == 2)
    {
        const QString argument{ argv[1] };

        if (argument.endsWith(".edy", Qt::CaseInsensitive) ||
            argument.endsWith(".emp", Qt::CaseInsensitive) ||
            argument.endsWith(".ebs", Qt::CaseInsensitive) )
            argument_ = argument.toStdString().data();
    }

    Project::RegisterObject(context_);
    context_->RegisterFactory<EdddyCam>();
    context_->RegisterFactory<EdddyCursor>();
    context_->RegisterFactory<BlockMap>();
    context_->RegisterFactory<GridBlock>();
    context_->RegisterFactory<FreeBlock>();
    context_->RegisterFactory<TileInstance>();
    context_->RegisterFactory<Centers>();
}

MasterControl::~MasterControl()
{
}

void MasterControl::Setup()
{
    //Determine resource path
    String resourcePath{ "EditorResources" };
    if (!FS->DirExists(AddTrailingSlash(FS->GetProgramDir()) + resourcePath))
    {
        const String installedResources{ RemoveTrailingSlash(RESOURCEPATH) };
        if (FS->DirExists(installedResources))
            resourcePath = installedResources;
    }

    engineParameters_[EP_WINDOW_TITLE] = "Edddy";
    engineParameters_[EP_WINDOW_ICON] = "icon.png";
    engineParameters_[EP_LOG_NAME] = FS->GetAppPreferencesDir("luckey", "edddy") + "edddy.log";
    engineParameters_[EP_EXTERNAL_WINDOW] = (void*)(new QWidget())->winId();
    engineParameters_[EP_WORKER_THREADS] = true;
    engineParameters_[EP_RESOURCE_PATHS] = resourcePath;
}

void MasterControl::Start()
{
    GetSubsystem<Engine>()->SetTimeStepSmoothing(1);

    QTimer* timer{ new QTimer{ this } };
    connect(timer, SIGNAL(timeout()), this, SLOT(OnTimeout()));
    timer->start(1000 / GetSubsystem<Graphics>()->GetRefreshRate());
    exec();
    engine_->Exit();
}

void MasterControl::OnTimeout()
{
    if (!mainWindow_)
    {
        SetRandomSeed(TIME->GetSystemTime());

        context_->RegisterSubsystem(this);
        context_->RegisterSubsystem<InputMaster>();
        context_->RegisterSubsystem<CastMaster>();
        context_->RegisterSubsystem<EffectMaster>();
        context_->RegisterSubsystem<Cyberplasm>();

        mainWindow_ = new MainWindow{ context_ };
        context_->RegisterSubsystem<EditMaster>();
        mainWindow_->initialize();

        GetSubsystem<ResourceCache>()->SetAutoReloadResources(true);

        if (!argument_.IsEmpty())
        {
            if (argument_.Contains(".edy", false))
            {
                OpenProject(argument_);
            }
            else
            {
                for (String path{ FS->GetCurrentDir() };
                     GetParentPath(path) != path;
                     path = GetParentPath(path))
                {
                    Log::Write(LOG_INFO, path);
                    StringVector results{};
                    FS->ScanDir(results, path, "*.edy", SCAN_FILES, false);
                    if (results.Size())
                    {
                        const String stripped{ FS->GetCurrentDir().Replaced(path, "") };
                        FS->SetCurrentDir(path);
                        OpenProject(results.Front());
                        break;
                    }
                }
            }
        }
    }

    if (!engine_ || !requireUpdate_ || mainWindow_->isMinimized() ||
        engine_->IsExiting())
        return;

    RunFrame();
}

void MasterControl::RunFrame()
{
    requireUpdate_ = false;
    engine_->RunFrame();

//    qDebug() << TIME->GetTimeStamp().CString() << " Ran frame";
}

void MasterControl::NewProject(QString path)
{
    if (path.isEmpty())
        return;

    if (!path.endsWith(".edy", Qt::CaseInsensitive))
        path += ".edy";

#if QT_VERSION < QT_VERSION_CHECK(5, 15, 0)
    QString projectName{ path.split('/', QString::SkipEmptyParts).last() };
#else
    QString projectName{ path.split('/', Qt::SkipEmptyParts).last() };
#endif

    path.chop(projectName.length());

    Project* newProject{ new Project{ context_ } };
    newProject->SetAttribute("Location", QD(path));
    newProject->SetAttribute("FileName", QD(projectName));
    newProject->SetAttribute("DisplayName", QD(newProject->trimmedName()) );

    project_ = newProject;
    mainWindow_->handleCurrentProjectChanged();
}

void MasterControl::OpenProject(String path)
{
    if (path.IsEmpty() || GetExtension(path) != ".edy")
        return;

    if (path.Front() != '/')
        path = FS->GetCurrentDir() + path;

    Log::Write(LOG_INFO, path);

    const String projectName{ path.Split('/').Back() };
    path.Resize(path.Length() - projectName.Length());

    Project* newProject{ new Project{ context_ } };
    newProject->SetAttribute("Location", path);
    newProject->SetAttribute("FileName", projectName);

    if (!newProject->Load(path + projectName))
        return;

    if (project_)
        project_->Close();

    project_ = newProject;
    mainWindow_->handleCurrentProjectChanged();

    // Update recent files
    path += projectName;
    UpdateRecentProjects(path.CString());
}

void MasterControl::SaveCurrentProject()
{
    if (CurrentProject()->Save())
        UpdateRecentProjects(CurrentProject()->fullPath().CString());
}

void MasterControl::UpdateRecentProjects(const QString& latest)
{
    QSettings settings{};
    QStringList recentFiles{ settings.value("recentprojects").toStringList() };

    if (recentFiles.contains(latest))
    {
        for (int r{ 0 }; r < recentFiles.size(); ++r)
        {
            if (latest == recentFiles.at(r))
                recentFiles.removeAt(r);
        }
    }

    recentFiles.push_front(latest);

    while (recentFiles.size() > 10)
        recentFiles.pop_back();

    settings.setValue("recentprojects", recentFiles);
}

void MasterControl::Stop()
{
//    engine_->DumpResources(true);
}

bool MasterControl::Quit()
{
    if (!project_ || project_->Close())
        return true;
    else
        return false;
}

String MasterControl::trimmedResourceName(const String& fileName)
{
    if (!project_->inResourceFolder(fileName))
        return "";
    else
        return fileName.Replaced(containingFolder(fileName), "");
}

String MasterControl::containingFolder(const String& path)
{
    if (project_)
    {
        const String absoluteResourceFolder{ project_->resourceFolder(true) };
        if (path.Contains(absoluteResourceFolder))
            return absoluteResourceFolder;
    }

    return "";
}
