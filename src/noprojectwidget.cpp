/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QSpacerItem>
#include <QFileInfo>
#include <QSettings>
#include "mainwindow.h"

#include "noprojectwidget.h"

NoProjectWidget::NoProjectWidget(Context* context, QWidget* parent): QWidget(parent), Object(context),
    mainLayout_{ new QVBoxLayout{} }, // Main layout \\\ Should be scroll area
    logo_{ nullptr },
    recentHeader_{ new QLabel{ "Recent Projects" }, new QFrame{} },
    recentButtons_{}
{
    QVBoxLayout* rootLayout{ new QVBoxLayout{} }; // Has main frame as only direct child
    rootLayout->setContentsMargins(0, 2, 0, 0);
    QFrame* mainFrame{ new QFrame{ this } }; // Sunken frame around main layout
    mainFrame->setFrameShape(QFrame::Panel);
    mainFrame->setFrameShadow(QFrame::Sunken);
    mainFrame->setLineWidth(1);
    rootLayout->addWidget(mainFrame);
    setLayout(rootLayout);

    mainLayout_->setContentsMargins(64, 8, 64, 8);
    mainLayout_->setSpacing(3);
    mainFrame->setLayout(mainLayout_);

    logo_ = new QLabel{};
    const QPixmap logoPixmap{ ":/Edddy" };
    logo_->setPixmap(logoPixmap);
    logo_->setAlignment(Qt::AlignHCenter);
    logo_->setMinimumHeight(64);
    logo_->setMaximumHeight(logoPixmap.size().shrunkBy(logo_->contentsMargins()).height());
    logo_->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    mainLayout_->addWidget(logo_);

    mainLayout_->addSpacing(8);

    for (bool first: { true, false }) // Add New Project and Open Project buttons
    {
        const QString name{ (first ? "New" : "Open") };
        QPushButton* button{ new QPushButton{ QIcon{ ":/" + name }, name + " Project..." } };
        button->setFixedWidth(172);
        mainLayout_->addWidget(button);
        mainLayout_->setAlignment(button, Qt::AlignCenter);

        if (first)
            connect(button, SIGNAL(clicked(bool)), this, SIGNAL(newProject()));
        else
            connect(button, SIGNAL(clicked(bool)), this, SIGNAL(openProject()));
    }

    mainLayout_->addSpacing(6);
    createRecentHeader();
    mainLayout_->addSpacerItem(new QSpacerItem{ 0, 0, QSizePolicy::Expanding, QSizePolicy::Expanding });
}

void NoProjectWidget::createRecentHeader()
{
    recentHeader_.first->setEnabled(false);
    recentHeader_.first->setAlignment(Qt::AlignHCenter);
    mainLayout_->addWidget(recentHeader_.first);

    recentHeader_.second->setFrameShape(QFrame::HLine);
    recentHeader_.second->setFrameShadow(QFrame::Sunken);
    mainLayout_->addWidget(recentHeader_.second);
}

void NoProjectWidget::updateRecentList()
{
    const QSettings settings{};
    const QStringList recentProjects{ settings.value("recentprojects").toStringList() };
    const bool anyRecent{ !recentProjects.isEmpty() };

    for (QPushButton* b: recentButtons_)
        b->deleteLater();

    recentButtons_.clear();

    recentHeader_.first->setVisible(anyRecent);
    recentHeader_.second->setVisible(anyRecent);

    for (const QString& fileName: recentProjects)
    {
        if (fileName.isEmpty())
            continue;

        SharedPtr<XMLFile> projectFile{ GetSubsystem<ResourceCache>()
                    ->GetResource<XMLFile>(QD(fileName), false) };

        if (projectFile.IsNull() || projectFile->GetRoot("project").IsNull() )
            continue;

        QString projectName{ fileName.split('/').back().split('.').front() };
        const XMLElement rootElem{ projectFile->GetRoot("project") };
        XMLElement attrElem{ rootElem.GetChild("attribute") };
        while (attrElem)
        {
            if (attrElem.GetAttribute("name") == "DisplayName")
            {
                projectName = DQ(attrElem.GetAttribute("value"));
                break;
            }

            attrElem = attrElem.GetNext("attribute");
        }

        QPushButton* recentButton{ createRecentProjectButton(fileName, projectName) };
        mainLayout_->insertWidget(mainLayout_->count() - 1, recentButton);
        recentButtons_.push_back(recentButton);

        connect(recentButton, SIGNAL(clicked(bool)), MC, SLOT(OpenRecentProject()));
    }
}

QPushButton* NoProjectWidget::createRecentProjectButton(const QString& fileName, const QString& projectName) const
{
    QPushButton* recentButton{ new QPushButton{} };

    QFont font{ recentButton->font() };
    font.setPixelSize(16);
    font.setBold(true);

    recentButton->setFont(font);
    recentButton->setObjectName(fileName);
    recentButton->setToolTip(fileName);
    recentButton->setText(projectName);
    recentButton->setFlat(true);
    recentButton->setMaximumHeight(24);
    recentButton->setCursor(Qt::PointingHandCursor);

    return recentButton;
}

void NoProjectWidget::resizeEvent(QResizeEvent* /*event*/)
{
    if (!logo_)
        return;

    const QSize targetSize{ logo_->size().shrunkBy(logo_->contentsMargins()) };
    if (!targetSize.isEmpty() && logo_->pixmap(Qt::ReturnByValue).size() != targetSize)
        logo_->setPixmap(QPixmap{ ":/Edddy" }.scaled(targetSize, Qt::KeepAspectRatio, Qt::SmoothTransformation));
}
