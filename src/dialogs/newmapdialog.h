/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef NEWMAPDIALOG_H
#define NEWMAPDIALOG_H

#include "../luckey.h"

#include "../shortdoublespinbox.h"
#include <QSpinBox>
#include <QDialogButtonBox>
#include <QLineEdit>

#include <QDialog>

class NewMapDialog: public QDialog, public Object
{
    Q_OBJECT
    DRY_OBJECT(NewMapDialog, Object)

public:
    explicit NewMapDialog(Context* context, QWidget* parent = nullptr, const QString& path = "");

public slots:
    void accept() override;

private slots:
    void nameEdited();
    void nameEditFinished();

private:
    QString path_;
    QLineEdit* nameEdit_;
    QDialogButtonBox* buttonBox_;

    QSpinBox* mapWidthBox_;
    QSpinBox* mapHeightBox_;
    QSpinBox* mapDepthBox_;

    ShortDoubleSpinBox* blockWidthBox_;
    ShortDoubleSpinBox* blockHeightBox_;
    ShortDoubleSpinBox* blockDepthBox_;
};

#endif // NEWMAPDIALOG_H
