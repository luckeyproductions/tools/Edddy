/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QFormLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include "../editmaster.h"
#include "../blockmap/blockset.h"

#include "newblocksetdialog.h"

NewBlocksetDialog::NewBlocksetDialog(Context* context, QWidget* parent): QDialog(parent), Object(context),
    nameEdit_{ new QLineEdit{} },
    buttonBox_{ new QDialogButtonBox{ QDialogButtonBox::Ok | QDialogButtonBox::Cancel } }
{
    setWindowTitle("New Blockset");
    setWindowIcon(QIcon{ ":/Blockset" });

    QVBoxLayout* layout{ new QVBoxLayout{ this } };
    layout->setSizeConstraint(QLayout::SetFixedSize);

    QFormLayout* form{ new QFormLayout{} };
    form->setLabelAlignment(Qt::AlignRight);

    form->addRow("Name:", nameEdit_);

    layout->addLayout(form);
    layout->addWidget(buttonBox_);

    connect(buttonBox_, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox_, SIGNAL(rejected()), this, SLOT(reject()));

    connect(nameEdit_, SIGNAL(textEdited(QString)), this, SLOT(nameEdited()));
    connect(nameEdit_, SIGNAL(editingFinished()),   this, SLOT(nameEditFinished()));

    buttonBox_->button(QDialogButtonBox::Ok)->setEnabled(false);

    exec();
}

void NewBlocksetDialog::accept()
{
    String name{ nameEdit_->text().toStdString().data() };
    EM->NewBlockset(name);

    QDialog::accept();
}

void NewBlocksetDialog::nameEdited()
{
    if (nameEdit_->text().isEmpty())
        buttonBox_->button(QDialogButtonBox::Ok)->setEnabled(false);
    else
        buttonBox_->button(QDialogButtonBox::Ok)->setEnabled(true);
}

void NewBlocksetDialog::nameEditFinished()
{
    QString text{ nameEdit_->text() };

    if (text.contains('.'))
        text = text.split('.').first();

    if (text.contains('/'))
        text.replace('/', "");

    if (!text.isEmpty())
        text.append(".ebs");

    for (Blockset* bs: EM->GetBlocksets())
    {
        if (bs->name_.CString() == text)
            text.clear();
    }

    if (text != nameEdit_->text())
    {
        nameEdit_->setText(text);
        nameEdited();
    }
}
