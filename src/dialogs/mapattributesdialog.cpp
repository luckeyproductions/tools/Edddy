/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QVBoxLayout>
#include <QLineEdit>
#include <QComboBox>
#include <QPushButton>
#include <QHeaderView>
#include "../variantinput.h"
#include "../blockmap/blockmap.h"
#include "../mainwindow.h"

#include "mapattributesdialog.h"

MapAttributesDialog::MapAttributesDialog(Context* context, QWidget* parent): QDialog(parent), Object(context),
    table_{ nullptr },
    buttonBox_{ new QDialogButtonBox{ QDialogButtonBox::Ok | QDialogButtonBox::Cancel } }
{
    setWindowTitle("Map Attributes");
    setWindowIcon(QIcon{ ":/Map" });

    QVBoxLayout* layout{ new QVBoxLayout{ this } };
    layout->setSizeConstraint(QLayout::SetFixedSize);

    QPushButton* addButton{ new QPushButton{ "Add attribute" } };
    connect(addButton, SIGNAL(clicked(bool)), this, SLOT(addAttribute()));

    table_ = new QTableWidget{};
    table_->setColumnCount(4);
    table_->setHorizontalHeaderLabels({ "Name", "Type", "Default", "Delete" });
    table_->setColumnWidth(2, 256);
    table_->setColumnWidth(3, 64);
    table_->setCornerButtonEnabled(false);

    table_->setFrameShape(QFrame::NoFrame);
    table_->setShowGrid(false);
    table_->verticalHeader()->hide();
    table_->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
    table_->setSelectionMode(QAbstractItemView::NoSelection);
    table_->setContentsMargins({});
    table_->setFocusPolicy(Qt::NoFocus);

    QPushButton* okButton{ buttonBox_->button(QDialogButtonBox::Ok) };
    okButton->setAutoDefault(false);
    okButton->setDefault(false);

    layout->addWidget(addButton);
    layout->addWidget(table_);
    layout->addWidget(buttonBox_);

    connect(buttonBox_, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox_, SIGNAL(rejected()), this, SLOT(reject()));

    addAttribute(VAR_INTVECTOR3);
    addAttribute(VAR_VECTOR3);
    addAttributes();

    exec();
}

void MapAttributesDialog::accept()
{
    if (!buttonBox_->button(QDialogButtonBox::Ok)->hasFocus())
        return;

    Project* project{ MC->CurrentProject() };
    MapAttributes attributes{};
    bool modified{ false };

    for (int r{ 0 }; r < table_->rowCount(); ++r)
    {
        VariantInput* valueInput{ static_cast<VariantInput*>(table_->cellWidget(r, 2)) };
        const Variant value{ valueInput->variant() };

        if (r == 0)
            modified |= project->setDefaultMapSize(value.GetIntVector3());
        else if (r == 1)
            modified |= project->setDefaultBlockSize(value.GetVector3());
        else
        {
            QLineEdit* nameEdit{ static_cast<QLineEdit*>(table_->cellWidget(r, 0)) };
            const String name{ QD(nameEdit->text()) };

            attributes.Push({ name, value });
        }
    }

    modified |= project->setMapAttributes(attributes);

    if (modified)
        MainWindow::mainWindow_->updateWindowTitle();

    QDialog::accept();
}

int MapAttributesDialog::addAttribute(VariantType type)
{
    const int newRowIndex{ table_->rowCount() };
    table_->setRowCount(newRowIndex + 1);
    VariantInput* variantInput{ new VariantInput{} };
    table_->setCellWidget(newRowIndex, 2, variantInput);

    if (type == VAR_NONE)
    {
        QComboBox* typeBox{ new QComboBox{} };
        for (int t{ VAR_INT }; t < VAR_CUSTOM_HEAP; ++t)
        {
            const VariantType type{ static_cast<VariantType>(t) };

            switch (type)
            {
            default: break;
            case VAR_BUFFER: case VAR_VOIDPTR: case VAR_PTR: continue;

            case VAR_COLOR:
            case VAR_RESOURCEREF: case VAR_RESOURCEREFLIST: case VAR_VARIANTVECTOR: case VAR_VARIANTMAP:
            case VAR_MATRIX3: case VAR_MATRIX3X4: case VAR_MATRIX4: case VAR_STRINGVECTOR: case VAR_INT64: continue;
            }

            typeBox->addItem(DQ(Variant::GetTypeName(type)));
            typeBox->setItemData(typeBox->count() - 1, { t });
        }

        typeBox->setCurrentIndex(1); // Default: bool
        connect(typeBox, SIGNAL(currentIndexChanged(int)), variantInput, SLOT(typeChanged()));
        variantInput->setType(static_cast<VariantType>(typeBox->currentData().toInt()));

        QLineEdit* nameEdit{ new QLineEdit{} };
        table_->setCellWidget(newRowIndex, 0, nameEdit);
        connect(nameEdit, SIGNAL(editingFinished()), this, SLOT(updateOkButton()));
        table_->setCellWidget(newRowIndex, 1, typeBox);

        QPushButton* removeButton{ new QPushButton{} };
        removeButton->setIcon(QIcon{ ":/Delete" });
        removeButton->setFlat(true);
        table_->setCellWidget(newRowIndex, 3, removeButton);
        connect(removeButton, SIGNAL(clicked(bool)), this, SLOT(removeAttribute()));
    }
    else
    {
        Project* project{ MC->CurrentProject() };
        variantInput->setNegative(false);

        QString name{};
        switch (type)
        {
        default: break;
        case VAR_INTVECTOR3:
        {
            name = "Map Size";
            variantInput->setVariant({ project->defaultMapSize() });
        }
        break;
        case VAR_VECTOR3:
        {
            name = "Block Size";
            variantInput->setVariant({ project->defaultBlockSize() });
        }
        break;
        }

        QLabel* nameLabel{ new QLabel{ name } };
        QLabel* typeLabel{ new QLabel{ DQ(Variant::GetTypeName(type)) } };
        nameLabel->setAlignment(Qt::AlignCenter);
        typeLabel->setAlignment(Qt::AlignCenter);
        table_->setCellWidget(newRowIndex, 0, nameLabel);
        table_->setCellWidget(newRowIndex, 1, typeLabel);
    }

    updateOkButton();

    return newRowIndex;
}

void MapAttributesDialog::addAttributes()
{
    const MapAttributes& attributes{ MC->CurrentProject()->mapAttributes() };
    for (const Attribute& a: attributes)
        setAttribute(addAttribute(), a.first_, a.second_);

    updateOkButton();
}

void MapAttributesDialog::removeAttribute()
{
    for (int r{ 0 }; r < table_->rowCount(); r++)
    {
        if (table_->cellWidget(r, 3) == sender())
        {
            table_->removeRow(r);
            updateOkButton();

            return;
        }
    }
}

void MapAttributesDialog::updateOkButton()
{
    bool conflict{ false };
    QStringList names{};

    for (int r{ 2 }; r < table_->rowCount(); ++r)
    {
        QLineEdit* nameEdit{ static_cast<QLineEdit*>(table_->cellWidget(r, 0)) };
        const QString name{ nameEdit->text() };
        if (!name.isEmpty() && !names.contains(name))
        {
            names.push_back(name);
        }
        else
        {
            conflict = true;
            break;
        }
    }

    buttonBox_->button(QDialogButtonBox::Ok)->setEnabled(!conflict);
}

void MapAttributesDialog::setAttribute(int row, const String& name, const Variant& value)
{
    QComboBox* typeBox{ static_cast<QComboBox*>(table_->cellWidget(row, 1)) };
    for (int i{ 0 }; i < typeBox->count(); ++i)
    {
        if (typeBox->itemData(i).toInt() == value.GetType())
        {
            typeBox->setCurrentIndex(i);
            break;
        }
    }

    static_cast<QLineEdit*>(table_->cellWidget(row, 0))->setText(DQ(name));
    static_cast<VariantInput*>(table_->cellWidget(row, 2))->setVariant(value);
}
