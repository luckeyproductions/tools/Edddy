/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QFormLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QIcon>
#include <QFileDialog>
#include "../editmaster.h"
#include "../project.h"

#include "playdialog.h"

PlayDialog::PlayDialog(Context* context, QWidget* parent): QDialog(parent), Object(context),
    buttonBox_{ new QDialogButtonBox{ QDialogButtonBox::Ok | QDialogButtonBox::Cancel } },
    commandEdit_{ nullptr }
{
    setWindowTitle("Play settings");
    setWindowIcon(QIcon{ ":/Play" });

    QVBoxLayout* layout{ new QVBoxLayout{ this } };
    layout->setSizeConstraint(QLayout::SetFixedSize);

    QFormLayout* form{ new QFormLayout{} };
    form->setLabelAlignment(Qt::AlignRight);

    QHBoxLayout* commandRow{ new QHBoxLayout{} };
    commandEdit_ = new QLineEdit{};
    commandEdit_->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    commandEdit_->setMinimumWidth(64);
    commandEdit_->setAlignment(Qt::AlignRight);
    Project* p{ MC->CurrentProject() };
    commandEdit_->setText(DQ(p->GetAttribute("Command").GetString()));
    commandRow->addWidget(commandEdit_);

    QPushButton* commandButton{ new QPushButton{} };
    commandButton->setIcon(QIcon{ ":/Open" });
    commandButton->setToolTip("Pick command");
    commandButton->setProperty("lineedit", QVariant{ QMetaType::QObjectStar, &commandEdit_ });

    commandRow->addWidget(commandButton);
    form->addRow("Command:", commandRow);

    layout->addLayout(form);
    layout->addWidget(buttonBox_);

    connect(commandButton, SIGNAL(clicked(bool)), this, SLOT(onCommandPickClicked()));

    connect(buttonBox_, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox_, SIGNAL(rejected()), this, SLOT(reject()));

    exec();
}

void PlayDialog::accept()
{
    Project* p{ MC->CurrentProject() };
    /// Should validate
    p->SetAttribute("Command", QD(commandEdit_->text()));

    QDialog::accept();
}

void PlayDialog::onCommandPickClicked()
{
    QFileDialog dialog{this, tr("Select executable"), "" };
    dialog.setFileMode(QFileDialog::ExistingFile);
    dialog.setFilter(QDir::AllEntries | QDir::Hidden );
    if (dialog.exec())
    {
        const QString path { dialog.selectedFiles().front() };
        QFileInfo info{ path };

        //    QString folder{ path.split('/').last().append('/') };
        if (info.isExecutable() && commandEdit_->text() != path)
        {
            commandEdit_->setText(path);
        }
    }
}
