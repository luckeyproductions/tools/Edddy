/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QVBoxLayout>
#include <QListWidget>
#include <QListWidgetItem>
#include <QRadioButton>
#include <QScreen>

#include "../editmaster.h"
#include "../mainwindow.h"
#include "../blockmap/blockset.h"
#include "../docks/blockspanel.h"
#include "../docks/projectpanel.h"
#include "../project.h"

#include "blockdialog.h"

BlockDialog::BlockDialog(Context* context, Vector<Block*> blocks, QWidget* parent): QDialog(parent), Object(context),
    blocksIn_{ blocks },
    types_{},
    previewBlocks_{},
    previewItems_{},
    newBlockMaterials_{},
    previewIconSize_{ 128 },
    lineHeight_{ 16 },
    modeTabs_{ nullptr },
    splitter_{        new QSplitter{} },
    previewList_{     new QListWidget{} },
    modelList_{       new QListWidget{} },
    materialList_{    new QListWidget{} },
    materialLabel_{   new QLabel{ "Indices:" } },
    materialButtons_{ nullptr },
    buttonBox_{       new QDialogButtonBox{ QDialogButtonBox::Ok | QDialogButtonBox::Cancel } }
{
    QString title{};

    switch (blocksIn_.Size())
    {
    case 0:  title = "Add Blocks";  break;
    case 1:  title = "Edit Block";  break;
    default: title = "Edit Blocks"; break;
    }

    setWindowTitle(title);
    setWindowIcon(QIcon{ ":/Blockset" });
    setWindowFlags(windowFlags() | Qt::CustomizeWindowHint |
                                   Qt::WindowMaximizeButtonHint);

    QVBoxLayout* leftLayout{ new QVBoxLayout{} };
    leftLayout->setMargin(0);

//    blockList_->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    previewList_->setMinimumSize(previewIconSize_ + lineHeight_, previewIconSize_ + lineHeight_);
    previewList_->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    previewList_->setSizeAdjustPolicy(QAbstractScrollArea::AdjustIgnored);
    previewList_->setMovement(QListWidget::Static);
    previewList_->setFlow(QListWidget::LeftToRight);
    previewList_->setWrapping(true);
    previewList_->setResizeMode(QListWidget::Adjust);
    previewList_->setSpacing(0);
    previewList_->setGridSize({ previewIconSize_, previewIconSize_ + lineHeight_ });
    previewList_->setIconSize({ previewIconSize_, previewIconSize_ });
    previewList_->setViewMode(QListWidget::IconMode);
    previewList_->setUniformItemSizes(true);
    previewList_->setWordWrap(true);
    previewList_->setSelectionRectVisible(true);
    previewList_->setSelectionMode(QListWidget::NoSelection);

    setNumPreviewItems(blocksIn_.Size());

    if (!blocksIn_.IsEmpty())
    {
        for (unsigned b{ 0u }; b < blocksIn_.Size(); ++b)
        {
            Block* blockIn{ blocksIn_.At(b) };
            Block* previewBlock{ previewBlocks_.At(b) };
            previewBlock->setModel(blockIn->model());
            previewBlock->setMaterials(blockIn->materials());

            types_.Insert(blockIn->type());
        }

        if (blocksIn_.Size() > 1)
        {
            previewList_->setSelectionMode(QListWidget::ExtendedSelection);
            previewList_->selectAll();
        }
    }
    else
    {
        modelList_->setSelectionMode(QListWidget::ExtendedSelection);
    }

    leftLayout->addWidget(previewList_);
    QWidget* leftPanel{ new QWidget{} };
    leftPanel->setLayout(leftLayout);
    splitter_->addWidget(leftPanel);

    QVBoxLayout* rightLayout{ new QVBoxLayout{} };
    rightLayout->setMargin(0);

    if (blocksIn_.IsEmpty())
    {
        modeTabs_ =  new QTabBar{};
        modeTabs_->addTab("Block");
        modeTabs_->addTab("Tile");
        modeTabs_->addTab("Track");
        rightLayout->addWidget(modeTabs_);

        connect(modeTabs_, SIGNAL(currentChanged(int)), this, SLOT(modeChanged(int)));
    }

    const int resourceIconSize{ 96 };
    for (QListWidget* resourceList: { modelList_, materialList_ })
    {
        resourceList->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        resourceList->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContentsOnFirstShow);
        resourceList->setMovement(QListWidget::Static);
        resourceList->setFlow(QListWidget::LeftToRight);
        resourceList->setWrapping(true);
        resourceList->setResizeMode(QListWidget::Adjust);
        resourceList->setSpacing(4);
        resourceList->setGridSize(QSize(resourceIconSize + resourceList->spacing(), resourceIconSize + lineHeight_));
        resourceList->setIconSize(QSize(resourceIconSize, resourceIconSize));
        resourceList->setViewMode(QListWidget::IconMode);
        resourceList->setUniformItemSizes(true);
        resourceList->setWordWrap(true);
        resourceList->setSelectionRectVisible(true);
    }

    StringVector hiddenFolders{};
    if (!GetSubsystem<ProjectPanel>()->isShowingAll())
    {
        Project* project{ MC->CurrentProject() };
        hiddenFolders = toStringVector(project->hiddenFolders());
        for (unsigned f{ 0u }; f < hiddenFolders.Size(); ++f)
            hiddenFolders.At(f) = project->localizedPath(hiddenFolders.At(f));
    }

    populateModelList(hiddenFolders);
    rightLayout->addWidget(modelList_);
    populateMaterialList(hiddenFolders);
    rightLayout->addWidget(materialList_);

    QHBoxLayout* buttonRow{ new QHBoxLayout{} };
    buttonRow->setContentsMargins(4, 0, 0, 0);
    materialButtons_ = new QWidget{};
    materialButtons_->setLayout(new QHBoxLayout{});
    materialButtons_->layout()->setMargin(0);
    buttonRow->addWidget(materialLabel_);
    buttonRow->addWidget(materialButtons_);
    buttonRow->addSpacerItem(new QSpacerItem{ 0, 22, QSizePolicy::Expanding, QSizePolicy::Fixed });
    rightLayout->addLayout(buttonRow);

    if (blocksIn_.Size() == 1)
    {
        Block* blockIn{ blocksIn_.Front() };

        for (int i{ 0 }; i < modelList_->count(); ++i)
        {
            QListWidgetItem* item{ modelList_->item(i) };
            if (item->toolTip() == blockIn->modelName().CString())
            {
                modelList_->setCurrentItem(item);
                break;
            }
        }

        for (int i{ 0 }; i < materialList_->count(); ++i)
        {
            QListWidgetItem* item{ materialList_->item(i) };
            if (blockIn->materials().Values().Contains(item->toolTip().toStdString().data()))
            {
                materialList_->setCurrentItem(item);
                break;
            }
        }
    }

    QWidget* rightPanel{ new QWidget{} };
    rightPanel->setLayout(rightLayout);
    splitter_->addWidget(rightPanel);

    for (int i: { 0, 1 })
    {
        splitter_->setCollapsible  (i, false);
        splitter_->setStretchFactor(i, i + 1);
    }

    QVBoxLayout* mainLayout{ new QVBoxLayout(this) };
    mainLayout->setMargin(2);
    mainLayout->addWidget(splitter_);
    mainLayout->addWidget(buttonBox_);

    updateOkButton();
    updateMaterialButtons();

    connect(previewList_,  SIGNAL(itemSelectionChanged()), this, SLOT(updateMaterialButtons()));
    if (blocksIn_.IsEmpty())
        connect(modelList_,    SIGNAL(itemSelectionChanged()), this, SLOT(selectedModelChanged()));
    else
        connect(modelList_,    SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)), this, SLOT(selectedModelChanged()));

    connect(materialList_, SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)), this, SLOT(selectedMaterialChanged()));

    connect(buttonBox_, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox_, SIGNAL(rejected()), this, SLOT(reject()));

    resize(Clamp(2 + (static_cast<int>(blocksIn_.Size()) / 5 + modelList_->count() / 17 + materialList_->count() / 17), 5, 7) * QGuiApplication::primaryScreen()->size() / 8);
    exec();
}

void BlockDialog::updateMaterialButtons()
{
    if (!materialButtons_)
        return;

    bool addingTiles{ blocksIn_.IsEmpty() && mode() == AM_TILE };
    unsigned selected{ SINGLE };

    for (auto button: materialButtons_->findChildren<QRadioButton*>())
    {
        if (button->isChecked())
            selected = button->property("index").toUInt();

        delete button;
    }

    unsigned geometries{ 1u };

    if (!addingTiles)
    {
        if (previewList_->count() > 0)
        {
            for (BlockListItem* item: previewItems_)
            {
                if (blocksIn_.Size() > 1 && !previewList_->selectedItems().contains(item))
                    continue;

                Block* previewBlock{ extractPointer<Block>(item) };
                if (previewBlock->numGeometries() > geometries)
                    geometries = previewBlock->numGeometries();
            }

            if (selected >= geometries)
                selected = SINGLE;
        }
    }

    if (geometries > 1)
    {
        materialLabel_->setVisible(true);

        for (int b{ -1 }; b < static_cast<int>(geometries); ++b)
        {
            QRadioButton* button{ new QRadioButton{} };
            if (b == -1)
            {
                button->setText("All");
                button->setProperty("index", SINGLE);
                button->setChecked(selected == SINGLE);
            }
            else
            {
                button->setText(QString::number(b));
                button->setProperty("index", static_cast<unsigned>(b));

                if (static_cast<int>(selected) == b)
                    button->setChecked(true);
            }

            materialButtons_->layout()->addWidget(button);
        }
    }
    else
    {
        materialLabel_->setVisible(false);
    }
}

void BlockDialog::setNumPreviewItems(unsigned previews)
{
    while (previewItems_.Size() > previews)
    {
        BlockListItem* previewItem{ static_cast<BlockListItem*>(previewList_->takeItem(previewList_->count() - 1)) };
        previewItems_.Remove(previewItem);
        delete previewBlocks_.Back();
        previewBlocks_.Pop();
    }

    while (previewItems_.Size() < previews)
    {
        Block* previewBlock{ new Block{ context_ } };
        previewBlocks_.Push(previewBlock);
        BlockListItem* previewItem{ new BlockListItem{ context_, previewBlock }};
        previewItem->setSizeHint(previewItemSize());
        previewItem->setData(PointerRole, QVariant(QMetaType::QObjectStar, &previewBlock));
        previewItems_.Push(previewItem);
        previewList_->addItem(previewItem);
    }
}

void BlockDialog::populateModelList(const StringVector& hiddenFolders)
{
    PODVector<Model*> models{};
    GetSubsystem<ResourceCache>()->GetResources<Model>(models);
    for (Model* m: models)
    {
        if (!hiddenFolders.IsEmpty() && hiddenFolders.Contains(GetPath(m->GetName())))
            continue;

        Block* modelBlock{ new Block{ context_ } };
        modelBlock->setModel(m);
        modelBlock->setMaterial(nullptr);

        BlockListItem* item{ new BlockListItem{ context_, modelBlock } };
        item->setText(m->GetName().Split('/').Back().Split('.').Front().CString());
        item->setToolTip(m->GetName().CString());
        item->setData(PointerRole, { QMetaType::QObjectStar, &m });
        modelList_->addItem(item);
    }
}

void BlockDialog::populateMaterialList(const StringVector& hiddenFolders)
{
    PODVector<Material*> materials{};
    GetSubsystem<ResourceCache>()->GetResources<Material>(materials);
    for (Material* m: materials)
    {
        if (!hiddenFolders.IsEmpty() && hiddenFolders.Contains(GetPath(m->GetName())))
            continue;

        Block* materialBlock{ new Block{ context_ } };
        materialBlock->setMaterial(m);

        BlockListItem* item{ new BlockListItem{ context_, materialBlock }};
        item->setText(m->GetName().Split('/').Back().Split('.').Front().CString());
        item->setToolTip(m->GetName().CString());
        item->setData(PointerRole, { QMetaType::QObjectStar, &m });
        materialList_->addItem(item);
    }

    if (types_.Contains(BLOCK_TILE) && types_.Size() == 1u)
        modeChanged(AM_TILE);
    else
        modeChanged(AM_BLOCK);
}

AddMode BlockDialog::mode() const
{
    if (modeTabs_)
        return static_cast<AddMode>(modeTabs_->currentIndex());
    else
        return AM_BLOCK;
}

void BlockDialog::showEvent(QShowEvent* event)
{
    QDialog::showEvent(event);

    int size{ 16 + 130 * (1 + (static_cast<int>(blocksIn_.Size()) - 1) / Max(1, (height() - 64) / 144)) };
    splitter_->setSizes({ size, splitter_->sizes()[0] + splitter_->sizes()[1] - size });
}

void BlockDialog::accept()
{
    Blockset* currentBlockset{ EM->GetCurrentBlockset() };
    bool modifiedSet{ false };

    //New blocks
    if (!blocksIn_.Size())
    {
        for (Block* preview: previewBlocks_)
        {
            if (mode() == AM_BLOCK)
            {
                const String modelName{ preview->modelName() };
                const String blockName{ modelName.Split('/').Back().Split('.').Front() };
                currentBlockset->AddBlock(blockName, modelName, preview->materials());
            }
            else if (mode() == AM_TILE)
            {
                const Materials& previewMaterials{ preview->materials() };
                const String materialName{ (previewMaterials.IsEmpty() ? "" : previewMaterials.Values().Front()) };
                const String blockName{ materialName.Split('/').Back().Split('.').Front() };
                currentBlockset->AddTile(blockName, materialName);
            }
        }

        modifiedSet = true;
    }
    //Modify blocks
    else
    {
        for (unsigned i{ 0u }; i < blocksIn_.Size(); ++i)
        {
            Block* inBlock{ blocksIn_.At(i) };
            Block* previewBlock{ previewBlocks_.At(i) };

            const BlockType type{ inBlock->type() };
            bool modifiedBlock{ false };
            String blockName{};
            const Materials materials{ previewBlock->materials() };

            if (type == BLOCK_BLOCK )
            {
                const String modelName{ previewBlock->modelName() };
                blockName = modelName.Split('/').Back().Split('.').Front();

                if (inBlock->modelName() != modelName)
                {
                    inBlock->setModel(previewBlock->model());
                    modifiedBlock = true;
                }
            }
            else if (type == BLOCK_TILE)
            {
                const String materialName{ (materials.IsEmpty() ? "" : materials.Values().Front()) };
                blockName = materialName.Split('/').Back().Split('.').Front();
            }

            if (inBlock->name() != blockName)
            {
                inBlock->setName(blockName);
                modifiedBlock = true;
            }

            if (inBlock->materials() != materials)
            {
                inBlock->setMaterials(materials);
                modifiedBlock = true;
            }

            if (modifiedBlock)
            {
                inBlock->SendEvent(E_BLOCKMODIFIED);
                modifiedSet = true;
            }
        }
    }

    if (modifiedSet)
    {
        MainWindow::mainWindow_->handleFileModifiedStateChange();
        BlocksPanel* blocksPanel{ GetSubsystem<BlocksPanel>() };
        blocksPanel->updateBlocksetList();
        blocksPanel->updateBlockList();
    }

    for (Block* b: previewBlocks_)
        b->deleteLater();

    QDialog::accept();
}

void BlockDialog::modeChanged(int mode)
{
    modelList_->setVisible(mode == AM_BLOCK);

    if (mode == AM_BLOCK)
    {
        modelList_->setSelectionMode(QListWidget::ExtendedSelection);
        materialList_->setSelectionMode(QListWidget::SingleSelection);

        disconnect(materialList_, SIGNAL(itemSelectionChanged()), this, SLOT(selectedMaterialChanged()));
        connect(materialList_, SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)), this, SLOT(selectedMaterialChanged()));
    }
    else
    {
        modelList_->setSelectionMode(QListWidget::NoSelection);
        materialList_->setSelectionMode(QListWidget::ExtendedSelection);

        connect(materialList_, SIGNAL(itemSelectionChanged()), this, SLOT(selectedMaterialChanged()));
        disconnect(materialList_, SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)), this, SLOT(selectedMaterialChanged()));
    }

    for (int i{}; i < materialList_->count(); ++i)
    {
        BlockListItem* item{ static_cast<BlockListItem*>(materialList_->item(i)) };
        Block* materialBlock{ item->block() };

        if (mode != 1)
            materialBlock->setModel(GetSubsystem<ResourceCache>()->GetTempResource<Model>("Models/MaterialBox.mdl"));
        else
            materialBlock->setModel(GetSubsystem<ResourceCache>()->GetTempResource<Model>("Models/"/*Material*/"Plane.mdl"));

        item->UpdateBlockPreview();
    }

    updateMaterialButtons();
}

void BlockDialog::selectedModelChanged()
{
    if (blocksIn_.IsEmpty())
    {
        if (mode() == AM_BLOCK)
        {
            QList<QListWidgetItem*> modelItems{ modelList_->selectedItems() };
            setNumPreviewItems(modelItems.count());

            for (unsigned i{ 0u }; i < previewItems_.Size(); ++i)
            {
                BlockListItem* previewItem{ previewItems_.At(i) };
                Block* previewBlock{ extractPointer<Block>(previewItem) };
                const String modelName{ modelItems.at(i)->toolTip().toStdString().data() };
                previewBlock->setModel(GetSubsystem<ResourceCache>()->GetResource<Model>(modelName));

                if (newBlockMaterials_.Contains(SINGLE))
                    previewBlock->setMaterial(SINGLE, newBlockMaterials_[SINGLE]);

                for (const HashMap<unsigned, SharedPtr<Material>>::KeyValue& mat: newBlockMaterials_)
                    if (mat.first_ != SINGLE && mat.first_ < previewBlock->numGeometries())
                        previewBlock->setMaterial(mat.first_, mat.second_);

                previewItem->UpdateBlockPreview();
            }
        }
    }
    else
    {
        const String modelName{ modelList_->currentItem()->toolTip().toStdString().data() };

        for (unsigned i{ 0u }; i < previewItems_.Size(); ++i)
        {
            if (blocksIn_.At(i)->type() == BLOCK_BLOCK)
            {
                BlockListItem* item{ previewItems_.At(i) };

                if (previewList_->selectedItems().contains(item) || blocksIn_.Size() <= 1u)
                {
                    Block* previewBlock{ extractPointer<Block>(item) };
                    previewBlock->setModel(GetSubsystem<ResourceCache>()->GetResource<Model>(modelName));
                    item->UpdateBlockPreview();
                }
            }
        }
    }

    updateOkButton();
    updateMaterialButtons();
}

void BlockDialog::selectedMaterialChanged()
{
    const String materialName{ materialList_->currentItem()->toolTip().toStdString().data() };

    if (mode() == AM_BLOCK || !blocksIn_.IsEmpty())
    {
        for (BlockListItem* item: previewItems_)
        {
            if (previewList_->selectedItems().contains(item) || blocksIn_.Size() <= 1u)
            {
                Block* previewBlock{ extractPointer<Block>(item) };
                unsigned index{ SINGLE };

                for (auto button: materialButtons_->findChildren<QRadioButton*>())
                {
                    if (button->isChecked())
                        index = button->property("index").toUInt();
                }

                SharedPtr<Material> material{ GetSubsystem<ResourceCache>()->GetResource<Material>(materialName) };
                previewBlock->setMaterial(index, material);
                item->UpdateBlockPreview();

                if (blocksIn_.IsEmpty())
                    newBlockMaterials_[index] = material;
            }
        }
    }
    else if (mode() == AM_TILE)
    {
        QList<QListWidgetItem*> materialItems{ materialList_->selectedItems() };
        setNumPreviewItems(materialItems.count());

        for (unsigned i{ 0u }; i < previewItems_.Size(); ++i)
        {
            BlockListItem* previewItem{ previewItems_.At(i) };
            Block* previewBlock{ extractPointer<Block>(previewItem) };
            const String materialName{ materialItems.at(i)->toolTip().toStdString().data() };
            previewBlock->setModel(RES(Model, "Models/Plane.mdl"));
            previewBlock->setMaterial(GetSubsystem<ResourceCache>()->GetResource<Material>(materialName));

            previewItem->UpdateBlockPreview();
        }
    }

    updateOkButton();
}

void BlockDialog::updateOkButton()
{
    if (blocksIn_.IsEmpty())
        buttonBox_->button(QDialogButtonBox::Ok)->setEnabled(!previewBlocks_.IsEmpty());
}
