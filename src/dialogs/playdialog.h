/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef PLAYDIALOG_H
#define PLAYDIALOG_H

#include "../luckey.h"

#include <QDialogButtonBox>
#include <QDialog>
#include <QLineEdit>

class PlayDialog: public QDialog, public Object
{
    Q_OBJECT
    DRY_OBJECT(PlayDialog, Object);

public:
    PlayDialog(Context* context, QWidget* parent = nullptr);

public slots:
    void accept() override;

private slots:
    void onCommandPickClicked();

private:
    QDialogButtonBox* buttonBox_;
    QLineEdit* commandEdit_;
};

#endif // PLAYDIALOG_H
