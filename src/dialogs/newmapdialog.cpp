/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QFormLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include "../editmaster.h"
#include "../blockmap/blockmap.h"
#include "../view3d.h"
#include "../mainwindow.h"
#include "../docks/projectpanel.h"

#include "newmapdialog.h"

NewMapDialog::NewMapDialog(Context* context, QWidget* parent, const QString& path): QDialog(parent), Object(context),
    path_{ path },
    nameEdit_{ new QLineEdit{} },
    buttonBox_{ new QDialogButtonBox{ QDialogButtonBox::Ok | QDialogButtonBox::Cancel } }
{
    setWindowTitle("New Map");
    setWindowIcon(QIcon{ ":/Map" });

    BlockMap* activeMap{ nullptr };
    if (View3D::Active())
        activeMap = View3D::Active()->GetBlockMap();

    QVBoxLayout* layout{ new QVBoxLayout{ this } };
    layout->setSizeConstraint(QLayout::SetFixedSize);

    QFormLayout* form{ new QFormLayout{} };
    form->setLabelAlignment(Qt::AlignRight);

    form->addRow("Name:", nameEdit_);

    Project* project{ MC->CurrentProject() };
    const IntVector3& defaultMapSize{ project->defaultMapSize() };
    QHBoxLayout* mapRow{ new QHBoxLayout{} };
    for (int i{ 0 }; i < 3; ++i)
    {
        QSpinBox* spinBox{ new QSpinBox{} };
        spinBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
        spinBox->setMinimumWidth(64);
        spinBox->setMinimum(1);
        spinBox->setMaximum(256);
        spinBox->setAlignment(Qt::AlignRight);

        QString labelText{};
        switch (i) {
        case 0:
            labelText = "X";
            mapWidthBox_ = spinBox;
            spinBox->setValue(defaultMapSize.x_);
            break;
        case 1:
            labelText = "Y";
            mapHeightBox_ = spinBox;
            spinBox->setValue(defaultMapSize.y_ );
            break;
        case 2:
            labelText = "Z";
            mapDepthBox_ = spinBox;
            spinBox->setValue(defaultMapSize.z_ );
            break;
        default:
            break;
        }

        mapRow->addWidget(new QLabel{ labelText });
        mapRow->addWidget(spinBox);
    }
    form->addRow("Map Size:", mapRow);

    const Vector3& defaultBlockSize{ project->defaultBlockSize() };
    QHBoxLayout* blockRow{ new QHBoxLayout{} };
    for (int i{ 0 }; i < 3; ++i)
    {
        ShortDoubleSpinBox* doubleSpinBox{ new ShortDoubleSpinBox{} };
        doubleSpinBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
        doubleSpinBox->setMinimumWidth(64);
        doubleSpinBox->setMinimum(.001);
        doubleSpinBox->setMaximum(1024.);
        doubleSpinBox->setAlignment(Qt::AlignRight);
        QString labelText{};

        switch (i) {
        case 0:
            labelText = "X";
            blockWidthBox_ = doubleSpinBox;
            doubleSpinBox->setValue(defaultBlockSize.x_ );
            break;
        case 1:
            labelText = "Y";
            blockHeightBox_ = doubleSpinBox;
            doubleSpinBox->setValue(defaultBlockSize.y_ );
            break;
        case 2:
            labelText = "Z";
            blockDepthBox_ = doubleSpinBox;
            doubleSpinBox->setValue(defaultBlockSize.z_ );
            break;
        default:
            break;
        }

        blockRow->addWidget(new QLabel{ labelText });
        blockRow->addWidget(doubleSpinBox);
    }
    form->addRow("Grid Size:", blockRow);

    QPushButton* okButton{ buttonBox_->button(QDialogButtonBox::Ok) };
    okButton->setEnabled(false);
    okButton->setAutoDefault(false);
    okButton->setDefault(false);
    layout->addLayout(form);
    layout->addWidget(buttonBox_);

    connect(buttonBox_, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox_, SIGNAL(rejected()), this, SLOT(reject()));

    connect(nameEdit_, SIGNAL(textEdited(QString)), this, SLOT(nameEdited()));
    connect(nameEdit_, SIGNAL(editingFinished()),   this, SLOT(nameEditFinished()));

    exec();
}

void NewMapDialog::accept()
{
    if (!buttonBox_->button(QDialogButtonBox::Ok)->hasFocus())
        return;

    const QString name{ nameEdit_->text() };
    const IntVector3 mapSize{ mapWidthBox_->value(),
                              mapHeightBox_->value(),
                              mapDepthBox_->value() };

    const Vector3 blockSize{ static_cast<float>(blockWidthBox_->value()),
                             static_cast<float>(blockHeightBox_->value()),
                             static_cast<float>(blockDepthBox_->value()) };

    BlockMap* newMap{ EM->NewMap(mapSize, blockSize, name.toStdString().data()) };
    EM->OpenMap(newMap);
    GetSubsystem<ProjectPanel>()->updateBrowser();
    MainWindow::mainWindow_->updateWindowTitle();

    QDialog::accept();
}

void NewMapDialog::nameEdited()
{
    if (nameEdit_->text().isEmpty())
        buttonBox_->button(QDialogButtonBox::Ok)->setEnabled(false);
    else
        buttonBox_->button(QDialogButtonBox::Ok)->setEnabled(true);
}

void NewMapDialog::nameEditFinished()
{
    const QString name{ EM->validateMapName(nameEdit_->text()) };

    if (EM->checkMapNameConflict(path_ + name))
    {
        nameEdit_->clear();
        nameEdited();
        return;
    }

    if (name != nameEdit_->text())
    {
        nameEdit_->setText(name);
        nameEdited();
    }
}
