/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "mainwindow.h"

#include <QDebug>
#include <QFileDialog>
#include <QImage>
#include <QMessageBox>
#include <QColorDialog>
#include <QResizeEvent>
#include <QSpacerItem>
#include <QTabBar>
#include <QPainter>
#include <QSettings>
#include <QTemporaryDir>
#include <QProcess>

#include <QDesktopWidget>
#include <QMenuBar>
#include <QMenu>
#include <QToolBar>
#include <QStatusBar>
#include <QAction>

#include "blockmap/blockmap.h"
#include "blockmap/blockset.h"
#include "dialogs/newmapdialog.h"
#include "dialogs/newblocksetdialog.h"
#include "dialogs/blockdialog.h"
#include "docks/projectpanel.h"
#include "docks/mappanel.h"
#include "docks/blockspanel.h"
#include "tools/tool.h"
#include "blocklistitem.h"
#include "edddycam.h"
#include "edddycursor.h"
#include "editmaster.h"
#include "inputmaster.h"
#include "view3d.h"
#include "viewsplitter.h"
#include "project.h"

MainWindow* MainWindow::mainWindow_{ nullptr };

MainWindow::MainWindow(Context* context): QMainWindow{}, Object{ context },
    layerBoxes_{},
    centralStackWidget_{ nullptr },
    noProjectWidget_{ nullptr },
    undoView_{ nullptr },
    menuBar_{ nullptr },
    menuMap_{ nullptr },
    menuFile_{ nullptr },
    menuEdit_{ nullptr },
    menuTools_{ nullptr },
    menuHelp_{ nullptr },
    mainToolBar_{ nullptr },
    statusBar_{ nullptr },
    actionQuit_{ nullptr },
    actionNewMap_{ nullptr },
    actionSaveMap_{ nullptr },
    actionUndo_{ nullptr },
    actionRedo_{ nullptr },
    actionBrush_{ nullptr },
    actionFill_{ nullptr },
    actionOpenProject_{ nullptr },
    actionLockX_{ nullptr },
    actionLockY_{ nullptr },
    actionLockZ_{ nullptr },
    actionAboutEdddy_{ nullptr },
    actionSaveProject_{ nullptr },
    actionSplitView_{ nullptr },
    actionNewProject_{ nullptr },
    actionFly_{ nullptr },
    actionPlay_{ nullptr }
{
    //Influences how Dry writes floats to files
    std::locale::global(std::locale::classic());

    context_->RegisterSubsystem(this);
    mainWindow_ = this;
}

void MainWindow::initialize()
{
    setupUi();
    EM->setActiveLayer(1u);

    createUndoRedoActions();
    createPickLayerActions();

    loadSettings();
    show();

    actionBrush_->trigger();
    setAxisLock(std::bitset<3>{ 0b101 });
    handleCurrentProjectChanged();
}

void MainWindow::createUndoRedoActions()
{
    QAction* nextMapAction{ new QAction{ this } };
    nextMapAction->setShortcut({ "Ctrl+Tab" });
    connect(nextMapAction, SIGNAL(triggered()), EM, SLOT(nextMap()));
    QAction* previousMapAction{ new QAction{ this } };
    previousMapAction->setShortcut({ "Ctrl+Shift+Tab" });
    connect(previousMapAction, SIGNAL(triggered()), EM, SLOT(previousMap()));
    addActions({ nextMapAction, previousMapAction });

    connect(undoView_->selectionModel(), SIGNAL(currentRowChanged(QModelIndex, QModelIndex)),
            EM, SLOT(forceToolPreviewUpdate()));
}

void MainWindow::createPickLayerActions()
{
    for (int l{ 1 }; l <= NUM_LAYERS; ++l)
    {
        const QString layerString{ QString::number(l) };
        QAction* pickLayerAction{ new QAction{ this }};
        pickLayerAction->setObjectName("pickLayerAction" + layerString);
        pickLayerAction->setShortcut({ layerString });
        connect(pickLayerAction, SIGNAL(triggered()), EM, SLOT(pickLayer()));
        addAction(pickLayerAction);
    }
}

void MainWindow::loadSettings()
{
    const QSettings settings{};
    restoreGeometry(settings.value("mainwindow/geometry").toByteArray());
    restoreState(settings.value("mainwindow/state").toByteArray());

    noProjectWidget_->updateRecentList();
}

//void MainWindow::restoreGeometry()
//{
//    QSettings settings{};

//    QRect lastGeometry{ settings.value("mainwindow/geometry").toRect() };
//    if (!lastGeometry.isEmpty())
//    {
//        const QRect desktopGeometry{ QApplication::desktop()->geometry() };
//        if (desktopGeometry.contains(lastGeometry))
//        {
//            setGeometry(lastGeometry);
//        }
//        else if (desktopGeometry.height() >= lastGeometry.height() &&
//                 desktopGeometry.width()  >= lastGeometry.width())
//        {
//            const QSize cornerRect{ (desktopGeometry.size() - lastGeometry.size()) / 2 };
//            setGeometry(QRect(QPoint(cornerRect.width(), cornerRect.height()), lastGeometry.size()));
//        }
//    }

//    Qt::WindowState mainWindowState{ settings.value("mainwindow/state").value<Qt::WindowState>() };
    
//    if (mainWindowState != Qt::WindowState::WindowMinimized)
//        setWindowState(mainWindowState);
//}

void MainWindow::closeEvent(QCloseEvent* event)
{
    if (View3D::flyMode_)
        toggleFlyMode();

    if (!MC->Quit())
        event->ignore();
}

MainWindow::~MainWindow()
{
    QSettings settings{};

    settings.setValue("mainwindow/geometry", saveGeometry());
    settings.setValue("mainwindow/state", saveState());
}

void MainWindow::createLayerButtons()
{
    QWidget* expander{ new QWidget{} };
    expander->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    mainToolBar_->addWidget(expander);

    for (int i{ 1 }; i <= NUM_LAYERS; ++i)
    {
        const QString layerString{ QString::number(i) };
        QCheckBox* layerBox{ new QCheckBox{ layerString } };
        layerBox->setEnabled(false);
        layerBox->setObjectName("layerBox" + layerString);
        layerBox->setLayoutDirection(Qt::RightToLeft);
        mainToolBar_->addWidget(layerBox);
        layerBoxes_.push_back(layerBox);

        connect(layerBox, SIGNAL(clicked(bool)),                this, SLOT(layerBoxClicked()));
        connect(EM,       SIGNAL(activeLayerChanged(unsigned)), this, SLOT(activeLayerChanged(unsigned)));
    }
}

void MainWindow::layerBoxClicked()
{
    EM->setActiveLayer(std::stoi(sender()->objectName().right(1).toStdString()));
}

void MainWindow::activeLayerChanged(unsigned layer)
{
    for (QCheckBox* layerBox: layerBoxes_)
        layerBox->setChecked(std::stoul(layerBox->objectName().right(1).toStdString()) == layer);
}

void MainWindow::updateWindowTitle()
{
    Project* currentProject{ MC->CurrentProject() };

    if (currentProject)
        setWindowTitle(currentProject->displayName() + (currentProject->isModified() ? "*" : "") + " - Edddy");
    else
        setWindowTitle("Edddy");
}

void MainWindow::handleCurrentProjectChanged()
{
    updateMapActions();

    Project* currentProject{ MC->CurrentProject() };
    ProjectPanel* projectPanel{ GetSubsystem<ProjectPanel>() };

    actionSaveProject_->setEnabled(currentProject);
    actionNewMap_->setEnabled(currentProject);
    projectPanel->setEnabled(currentProject);
    GetSubsystem<BlocksPanel>()->setEnabled(currentProject);

    centralStackWidget_->setCurrentIndex((currentProject ? 1 : 0));

    MapPanel* mapPanel{ GetSubsystem<MapPanel>() };
    mapPanel->disconnect(mapPanel, SLOT(updateAttributeInfo()));

    if (currentProject)
    {
        currentProject->OnResourcesFolderSet();
        updateWindowTitle();

        mapPanel->connect(currentProject, SIGNAL(defaultMapAttributesChanged()), mapPanel, SLOT(updateAttributeInfo()));
    }

    GetSubsystem<ProjectPanel>()->updatePanel();
    projectPanel->focusBrowser();
}

void MainWindow::updateMapActions()
{
    Project* currentProject{ MC->CurrentProject() };
    bool activeMap{ EM->GetActiveBlockMap() != nullptr };

    actionSaveMap_->setEnabled(activeMap);
    actionUndo_->setEnabled(activeMap);
    actionRedo_->setEnabled(activeMap);
    actionBrush_->setEnabled(activeMap);
    actionFill_->setEnabled(activeMap);
    actionDig_->setEnabled(activeMap);
    actionSplitView_->setEnabled(activeMap);
    actionLockX_->setEnabled(activeMap);
    actionLockY_->setEnabled(activeMap);
    actionLockZ_->setEnabled(activeMap);
    actionFly_->setEnabled(activeMap);
    actionPlay_->setEnabled(activeMap && !currentProject->command().IsEmpty());

    GetSubsystem<MapPanel>()->setEnabled(activeMap);

    for (QCheckBox* box: layerBoxes_)
        box->setEnabled(activeMap);
}

void MainWindow::newProject()
{
    MC->NewProject(QFileDialog::getSaveFileName(
                        this, tr("New Project"), "/home", tr("EDY Files (*.edy)")));
}

void MainWindow::openProject()
{
    Project* project{ MC->CurrentProject() };

    MC->OpenProject(QD(QFileDialog::getOpenFileName(
                        this, tr("Open Project"), (project ? project->GetAttribute("Location").ToString().CString()
                                                           : "/home"),
                        tr("Edddy Project file (*.edy)"))));
}

void MainWindow::saveMap()
{
    EM->GetActiveBlockMap()->Save();

    for (View3D* v: View3D::views_)
        v->UpdateMapBox();

    GetSubsystem<ProjectPanel>()->updateBrowser();
}

QString MainWindow::getSaveFilename(const TypeInfo* typeInfo, const QString name)
{
    Project* project{ MC->CurrentProject() };
    const QString typeName{ DQ(typeInfo->GetTypeName()) };
    QString filter{};
    if (typeInfo->IsTypeOf<Blockset>())
        filter = "Edddy Blockset Files (*.ebs)";
    if (typeInfo->IsTypeOf<BlockMap>())
        filter = "Edddy Block Map Files (*.emp)";
    const QString startFolder{ DQ(project->resourceFolder(true)) };
    const QString fullPath{ QFileDialog::getSaveFileName(
                            this, tr(("Save " + typeName + " as").toLatin1()), startFolder + name, tr(filter.toLatin1())) };

    return fullPath;
}

void MainWindow::undo()
{
    EM->Undo();
}

void MainWindow::redo()
{
    EM->Redo();
}

void MainWindow::pickBrush()
{
    Tool* brush{ Tool::GetTool({ "Brush" }) };
    if (!brush)
    {
        actionBrush_->setChecked(false);
        return;
    }

    brush->Reset();
    EM->SetTool(brush);

    actionFill_->setChecked(false);
    actionDig_->setChecked(false);
    actionBrush_->setChecked(true);
}

void MainWindow::pickFill()
{
    Tool* fill{ Tool::GetTool({ "Fill" }) };
    if (!fill)
    {
        actionFill_->setChecked(false);
        return;
    }

    fill->Reset();
    EM->SetTool(fill);

    actionBrush_->setChecked(false);
    actionDig_->setChecked(false);
    actionFill_->setChecked(true);
}

void MainWindow::pickDig()
{
    Tool* dig{ Tool::GetTool({ "Dig" }) };
    if (!dig)
    {
        actionDig_->setChecked(false);
        return;
    }

    EM->SetTool(dig);

    actionBrush_->setChecked(false);
    actionFill_->setChecked(false);
    actionDig_->setChecked(true);
}

void MainWindow::about()
{
    QString aboutText{ QString{ "<p><b>" } + "Edddy" + QString(" ")
                + qApp->applicationVersion() + QString{ " </b></p>" }
                + tr("<p>Copyleft 🄯 2016-2024 <a href=\"https://luckey.games\">LucKey Productions</a></b>"
                     "<p>You may use and redistribute this software under the terms "
                     "of the<br><a href=\"http://www.gnu.org/licenses/gpl.html\">"
                     "GNU General Public License Version 2</a> or later.</p>") };

    QMessageBox::about(this, tr("About %1").arg("Edddy"), aboutText);
}

void MainWindow::setAxisLock(std::bitset<3> lock)
{
    EdddyCursor* cursor{ EdddyCursor::cursor_ };
    cursor->SetAxisLock(QApplication::keyboardModifiers() & Qt::ShiftModifier ? lock.flip()
                                                                              : lock);
    actionLockX_->setChecked(lock[0]);
    actionLockY_->setChecked(lock[1]);
    actionLockZ_->setChecked(lock[2]);
}

void MainWindow::setPlayCommandEnabled(bool enabled)
{
    actionPlay_->setEnabled(enabled);
}

void MainWindow::actionLockAxisTriggered()
{
    const QString senderName{ sender()->objectName() };
    switch (senderName.at(senderName.length() - 1).toLatin1())
    {
    case 'X': setAxisLock(0b001); break;
    case 'Y': setAxisLock(0b010); break;
    case 'Z': setAxisLock(0b100); break;
    default: break;
    }
}

void MainWindow::handleFileModifiedStateChange()
{
    updateWindowTitle();
    View3D::updateMapBoxes();

    /// Ugly solution, instead browser update should be less drastic
//    Block* currentBlock{ EM->GetCurrentBlock() };
//    GetSubsystem<ProjectPanel>()->updateBrowser();
//    EM->SetCurrentBlock(currentBlock);
}

void MainWindow::splitView()
{
    if (View3D::views_.Size())
        View3D::Active()->Split();
}

void MainWindow::saveProject()
{
    MC->SaveCurrentProject();
}

void MainWindow::newMap()
{
    NewMapDialog* dialog{ new NewMapDialog(context_, this) };
    dialog->deleteLater();
}

void MainWindow::newBlockset()
{
    NewBlocksetDialog* dialog{ new NewBlocksetDialog(context_, this) };
    dialog->deleteLater();

    GetSubsystem<ProjectPanel>()->updateBrowser();
}

void MainWindow::toggleFlyMode()
{
    if (!View3D::flyMode_)
    {
        if (View3D::Active() && View3D::Active()->GetBlockMap())
        {
            View3D::Active()->EnableFlyMode();
            actionFly_->setShortcuts({ QKeySequence("Alt+F"), QKeySequence("Escape") });
            actionFly_->setChecked(true);
        }
        else
        {
            actionFly_->setChecked(false);
        }
    }
    else
    {
        View3D::flyMode_ = false;
        View3D::Active()->setCursor(Qt::ArrowCursor);
        View3D::Active()->releaseMouse();
        actionFly_->setShortcuts({ QKeySequence("Alt+F") });
        actionFly_->setChecked(false);
    }
}

void MainWindow::playLevel(bool game)
{
    Project* p{ MC->CurrentProject() };
    String command{ p->command() };
    QStringList args{};

    if (command.Contains(' '))
    {
        const StringVector split{ command.Split(' ') };
        command = split.Front();

        for (unsigned s{ 1u }; s < split.Size(); ++s)
            args.push_back(DQ(split.At(s)));
    }

    if (!command.IsEmpty())
    {
        String arg{};
        QTemporaryDir tempDir{};
        BlockMap* activeMap{ EM->GetActiveBlockMap() };

        if (p->isModified())
        {
            QDir dir{ tempDir.path() };
            const QString resFolder{ DQ(p->resourceFolder()) };
            dir.mkpath(resFolder); dir.cd(resFolder);
            const String tempResPath{ QD(dir.path()) };

            if (!game && activeMap)
            {
                arg = activeMap->SaveTemp(tempResPath);
            }
            else
            {
                arg = QD(dir.path());

                PODVector<Blockset*> savedBlocksets{};
                for (Blockset* bs: EM->GetBlocksets())
                {
                    if (bs->isModified())
                    {
                        if (bs->SaveTemp(tempResPath))
                            savedBlocksets.Push(bs);
                    }
                }

                for (BlockMap* bm: EM->GetBlockMaps())
                {
                    if (bm->isModified())
                        bm->SaveTemp(tempResPath, savedBlocksets);
                }
            }
        }
        else
        {
            if (!game && activeMap)
                arg = activeMap->savedFullPath();

            tempDir.remove();
        }

        if (!arg.IsEmpty())
            args.push_back(arg.CString());

        if (View3D::flyMode_)
            toggleFlyMode();
        else if (mouseGrabber())
            mouseGrabber()->releaseMouse();

        mainToolBar_->repaint();

        String argsLog{};
        for (const QString& a: args)
            argsLog += " " + QD(a);

        Log::Write(LOG_INFO, "Running command: " + command + " With arguments:" + argsLog);
        QProcess game{ this };
        game.setProcessChannelMode(QProcess::MergedChannels);
        game.start(command.CString(), args);
        game.waitForFinished(-1);
    }

    actionPlay_->setChecked(false);
}

void MainWindow::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::ActivationChange)
    {
        if (this->isActiveWindow())
        {
            if (View3D::flyMode_)
                View3D::Active()->grabMouse();
        }
        else if (mouseGrabber())
        {
            mouseGrabber()->releaseMouse();
        }
    }
}
