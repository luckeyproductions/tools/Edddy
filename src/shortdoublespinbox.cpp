/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QApplication>
#include <math.h>
#include "shortdoublespinbox.h"

ShortDoubleSpinBox::ShortDoubleSpinBox(QWidget* parent): QDoubleSpinBox(parent)
{
    setDecimals(3);
    setSingleStep(.1);
    connect(this, SIGNAL(valueChanged(double)), this, SLOT(roundResult()));
}

QString ShortDoubleSpinBox::textFromValue(double value) const
{
    return QLocale().toString(value, 'g', QLocale::FloatingPointShortest);
}

void ShortDoubleSpinBox::keyPressEvent(QKeyEvent* e)
{
    if (e->key() == Qt::Key_Period && QLocale().decimalPoint() == ',')
    {
        QKeyEvent* press{   new QKeyEvent{ QEvent::KeyPress,   Qt::Key_Comma, Qt::NoModifier, "," } };
        QKeyEvent* release{ new QKeyEvent{ QEvent::KeyRelease, Qt::Key_Comma, Qt::NoModifier, "," } };

        qApp->postEvent(this, press);
        qApp->postEvent(this, release);

        return;
    }
    else if (e->key() == Qt::Key_Comma && QLocale().decimalPoint() == '.')
    {
        QKeyEvent* press{   new QKeyEvent{ QEvent::KeyPress,   Qt::Key_Period, Qt::NoModifier, "." } };
        QKeyEvent* release{ new QKeyEvent{ QEvent::KeyRelease, Qt::Key_Period, Qt::NoModifier, "." } };

        qApp->postEvent(this, press);
        qApp->postEvent(this, release);

        return;
    }

    QAbstractSpinBox::keyPressEvent(e);
}

void ShortDoubleSpinBox::roundResult()
{
    const float factor{ pow(10.f, 1.f * decimals()) };
    setValue(qRound(value() * factor) / factor);
}


