/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef VARIANTINPUT_H
#define VARIANTINPUT_H

#include "luckey.h"

#include <QHBoxLayout>
#include <QCheckBox>
#include <QSpinBox>
#include "shortdoublespinbox.h"
#include <QLineEdit>

using Attribute = Pair<String, Variant>;

class VariantInput: public QWidget
{
    Q_OBJECT
public:
    explicit VariantInput(QWidget* parent = nullptr);

    const Variant& variant() const { return var_; }

    void setType(VariantType type);
    void setVariant(const Variant& variant);
    void setNegative(bool negative) { negative_ = negative; }

    VariantType type() const { return type_; }

signals:
    void valueChanged(const Variant&);

public slots:
    void typeChanged();

private slots:
    void inputChanged();

private:
    void updateType();

    void addCheckBox();
    void addLineEdit();
    PODVector<QAbstractSpinBox*> addSpinBoxes(int count, bool integer);

    void setToolTipsVector(const PODVector<QAbstractSpinBox*>& spinBoxes);
    void setToolTipsQuaternion(const PODVector<QAbstractSpinBox*>& spinBoxes);
    void setToolTipsRect(const PODVector<QAbstractSpinBox*>& spinBoxes);

    int getInt(int i = 0) const
    {
        return static_cast<QSpinBox*>(layout_->itemAt(i)->widget())->value();
    }

    bool getBool() const
    {
        return static_cast<QCheckBox*>(layout_->itemAt(0)->widget())->isChecked();
    }

    float getFloat(int i = 0) const
    {
        return static_cast<ShortDoubleSpinBox*>(layout_->itemAt(i)->widget())->value();
    }

    Vector2 getVector2() const { return { getFloat(0), getFloat(1) }; }
    Vector3 getVector3() const { return { getFloat(0), getFloat(1), getFloat(2) }; }
    Vector4 getVector4() const { return { getFloat(0), getFloat(1), getFloat(2), getFloat(3) }; }

    Quaternion getQuaternion() const { return { getFloat(0), getFloat(1), getFloat(2), getFloat(3) }; }

    String getString() const
    {
        return QD(static_cast<QLineEdit*>(layout_->itemAt(0)->widget())->text());
    }

    IntRect getIntRect() { return { getInt(0), getInt(1), getInt(2), getInt(3) }; }
    IntVector2 getIntVector2() { return { getInt(0), getInt(1) }; }
    IntVector3 getIntVector3() { return { getInt(0), getInt(1), getInt(2) }; }

    double getDouble() const
    {
        return static_cast<ShortDoubleSpinBox*>(layout_->itemAt(0)->widget())->value();
    }

    Rect getRect() { return { getFloat(0), getFloat(1), getFloat(2), getFloat(3) }; }

    void updateValue();

    void setString(const String& string)
    {
        static_cast<QLineEdit*>(layout_->itemAt(0)->widget())->setText(DQ(string));
    }

    void setInt(const int* data, unsigned size)
    {
        for (unsigned d{ 0u }; d < size; ++d)
            static_cast<QSpinBox*>(layout_->itemAt(d)->widget())->setValue(*(data + d));
    }

    void setBool(bool value)
    {
        static_cast<QCheckBox*>(layout_->itemAt(0)->widget())->setChecked(value);
    }

    void setFloat(const float* data, unsigned size)
    {
        for (unsigned d{ 0u }; d < size; ++d)
            static_cast<ShortDoubleSpinBox*>(layout_->itemAt(d)->widget())->setValue(*(data + d));
    }

    void setDouble(double data)
    {
        static_cast<ShortDoubleSpinBox*>(layout_->itemAt(0)->widget())->setValue(data);
    }

    bool negative_;
    VariantType type_;
    Variant var_;
    QHBoxLayout* layout_;
};

#endif // VARIANTINPUT_H
