/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "edddycam.h"
#include "view3d.h"
#include "inputmaster.h"
#include "effectmaster.h"
#include "castmaster.h"
#include "editmaster.h"
#include "blockmap/blockmap.h"

#include "edddycursor.h"

EdddyCursor* EdddyCursor::cursor_{};

EdddyCursor::EdddyCursor(Context* context): LogicComponent(context),
    blockMap_{ nullptr },
    coords_{},
    mouseRay_{},
    rotation_{},
    blockModelGroups_{},
    previewNodes_{},
    axisLock_{}
{
    cursor_ = this;
}

void EdddyCursor::OnNodeSet(Node *node)
{
    if (!node)
        return;

    blockNode_ = node_->CreateChild("PREVIEW");

    ResourceCache* cache{ GetSubsystem<ResourceCache>() };

    boxNode_ = node_->CreateChild("BOX");
    boxModel_ = boxNode_->CreateComponent<StaticModel>();
    boxModel_->SetModel(cache->GetTempResource<Model>("Models/Cursor.mdl"));
    boxModel_->SetMaterial(cache->GetTempResource<Material>("Materials/GlowWire.xml"));

    for (bool wire: { true, false })
    {
        StaticModelGroup* blockModelGroup{ blockNode_->CreateComponent<StaticModelGroup>() };

        if (wire)
            blockModelGroups_.first_ = blockModelGroup;
        else
            blockModelGroups_.second_ = blockModelGroup;
    }

    AddCursorPreviewInstance();
    SubscribeToEvent(E_CURRENTTOOLCHANGED, DRY_HANDLER(EdddyCursor, HandleCurrentToolChanged));
    SubscribeToEvent(E_CURRENTBLOCKCHANGED, DRY_HANDLER(EdddyCursor, UpdateModel));
    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(EdddyCursor, DrawDebug));
}

void EdddyCursor::SetBlockMap(BlockMap* blockMap)
{
    if (blockMap == blockMap_)
        return;

    BlockMap* previousBlockMap{ blockMap_ };

    if (blockMap)
    {
        blockMap_ = blockMap;
        UpdateSize();
        blockMap->GetScene()->AddChild(node_);
        MoveTo(blockMap->GetCenter());
    }
    else
    {
        blockMap_ = nullptr;
    }

    if (previousBlockMap)
        previousBlockMap->UpdateViews();
}

Tool* EdddyCursor::tool() const
{
    if (!EM)
        return nullptr;

    return EM->GetTool();
}

void EdddyCursor::UpdateModel(StringHash /*eventType*/, VariantMap& eventData)
{
    if (Block* currentBlock{ static_cast<Block*>(eventData[CurrentBlockChanged::P_BLOCK].GetPtr()) })
    {
        Model* model{ currentBlock->model() };

        if (model)
        {
            ResourceCache* cache{ GetSubsystem<ResourceCache>() };

            blockModelGroups_.first_->SetModel(model);
            blockModelGroups_.first_->SetMaterial(cache->GetTempResource<Material>("Materials/GlowWire.xml"));
            blockModelGroups_.second_->SetModel(model);
            blockModelGroups_.second_->SetMaterial(cache->GetTempResource<Material>("Materials/TransparentGlow.xml"));
            ClearPreview();

            if (boxNode_->IsEnabled())
                boxNode_->SetEnabled(false);
        }
    }
    else
    {
        blockModelGroups_.first_->SetModel(nullptr);
        blockModelGroups_.second_->SetModel(nullptr);

        if (!boxNode_->IsEnabled())
            boxNode_->SetEnabled(true);
    }

    tool()->UpdatePreview(true);
}

void EdddyCursor::Hide()
{
    hidden_ = true;
    boxNode_->SetEnabled(false);
    blockNode_->SetEnabled(false);

    SendEvent(E_CURSORSTEP);
    ClearPreview();
}

void EdddyCursor::Show()
{
    hidden_ = false;
    blockNode_->SetEnabled(true);

    if (blockModelGroups_.first_->GetModel())
        boxNode_->SetEnabled(false);
    else
        boxNode_->SetEnabled(true);

    EM->forceToolPreviewUpdate();
}

void EdddyCursor::AddCursorPreviewInstance()
{
    if (!EM)
        return;

    blockModelGroups_.first_->AddInstanceNode(blockNode_);

    if (tool() && tool()->IsInstanceOf("Brush"))
        blockModelGroups_.second_->AddInstanceNode(blockNode_);
}

void EdddyCursor::AddPreviewNode(const Vector3& position, const Quaternion& rotation)
{
    Node* previewNode{ GetScene()->CreateChild("PREVIEW") };

    previewNode->SetPosition(position);
    previewNode->SetWorldRotation(rotation);
    blockModelGroups_.second_->AddInstanceNode(previewNode);

    previewNodes_.Push(previewNode);
}

void EdddyCursor::AddPreviewNode(const IntVector3& coords, const Quaternion& rotation)
{
    if (!blockMap_ || !GetScene() || !EM)
        return;

    const Vector3 position{ blockMap_->coordsToPosition(coords) };

    if (!EM->GetCurrentBlock())
    {
        if (BlockInstance* gridBlock{ blockMap_->GetBlockInstance(coords, GetSubsystem<EditMaster>()->activeLayer()) })
        {
            Node* previewNode{ GetScene()->CreateChild("PREVIEW") };
            previewNode->SetPosition(position);
            previewNode->SetWorldRotation(gridBlock->GetRotation());

            StaticModel* previewModel{ previewNode->CreateComponent<StaticModel>() };
            previewModel->SetModel(gridBlock->model());
            previewModel->SetMaterial(GetSubsystem<ResourceCache>()->GetTempResource<Material>("Materials/RedWire.xml"));
            previewNodes_.Push(previewNode);
        }
    }
    else
    {
        AddPreviewNode(position, rotation);
    }
}

void EdddyCursor::ClearPreview()
{
    if (!GetScene())
        return;

    blockModelGroups_.first_->RemoveAllInstanceNodes();
    blockModelGroups_.second_->RemoveAllInstanceNodes();

    for (Node* node: previewNodes_)
        node->Remove();

    previewNodes_.Clear();

    if (!EM->digging())
    {
        AddCursorPreviewInstance();
        boxNode_->SetEnabled(!EM->GetCurrentBlock());
    }
    else
    {
        boxNode_->SetEnabled(false);
    }
}

void EdddyCursor::UpdateSize()
{
    BlockMap* currentMap{ EM->GetActiveBlockMap() };

    if (currentMap)
        boxNode_->SetScale(currentMap->GetBlockSize());
    else
        boxNode_->SetScale(Vector3::ONE);
}

void EdddyCursor::SetAxisLock(std::bitset<3> lock)
{
    if (lock.all() || lock.none())
        return;

    if (lock != axisLock_)
    {
        axisLock_ = lock;

        SendEvent(E_CURSORSTEP);
    }
}

void EdddyCursor::Step(IntVector3 step)
{
    if (step == IntVector3::ZERO)
        return;

    IntVector3 mapSize{ IntVector3::ONE };
    if (blockMap_)
        mapSize = blockMap_->GetMapSize();

    switch (axisLock_.to_ulong())
    {
    default: break;
    case 5: step = { step.x_, step.z_, step.y_ }; break;
    case 3: step = { step.x_, step.y_, step.z_ }; break;
    case 6: step = { step.z_, step.y_, step.x_ }; break;

    case 1: step = { step.x_ + step.y_ + step.z_, 0, 0 }; break;
    case 2: step = { 0, step.x_ + step.y_ + step.z_, 0 }; break;
    case 4: step = { 0, 0, step.x_ + step.y_ + step.z_ }; break;
    }

    const IntVector3 resultingCoords{ coords_ + step };

    if (resultingCoords.x_ < 0
     || resultingCoords.y_ < 0
     || resultingCoords.z_ < 0
     || resultingCoords.x_ >= mapSize.x_
     || resultingCoords.y_ >= mapSize.y_
     || resultingCoords.z_ >= mapSize.z_)
    {
        return;
    }

    SetCoords(resultingCoords);
}

void EdddyCursor::SetCoords(const IntVector3& coords, const IntVector3& subCoords)
{
    if (!blockMap_)
        return;

    const Vector3 blockSize{ blockMap_->GetBlockSize() };
    bool appeared{ !hidden_ };
    bool coordsChanged{ false };

    if (!blockMap_->Contains(coords))
    {
        if (!hidden_)
            Hide();

        return;
    }
    else
    {
        if (hidden_)
        {
            Show();
            appeared = true;
        }
    }

    if (coords != coords_ || subCoords_ != subCoords)
    {
        coords_ = coords;
        subCoords_ = subCoords;
        const Vector3 subBlock{ blockSize / subStep() };
        const Vector3 offset{ EM->digging() * Vector3::DOWN * blockSize.y_ * .5f };
        const Vector3 position{ coords * blockSize + (subCoords * subBlock) + offset };

        if (appeared)
        {
            node_->RemoveAttributeAnimation("Position");
            node_->SetPosition(position);
        }
        else
        {
            GetSubsystem<EffectMaster>()->TranslateTo(node_, position, .023f);
        }

        coordsChanged = true;
    }

    if (coordsChanged || appeared)
        SendEvent(E_CURSORSTEP);
}

Vector3 EdddyCursor::positionFromCoords() const
{
    const Vector3 blockSize{ blockMap_->GetBlockSize() };
    return coords_ * blockSize +
            (subCoords_ - subStep() * .5f) * blockSize / subStep();
}

void EdddyCursor::MoveTo(const Vector3& position)
{
    //if current layer type == BLOCKS_GRID
    const Vector3 blockSize{ (blockMap_ ? blockMap_->GetBlockSize() : Vector3::ONE) };
    const IntVector3 coords{ VectorRoundToInt(position / blockSize) };
    const IntVector3 subCoords{ VectorRoundToInt((position - coords * blockSize) / (blockSize / subStep())) };

    if (coords_ == coords && subCoords_ == subCoords && !IsHidden())
        return;

    SetCoords({ axisLock_[0] ? coords.x_ : coords_.x_,
                axisLock_[1] ? coords.y_ : coords_.y_,
                axisLock_[2] ? coords.z_ : coords_.z_ },

              { axisLock_[0] ? subCoords.x_ : subCoords_.x_,
                axisLock_[1] ? subCoords.y_ : subCoords_.y_,
                axisLock_[2] ? subCoords.z_ : subCoords_.z_ });
}

void EdddyCursor::Rotate(bool clockWise)
{
    const Vector3 axis{ Vector3::UP };
    rotation_ = Quaternion(clockWise ? 90.0f :  -90.0f, axis) * rotation_;
    GetSubsystem<EffectMaster>()->RotateTo(node_, rotation_, .13f);

    SendEvent(E_CURSORSTEP);
}

void EdddyCursor::SetRotation(const Quaternion& rot)
{
    node_->SetWorldRotation(rot);

    if (rotation_ != rot)
    {
        rotation_ = rot;

        SendEvent(E_CURSORSTEP);
    }
}

void EdddyCursor::Update(float /*timeStep*/)
{
    if (node_->GetAttributeAnimation("Rotation") || node_->GetAttributeAnimation("Position"))
        blockMap_->UpdateViews();
}

void EdddyCursor::HandleMouseMove()
{
    if (!GetScene())
        return;

    PODVector<Plane> planes{};
    const Vector3 position{ positionFromCoords()  };

    if (!axisLock_.test(0))
        planes.Push({ Vector3::RIGHT,   position });
    if (!axisLock_.test(1))
        planes.Push({ Vector3::UP,      position });
    if (!axisLock_.test(2))
        planes.Push({ Vector3::FORWARD, position });

    float smallestDot{ M_INFINITY };
    unsigned planeIndex{ M_MAX_UNSIGNED };
    if (axisLock_.count() == 1)
    {
        for (unsigned p{ 0u }; p < planes.Size(); ++p)
        {
            const Plane& plane{ planes.At(p) };
            const float absDot{ plane.normal_.AbsDotProduct(View3D::Active()->cameraNode()->GetWorldDirection()) };
            if (smallestDot == M_INFINITY || absDot < smallestDot)
            {
                smallestDot = absDot;
                planeIndex = p;
            }
        }

        if (smallestDot < .42f)
            planes.Erase(planeIndex);
    }

    Vector3 closestHit{ Vector3::ONE * M_INFINITY };
    for (const Plane& plane: planes)
    {
        float distance{ mouseRay_.HitDistance(plane) };
        if (distance < closestHit.DistanceToPoint(mouseRay_.origin_))
            closestHit = mouseRay_.origin_ + distance * mouseRay_.direction_;
    }

    if (closestHit.x_ != M_INFINITY)
        MoveTo(closestHit);
}

void EdddyCursor::HandleCurrentToolChanged(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    SetCoords(coords_, VectorMod(subCoords_, subStep()));
}

void EdddyCursor::DrawDebug(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    if (!GetScene() || tool()->GetType() != "Dig")
        return;
    DebugRenderer* debug{ GetScene()->GetComponent<DebugRenderer>() };
    if (!debug)
        return;

    const IntVector3 sub{ subStep() };
    const Vector3 position{ node_->GetWorldPosition() };
    const Vector3 blockSize{ blockMap_->GetBlockSize() };

    debug->AddCross(position, 1.f / Max(Max(sub.x_, sub.y_), sub.z_), Color::CYAN, false);
    for (int d{ 0 }; d < 6; ++d)
    {
        Vector3 direction{};
        switch (d) {
        default: break;
        case 0: direction = Vector3::FORWARD; break;
        case 1: direction = Vector3::RIGHT; break;
        case 2: direction = Vector3::BACK; break;
        case 3: direction = Vector3::LEFT; break;
        case 4: direction = Vector3::UP; break;
        case 5: direction = Vector3::DOWN; break;
        }

        const Vector3 start{ position + direction * blockSize * (d < 4) };
        const Vector3 end{ position + direction * blockSize * 3 };
        debug->AddLine(start, end, (d < 4 ? Color::BLUE : Color::GRAY), true);
    }
}
