/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MASTERCONTROL_H
#define MASTERCONTROL_H

#include "luckey.h"
#include "edddyevents.h"

#include <QApplication>

using namespace Dry;

class EffectMaster;
class InputMaster;
class EdddyCam;
class BlockMap;
class MainWindow;
class Project;

#define MC GetSubsystem<MasterControl>()

class MasterControl: public QApplication, public Application
{
    Q_OBJECT
    DRY_OBJECT(MasterControl, Application);

public:
    MasterControl(int& argc, char** argv, Context* context);
    virtual ~MasterControl();

    void Setup() override;
    void Start() override;
    void Stop() override;

    void RunFrame();

    void NewProject(QString path);
    void OpenProject(String path);
    void SaveCurrentProject();
    Project* CurrentProject() { return project_.Get(); }
    String trimmedResourceName(const String& fileName);

public slots:
    void OpenRecentProject() { OpenProject(sender()->objectName().toStdString().data()); }
    void RequestUpdate() { requireUpdate_ = true; }
    void OnTimeout();
    bool Quit();

private:
    void UpdateRecentProjects(const QString& latest);
    String containingFolder(const String& path);

    String argument_;
    MainWindow* mainWindow_;

    SharedPtr<Project> project_;

    bool requireUpdate_;
    bool drawDebug_;
};

#endif // MASTERCONTROL_H
