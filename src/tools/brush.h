/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef BRUSH_H
#define BRUSH_H

#include "tool.h"

class Brush : public Tool
{
    DRY_OBJECT(Brush, Tool);
public:
    Brush(Context* context);

    void Reset() override { lastCoords_ = IntVector3::ONE * M_MAX_INT ; }
    void Apply(bool shiftDown, bool ctrlDown, bool altDown) override;
    void UpdatePreview(bool shiftDown, bool ctrlDown, bool altDown, bool force = false) override;

private:
    IntVector3 lastCoords_;
};

#endif // BRUSH_H
