/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../editmaster.h"
#include "../edddycursor.h"
#include "../blockmap/blockmap.h"

#include "brush.h"

Brush::Brush(Context* context): Tool(context),
    lastCoords_{ IntVector3::ONE * M_MAX_INT }
{
    Init();
}

void Brush::Apply(bool shiftDown, bool ctrlDown, bool altDown)
{
    const IntVector3 cursorCoords{ cursor()->GetCoords() };

    if (shiftDown && IsLastTool())
    {
        QUndoStack& stack{ EM->GetActiveBlockMap()->undoStack() };
        const HashSet<IntVector3> line{ BresenhamLine(lastCoords_, cursorCoords) };

        if (line.Size() > 1)
        {
            if (isEraser())
                stack.beginMacro("Erase line");
            else
                stack.beginMacro("Draw line");
        }

        for (const IntVector3& coords: line)
        {
            if (coords != lastCoords_)
                EM->PutBlock(coords);
        }

        if (line.Size() > 1)
            stack.endMacro();
    }
    else
    {
        EM->PutBlock(cursorCoords);
    }

    lastCoords_ = cursorCoords;
}

void Brush::UpdatePreview(bool shiftDown, bool ctrlDown, bool altDown, bool /*force*/)
{
    Tool::UpdatePreview(shiftDown, ctrlDown, altDown);

    if (cursor()->IsHidden())
        return;

    const IntVector3 cursorCoords{ cursor()->GetCoords() };
    const Quaternion rotation{ cursor()->GetRotation() };

    if (shiftDown && IsLastTool() && lastCoords_.x_ != M_MAX_INT)
    {
        BlockMap* currentBlockMap{ cursor()->GetBlockMap() };

        if (!currentBlockMap)
            return;

        for (const IntVector3& coords: BresenhamLine(lastCoords_, cursorCoords))
        {
            if ((coords != cursorCoords || isEraser()) && coords != lastCoords_)
                cursor()->AddPreviewNode(coords, rotation);
        }
    }
    else if (isEraser())
    {
        cursor()->AddPreviewNode(cursorCoords, rotation);
    }
}
