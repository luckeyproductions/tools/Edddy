/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef TOOL_H
#define TOOL_H

#include "../luckey.h"

class EdddyCursor;

class Tool: public Object
{
    DRY_OBJECT(Tool, Object);

public:
    template <class T> static Tool* GetTool()
    {
        StringHash toolType{ T::GetTypeInfoStatic()->GetType() };

        return Tool::GetTool(toolType);
    }

    static Tool* GetTool(StringHash toolType)
    {
        if (tools_.Contains(toolType))
            return tools_[toolType].Get();
        else
            return nullptr;
    }

    Tool(Context* context);

    virtual void Reset() {};
    virtual void Apply(bool shiftDown, bool ctrlDown, bool altDown) = 0;
    virtual void Release() {}
    virtual void UpdatePreview(bool shiftDown, bool ctrlDown, bool altDown, bool force = false);
    void UpdatePreview(bool force = false);

    bool IsLastTool() const;

    IntVector3 subStep() const { return subStep_; }

protected:
    static HashMap<StringHash, WeakPtr<Tool>> tools_;

    void Init();

    HashSet<IntVector3> BresenhamLine(const IntVector3& start, const IntVector3& end) const;
    EdddyCursor* cursor() const;
    unsigned ActiveLayer() const;
    bool isEraser() const;

    IntVector3 subStep_;
};

#endif // TOOL_H
