/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../editmaster.h"
#include "../edddycursor.h"
#include "../blockmap/blockmap.h"

#include "fill.h"

Fill::Fill(Context* context): Tool(context),
    targetBlock_{ nullptr },
    targetRotation_{},
    replacementBlock_{ nullptr },
    lastPreviewArea_{},
    lastLock_{},
    lastPreviewRotation_{ Quaternion::IDENTITY },
    lastRotationMatch_{ false }
{
    Init();
}

void Fill::Apply(bool shiftDown, bool ctrlDown, bool altDown)
{
    const IntVector3 cursorCoords{ cursor()->GetCoords() };

    BlockInstance* instance{ cursor()->GetBlockMap()->GetBlockInstance(cursorCoords, ActiveLayer()) };
    targetBlock_ = instance ? instance->GetBlock() : nullptr;
    targetRotation_ = instance ? instance->GetRotation() : Quaternion::IDENTITY;
    replacementBlock_ = EM->GetCurrentBlock();

    HashSet<IntVector3> area{};

    Flood(cursorCoords, area, shiftDown, ctrlDown);

    QUndoStack& stack{ EM->GetActiveBlockMap()->undoStack() };
    if (area.Size() > 1u)
    {
        if (isEraser())
            stack.beginMacro("Erase area");
        else
            stack.beginMacro("Fill area");
    }

    for (const IntVector3& coords: area)
        EM->PutBlock(coords);

    if (area.Size() > 1u)
        stack.endMacro();

    cursor()->ClearPreview();
}

void Fill::Flood(const IntVector3& coords, HashSet<IntVector3>& area, bool invertLock, bool matchRotation)
{
    if (area.Contains(coords))
        return;

    BlockMap* currentBlockMap{ EM->GetActiveBlockMap() };
    Block* oldBlock{ nullptr };
    BlockInstance* instance{ currentBlockMap->GetBlockInstance(coords, ActiveLayer()) };
    bool rotationMatch{ true };

    if (instance)
    {
        oldBlock = instance->GetBlock();

        if (matchRotation)
        {
            const Vector3 targetDirection{ targetRotation_ * Vector3::FORWARD };
            const Vector3 targetUp{ targetRotation_ * Vector3::UP };
            const Quaternion instanceRotation{ instance->GetRotation() };
            const Vector3 instanceDirection{ instanceRotation * Vector3::FORWARD };
            const Vector3 instanceUp{ instanceRotation * Vector3::UP };

            if (!Equals((instanceDirection).DistanceToPoint(targetDirection), 0.f) ||
                !Equals((instanceUp).DistanceToPoint(targetUp), 0.f))
                rotationMatch = false;
        }
    }

    if ((currentBlockMap->Contains(coords) &&
        oldBlock == targetBlock_ && oldBlock != replacementBlock_) &&
        rotationMatch)
    {
        area.Insert(coords);

        for (int i{ 0 }; i < 6; ++i)
        {
            IntVector3 direction{};

            switch (i)
            {
            default: break;
            case 0: direction = IntVector3::RIGHT;   break;
            case 1: direction = IntVector3::LEFT;    break;
            case 2: direction = IntVector3::UP;      break;
            case 3: direction = IntVector3::DOWN;    break;
            case 4: direction = IntVector3::FORWARD; break;
            case 5: direction = IntVector3::BACK;    break;
            }

            std::bitset<3> lock{ cursor()->GetAxisLock() };

            direction.x_ *= lock[0] ^ invertLock;
            direction.y_ *= lock[1] ^ invertLock;
            direction.z_ *= lock[2] ^ invertLock;

            if (direction == IntVector3::ZERO)
                continue;

            Flood(coords + direction, area, invertLock, matchRotation);
        }
    }
}

void Fill::UpdatePreview(bool shiftDown, bool ctrlDown, bool altDown, bool force)
{
    BlockMap* currentBlockMap{ cursor()->GetBlockMap() };
    if (!currentBlockMap)
        return;

    force |= ctrlDown != lastRotationMatch_;
    lastRotationMatch_ = ctrlDown;

    if (!cursor()->IsHidden())
    {
        const IntVector3 cursorCoords{ cursor()->GetCoords() };
        const Quaternion rot{ cursor()->GetRotation() };
        const IntVector3 lock{ cursor()->GetLockVector() };
        const IntVector3 lockEffective{ (shiftDown ? IntVector3::ONE - lock : lock) };

        if (force || !lastPreviewArea_.Contains(cursorCoords) || lockEffective != lastLock_)
        {
            BlockInstance* instance{ currentBlockMap->GetBlockInstance(cursorCoords, ActiveLayer()) };
            targetBlock_ = instance ? instance->GetBlock() : nullptr;
            targetRotation_ = instance ? instance->GetRotation() : Quaternion::IDENTITY;
            replacementBlock_ = EM->GetCurrentBlock();

            Tool::UpdatePreview(shiftDown, ctrlDown, altDown);
            HashSet<IntVector3> area{};

            Flood(cursorCoords, area, shiftDown, ctrlDown);

            for (const IntVector3& coords: area)
                cursor()->AddPreviewNode(coords, rot);

            lastPreviewArea_ = area;
            lastLock_ = lockEffective;
            lastPreviewRotation_ = rot;
        }
        else if (!isEraser() && lastPreviewRotation_ != rot && !lastPreviewArea_.IsEmpty())
        {
            for (Node* n: cursor()->GetPreviewNodes())
                n->SetWorldRotation(rot);

            lastPreviewRotation_ = rot;
        }
    }

    MC->RequestUpdate();
}
