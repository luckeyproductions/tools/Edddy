/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../editmaster.h"
#include "../edddycursor.h"
#include "../blockmap/blockmap.h"
#include "../undostack/digcommand.h"

#include "dig.h"

Dig::Dig(Context* context): Tool(context),
    affectedCoordinates_{},
    tileCorners_{},
    startHeight_{ M_MAX_INT },
    startSub_{},
    spanning_{ false }
{
    Init();
    subStep_ = { 2, 1, 2 };

    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(Dig, DrawDebug));
    SubscribeToEvent(E_CURSORSTEP, DRY_HANDLER(Dig, HandleCursorStep));
}

void Dig::Apply(bool shiftDown, bool ctrlDown, bool altDown)
{
    /// Shift: Include neighbour corners
    ///
    /// Ctrl: Set rect
    /// Alt: Merge/Split
    /// Ctrl+Alt: Rotate normal

    if (tileCorners_.empty())
    {
        if (affectedCoordinates_.IsEmpty())
            return;

        BlockMap* bm{ cursor()->GetBlockMap() };
        const IntVector3 cursorCoords{ cursor()->GetCoords() };
        const PODVector<TileInstance*> tilesAtCursor{ bm->GetTileInstances(cursorCoords) };
        if (tilesAtCursor.IsEmpty())
            return;

        TileInstance* primaryTile{ tilesAtCursor.Front() };
        const IntVector3 cursorSubCoords{ cursor()->subCoords() };
        const HashSet<IntVector3> effectiveFlatCursorCoords{ collectFlatCursorCoords(cursorCoords, cursorSubCoords) };
        const HashSet<IntVector3> neighbours{ (shiftDown ? collectNeighbours(cursorSubCoords)
                                                         : HashSet<IntVector3>{ IntVector3::ZERO }) };

        const std::map<TileInstance*, HashSet<IntVector3>> tilesAndCorners{ bm->GetTilesAndCorners() };

        for (const auto& kv: tilesAndCorners)
        {
            TileInstance* tile{ kv.first };
            if (shiftDown || tile == primaryTile)
            {
                for (const IntVector3& coords: affectedCoordinates_)
                {
                    if (kv.second.Contains(coords))
                    {
                        if (!tileCorners_.contains(tile))
                            tileCorners_.insert({ tile, {} });

                        tileCorners_.find(tile)->second.Insert(tile->indexFromCornerCoords(coords));
                    }
                }
            }
        }

        previousAxisLock_ = cursor()->GetAxisLock();
        if (ctrlDown)
        {
            cursor()->SetAxisLock(0b101);

            if (!altDown)
            {
                spanning_ = true;
                subStep_ = { 1, 1, 1 };
            }
        }
        else
        {
            cursor()->SetAxisLock(0b010);
        }

        startHeight_ = cursorCoords.y_;
        startSub_ = sub();
    }
}

void Dig::HandleCursorStep(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    if (!EM || cursor()->tool() != this || !tileCorners_.empty())
        return;

    updateAffectedCoordinates();
}

void Dig::updateAffectedCoordinates()
{
    affectedCoordinates_.Clear();

    if (!cursor())
        return;
    BlockMap* bm{ cursor()->GetBlockMap() };
    if (!bm)
        return;
    PODVector<TileInstance*> tiles{ bm->GetTileInstances(cursor()->GetCoords()) };
    if (tiles.IsEmpty())
        return;

    TileInstance* primaryTile{ tiles.Front() };

    const IntVector3 cursorCoords{ cursor()->GetCoords() };
    const HashSet<IntVector3> effectiveFlatCursorCoords{ collectFlatCursorCoords(cursorCoords, cursor()->subCoords()) };

    HashSet<IntVector3> relevant{};
    Pair<int, int> range{ M_MAX_INT, M_MIN_INT };
    for (const IntVector3& efcc: effectiveFlatCursorCoords)
    {
        if (effectiveFlatCursorCoords.Size() != 2u)
        {
            for (const IntVector3& corCor: primaryTile->cornerCoordinates())
            {
                if (effectiveFlatCursorCoords.Size() == 1u)
                {
                    if (corCor.x_ == efcc.x_ && corCor.z_ == efcc.z_ && corCor.y_ == cursorCoords.y_)
                        affectedCoordinates_.Insert(corCor);
                }
                else
                {
                    if (corCor.x_ == efcc.x_ && corCor.z_ == efcc.z_)
                        affectedCoordinates_.Insert(corCor);
                }
            }
        }
        else
        {
            for (const IntVector3& corCor: primaryTile->cornerCoordinates())
            {
                if (corCor.x_ == efcc.x_ && corCor.z_ == efcc.z_)
                {
                    range.first_  = Min(range.first_,  corCor.y_);
                    range.second_ = Max(range.second_, corCor.y_);
                    relevant.Insert(corCor);
                }
            }
        }
    }

    for (const IntVector3& corCor: relevant)
    {
        if (cursorCoords.y_ >= range.first_ && cursorCoords.y_ <= range.second_)
            affectedCoordinates_.Insert(corCor);
    }
}

void Dig::Release()
{
    if (!tileCorners_.empty())
    {
        BlockMap* bm{ cursor()->GetBlockMap() };
        const int difference{ cursor()->GetCoords().y_ - startHeight_ };
        const IntVector2 lateral{ sub() - startSub_ };
        PODVector<DigCommand*> commands{};

        for (TileInstance* tile: tiles())
        {
            HeightProfile profile{ tile->heightProfile() };
            const IntVector3 tileCoords{ tile->GetCoords() };
            const Pair<int, int> limit{ -tileCoords.y_, bm->GetMapHeight() - tileCoords.y_ - 1 };
            const HashSet<int>& corners{ tileCorners_.find(tile)->second };

            if (!spanning_)
            {
                for (int c: corners)
                {
                    const int targetHeight{ Min(Max(profile.at(c) + difference, limit.first_), limit.second_) };
                    profile.set(c, targetHeight);
                    profile.setNormalInt(c, profile.normals().At(c) + lateral);
                }
            }
            else
            {
                IntRect span{ profile.span() };
                span.right_ += lateral.x_;
                span.top_ += lateral.y_;

                profile.setSpan(span);
            }

            if (tile->heightProfile() != profile)
            {
                DigCommand* command{ new DigCommand{ bm, tileCoords, tile->GetLayer() } };
                command->setOldState(tile->heightProfile());
                command->setNewState(profile);
                commands.Push(command);
            }
        }

        if (commands.Size() > 1u)
            bm->undoStack().beginMacro("Dig");

        for (DigCommand* command: commands)
            bm->undoStack().push(command);

        if (commands.Size() > 1)
            bm->undoStack().endMacro();

        tileCorners_.clear();

        cursor()->SetAxisLock(previousAxisLock_);

        spanning_ = false;
        subStep_ = { 2, 1, 2 };
    }
}

void Dig::DrawDebug(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    if (tileCorners_.empty())
        return;

    Scene* scene{ cursor()->GetScene() };
    if (!scene)
        return;

    DebugRenderer* debug{ scene->GetComponent<DebugRenderer>() };
    BlockMap* bm{ cursor()->GetBlockMap() };

    if (!debug || !bm)
        return;

    const int cursorCoordsY{ cursor()->GetCoords().y_ };
    const int difference{ cursorCoordsY - startHeight_ };

    for (TileInstance* tile: tiles())
    {
        const IntVector3& tileCoords{ tile->GetCoords() };
        const HeightProfile& profile{ tile->heightProfile() };
        const Vector3 halfBlock{ bm->GetBlockSize() * .5f };
        const HashSet<int>& corners{ tileCorners_.find(tile)->second };

        for (int c: corners)
        {
            const IntVector3 corCor{ tile->cornerCoordinate(c) };
            const Pair<int, int> limit{ -tileCoords.y_, bm->GetMapHeight() - tileCoords.y_ - 1 };
            const int targetHeight{ Min(Max(profile.at(c) + difference, limit.first_), limit.second_) };
            const Vector3 position{ IntVector3{ corCor.x_, tileCoords.y_, corCor.z_ } + IntVector3::UP * targetHeight - halfBlock};
            debug->AddCross(position, .25f, (difference == 0 ? Color::YELLOW : Color::ORANGE), false);
        }
    }
}

IntVector2 Dig::sub()
{
    const IntVector3 sub3{ cursor()->GetCoords() * cursor()->subStep() + cursor()->subCoords() };
    return { sub3.x_, sub3.z_ };
}

PODVector<TileInstance*> Dig::tiles()
{
    PODVector<TileInstance*> res{};
    for (const auto& kv: tileCorners_)
        res.Push(kv.first);
    return res;
}
