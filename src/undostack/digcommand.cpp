/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../blockmap/gridblock.h"
#include "../blockmap/blockset.h"
#include "../editmaster.h"
#include "../tools/tool.h"

#include "digcommand.h"

DigCommand::DigCommand(BlockMap* blockMap, const IntVector3& coords, unsigned layer): QUndoCommand(),
    blockMap_{ blockMap },
    coords_{ coords },
    layer_{ layer },
    oldProfile_{},
    newProfile_{}
{
    setText("Dig");
}

void DigCommand::redo()
{
    blockMap_->SetProfile(coords_, layer_, newProfile_);

    if (isDigging())
        blockMap_->updateCenters();
}

void DigCommand::undo()
{
    blockMap_->SetProfile(coords_, layer_, oldProfile_);

    if (isDigging())
        blockMap_->updateCenters();
}

void DigCommand::setOldState(const HeightProfile& profile)
{
    oldProfile_ = profile;
}

void DigCommand::setNewState(const HeightProfile& profile)
{
    newProfile_ = profile;
}

bool DigCommand::isDigging() const
{
    if (Tool* tool{ blockMap_->GetSubsystem<EditMaster>()->GetTool() })
        return tool->GetType() == "Dig";
    else
        return false;
}
