/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef GRIDBLOCKCOMMAND_H
#define GRIDBLOCKCOMMAND_H

#include <QUndoCommand>

#include "../luckey.h"
#include "../blockmap/blockmap.h"

class EditMaster;

class GridBlockCommand: public QUndoCommand
{
public:
    GridBlockCommand(BlockMap* map, const IntVector3& coords, unsigned layer);

    void redo() override;
    void undo() override;

    void setOldState(Block* block, const Quaternion& rotation, const HeightProfile& elevation = {});
    void setNewState(Block* block, const Quaternion& rotation);

    bool isChange() const
    {
        return oldRotation_ != newRotation_ ||
               oldBlocksetIndex_ != newBlocksetIndex_ ||
               oldBlockId_ != newBlockId_;
    }

private:
    EditMaster* editMaster() const;

    BlockMap* blockMap_;
    IntVector3 coords_;
    unsigned layer_;
    Quaternion oldRotation_;
    Quaternion newRotation_;
    unsigned oldBlocksetIndex_;
    unsigned newBlocksetIndex_;
    unsigned oldBlockId_;
    unsigned newBlockId_;
    HeightProfile elevation_;
};

#endif // GRIDBLOCKCOMMAND_H
