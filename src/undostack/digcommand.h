/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef DIGCOMMAND_H
#define DIGCOMMAND_H

#include <QUndoCommand>

#include "../luckey.h"
#include "../blockmap/blockmap.h"

class EditMaster;

class DigCommand: public QUndoCommand
{
public:
    DigCommand(BlockMap* map, const IntVector3& coords, unsigned layer);

    void redo() override;
    void undo() override;

    void setOldState(const HeightProfile& profile);
    void setNewState(const HeightProfile& profile);

    bool isChange() const
    {
        return oldProfile_ != newProfile_;
    }

private:
    bool isDigging() const;

    BlockMap* blockMap_;
    IntVector3 coords_;
    unsigned layer_;
    HeightProfile oldProfile_;
    HeightProfile newProfile_;
};

#endif // DIGCOMMAND_H
