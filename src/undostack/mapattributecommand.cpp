/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "mapattributecommand.h"

MapAttributeCommand::MapAttributeCommand(BlockMap* blockMap, const String& name): QUndoCommand(),
    blockMap_{ blockMap },
    attributeName_{ name },
    oldValue_{},
    newValue_{}
{
    setText("Change " + DQ(attributeName_) + " attribute");
}

void MapAttributeCommand::redo()
{
    MapAttributes attributes{ blockMap_->attributes() };

    for (unsigned a{ 0u }; a < attributes.Size(); ++a)
    {
        if (attributeName_ == attributes.At(a).first_)
            attributes.Erase(a);
    }

    if (!newValue_.IsEmpty())
        attributes.Push({ attributeName_, newValue_ });

    blockMap_->setAttributes(attributes);
}

void MapAttributeCommand::undo()
{
    MapAttributes attributes{ blockMap_->attributes() };

    for (unsigned a{ 0u }; a < attributes.Size(); ++a)
    {
        if (attributeName_ == attributes.At(a).first_)
            attributes.Erase(a);
    }

    if (!oldValue_.IsEmpty())
        attributes.Push({ attributeName_, oldValue_ });

    blockMap_->setAttributes(attributes);
}

void MapAttributeCommand::setOldState(const Variant& var)
{
    oldValue_ = var;
}

void MapAttributeCommand::setNewState(const Variant& var)
{
    newValue_ = var;
}
