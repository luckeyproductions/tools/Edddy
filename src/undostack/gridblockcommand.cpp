/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../blockmap/gridblock.h"
#include "../blockmap/blockset.h"
#include "../editmaster.h"

#include "gridblockcommand.h"

GridBlockCommand::GridBlockCommand(BlockMap* blockMap, const IntVector3& coords, unsigned layer): QUndoCommand(),
    blockMap_{ blockMap },
    coords_{ coords },
    layer_{ layer },
    oldRotation_{},
    newRotation_{},
    oldBlocksetIndex_{ M_MAX_UNSIGNED },
    newBlocksetIndex_{ M_MAX_UNSIGNED },
    oldBlockId_{ M_MAX_UNSIGNED },
    newBlockId_{ M_MAX_UNSIGNED },
    elevation_{}
{
    setText("Put block");
}

void GridBlockCommand::redo()
{
    Block* block{ nullptr };
    if (newBlocksetIndex_ != M_MAX_UNSIGNED && newBlockId_ != M_MAX_UNSIGNED)
        block = editMaster()->getBlocksetByIndex(newBlocksetIndex_)->GetBlockById(newBlockId_);

    blockMap_->SetGridBlock(coords_, newRotation_, layer_, block);
}

void GridBlockCommand::undo()
{
    Block* block{ nullptr };
    if (oldBlocksetIndex_ != M_MAX_UNSIGNED && oldBlockId_ != M_MAX_UNSIGNED)
        block = editMaster()->getBlocksetByIndex(oldBlocksetIndex_)->GetBlockById(oldBlockId_);

    blockMap_->SetGridBlock(coords_, oldRotation_, layer_, block);

    if (block && block->type() == BLOCK_TILE)
        blockMap_->SetProfile(coords_, layer_, elevation_);
}

void GridBlockCommand::setOldState(Block* block, const Quaternion& rotation, const HeightProfile& elevation)
{
    if (block)
    {
        oldRotation_ = rotation;
        oldBlockId_ = block->id();
        oldBlocksetIndex_ = editMaster()->globalBlocksetIndex(block);
        elevation_ = elevation;
    }
}

void GridBlockCommand::setNewState(Block* block, const Quaternion& rotation)
{
    if (block)
    {
        newRotation_ = rotation;
        newBlockId_ = block->id();
        newBlocksetIndex_ = editMaster()->globalBlocksetIndex(block);
    }
    else
    {
        setText("Erase block");
    }
}

EditMaster* GridBlockCommand::editMaster() const
{
    return blockMap_->GetSubsystem<EditMaster>();
}
