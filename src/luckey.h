/*
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef LUCKEY_H
#define LUCKEY_H

#include "dry.h"
#include <QTreeWidgetItem>
#include <QListWidgetItem>

#include <initializer_list>
#include <bitset>
#include "mastercontrol.h"

#define ENGINE GetSubsystem<Engine>()
#define TIME GetSubsystem<Time>()
#define INPUT GetSubsystem<Input>()
#define GRAPHICS GetSubsystem<Graphics>()
#define RENDERER GetSubsystem<Renderer>()
#define AUDIO GetSubsystem<Audio>()
#define GUI GetSubsystem<UI>()
#define FS GetSubsystem<FileSystem>()

#define RES(x, y) GetSubsystem<ResourceCache>()->GetResource<x>(y)

#define QD(str) String{ str.toStdString().data() }
#define DQ(str) QString{ str.CString() }

namespace Dry {
class Drawable;
class Node;
class Scene;
class Sprite;
class Viewport;
class RenderPath;
class Camera;
}

using namespace Dry;

enum UserRoles{ FileName = Qt::UserRole, DryTypeHash, ID, PointerRole, StringRole };

template <class T>
T* extractPointer(QTreeWidgetItem* item)
{
    return qobject_cast<T*>(qvariant_cast<QObject*>(item->data(0, PointerRole)));
}

template <class T>
T* extractPointer(QListWidgetItem* item)
{
    return qobject_cast<T*>(qvariant_cast<QObject*>(item->data(PointerRole)));
}

static QStringList toStringList(const StringVector& strVec)
{
    QStringList list{};
    for (const String& string: strVec)
        list.push_back(DQ(string));
    return list;
}

static StringVector toStringVector(const QStringList& strLst)
{
    StringVector vec{};
    for (const QString& string: strLst)
        vec.Push(QD(string));
    return vec;
}

namespace LucKey {

float Delta(float lhs, float rhs, bool angle = false);
float RandomSign();
Color RandomColor();
Color RandomSkinColor();
Color RandomHairColor(bool onlyNatural = false);

float Sine(float x);
float Cosine(float x);

int Cycle(int x, int min, int max);
float Cycle(float x, float min, float max);
}
using namespace LucKey;

#endif // LUCKEY_H
