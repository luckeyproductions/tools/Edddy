/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "mainwindow.h"
#include "editmaster.h"
#include "blockmap/blockset.h"
#include "blockmap/blockmap.h"
#include "docks/projectpanel.h"

#include "project.h"

void Project::RegisterObject(Context* context)
{
    context->RegisterFactory<Project>();

    DRY_ATTRIBUTE("Location", String, location_, "", AM_EDIT);
    DRY_ATTRIBUTE("FileName", String, fileName_, "", AM_EDIT);
    DRY_ATTRIBUTE_EX("DisplayName", String, displayName_, OnDisplayNameSet, "", AM_FILE);
    DRY_ATTRIBUTE_EX("ResourcesFolder", String, resourceFolder_, OnFolderSet, "Resources/", AM_FILE);
    DRY_ATTRIBUTE_EX("Command", String, command_, OnCommandSet, "", AM_FILE);
    DRY_ATTRIBUTE("Default Map Size", IntVector3, defaultMapSize_, IntVector3(23, 5, 23), AM_FILE);
    DRY_ATTRIBUTE("Default Block Size", IntVector3, defaultBlockSize_, Vector3(1.f, 1.f, 1.f), AM_FILE);
}

Project::Project(Context* context): Serializable(context),
    displayName_{},
    fileName_{},
    location_{},
    previousResourceFolder_{},
    resourceFolder_{},
    command_{},
    hiddenFolders_{},
    defaultMapSize_{ 23, 5, 23 },
    defaultBlockSize_{ 1.f, 1.f, 1.f },
    mapAttributes_{},
    unsavedChanges_{ true }
{
    ResetToDefault();
}

bool Project::Close()
{
    if (isModified())
    {
        ModifiedBox* modifiedBox{ new ModifiedBox{ context_ } };

        switch (modifiedBox->exec())
        {
        case QMessageBox::Save:             return Save();  break;
        case QMessageBox::Discard:          return true;    break;
        case QMessageBox::Cancel: default:  return false;   break;
        }
    }

    return true;
}

void Project::OnDisplayNameSet()
{
    if (!unsavedChanges_)
    {
        unsavedChanges_ = true;
        MainWindow::mainWindow_->updateWindowTitle();
    }

    GetSubsystem<ProjectPanel>()->updatePanel(false);
}

void Project::OnCommandSet()
{
    if (!unsavedChanges_)
    {
        unsavedChanges_ = true;
        MainWindow::mainWindow_->updateWindowTitle();
    }

    GetSubsystem<ProjectPanel>()->updatePanel(false);
}

void Project::OnResourcesFolderSet()
{
    if (location_.IsEmpty())
        return;

    if (resourceFolder_ != previousResourceFolder_)
    {
        ResourceCache* cache{ GetSubsystem<ResourceCache>() };

        if (cache->GetResourceDirs().Contains(location_ + previousResourceFolder_))
            cache->RemoveResourceDir(location_ + previousResourceFolder_);

        cache->ReleaseAllResources();
        cache->AddResourceDir(location_ + resourceFolder_);
        previousResourceFolder_ = resourceFolder_;
    }
}

void Project::OnFolderSet()
{
    if (resourceFolder_ != previousResourceFolder_)
    {
        OnResourcesFolderSet();
        GetSubsystem<ProjectPanel>()->updateBrowser();
    }

    if (!unsavedChanges_)
    {
        unsavedChanges_ = true;
        MainWindow::mainWindow_->updateWindowTitle();
    }
}

bool Project::Load(const String& fileName)
{
    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    XMLFile* projectXML{ cache->GetResource<XMLFile>(fileName) };

    if (!projectXML || !LoadXML(projectXML->GetRoot("project")))
        return false;

    unsavedChanges_ = false;
    return true;
}

bool Project::LoadXML(const XMLElement& source)
{
    Serializable::LoadXML(source);

    XMLElement attributesElem{ source.GetChild("mapattributes")};
    if (!attributesElem.IsNull())
        loadMapAttributes(attributesElem);

    hiddenFolders_.clear();
    const XMLElement settingsElem{ source.GetChild("settings") };
    if (XMLElement hiddenFoldersElem{ settingsElem.GetChild("hiddenfolders") })
        hiddenFolders_ = toStringList(hiddenFoldersElem.GetStringVector());

    return true;
}

bool Project::Save()
{
    if (!isModified())
        return true;
    
    XMLFile* projectXML{ new XMLFile{ MC->GetContext() } };
    XMLElement rootElem{ projectXML->CreateRoot("project") };

    if (!SaveXML(rootElem) || !projectXML->SaveFile(fullPath()))
        return false;

    unsavedChanges_ = false;

    for (Blockset* bs: EM->GetBlocksets())
    {
        if (bs->isModified() && !bs->Save())
            return false;
    }

    for (BlockMap* bm: EM->GetBlockMaps())
    {
        if (bm->isModified() && !bm->Save())
            return false;
    }

    MainWindow::mainWindow_->handleFileModifiedStateChange();

    return true;
}

bool Project::SaveXML(XMLElement& dest) const
{
    if (!Serializable::SaveXML(dest))
        return false;

    XMLElement mapAttributesElem{ dest.CreateChild("mapattributes") };
    saveMapAttributes(mapAttributesElem);

    XMLElement hiddenFoldersElem{ dest.CreateChild("settings") };
    saveSettings(hiddenFoldersElem);

    return true;
}

void Project::saveSettings() const
{
    const String fullFilename{ location_ + fileName_ };
    XMLFile* projectXML{ GetSubsystem<ResourceCache>()->GetResource<XMLFile>(fullFilename) };
    if (!projectXML)
        return;

    XMLElement rootElem{ projectXML->GetRoot("project") };
    XMLElement hiddenFoldersElem{ rootElem.GetChild("settings") };
    if (hiddenFoldersElem.IsNull())
        hiddenFoldersElem = rootElem.CreateChild("settings");
    else
        hiddenFoldersElem.RemoveChildren();

    saveSettings(hiddenFoldersElem);

    projectXML->SaveFile(fullFilename);
}

void Project::saveSettings(XMLElement& dest) const
{
    dest.CreateChild("hiddenfolders").SetVariant(toStringVector(hiddenFolders_));
}

void Project::saveMapAttributes(XMLElement& dest) const
{
    for (const Pair<String, Variant>& attribute: mapAttributes_)
    {
        XMLElement attrElem{ dest.CreateChild("attribute") };
        attrElem.SetAttribute("name", attribute.first_);
        attrElem.SetAttribute("type", attribute.second_.GetTypeName());
        attrElem.SetAttribute("default", attribute.second_.ToString());
    }
}

void Project::loadMapAttributes(const XMLElement& source)
{
    XMLElement attrElem{ source.GetChild("attribute") };

    while (!attrElem.IsNull())
    {
        Pair<String, Variant> attribute{};
        attribute.first_ = attrElem.GetAttribute("name");
        attribute.second_ = Variant{ attrElem.GetAttribute("type"), attrElem.GetAttribute("default") };
        mapAttributes_.Push(attribute);

        attrElem = attrElem.GetNext("attribute");
    }
}

bool Project::isModified() const
{
    if (unsavedChanges_)
        return true;

    for (Blockset* bs: EM->GetBlocksets())
    {
        if (bs->isModified())
            return true;
    }

    for (BlockMap* bm: EM->GetBlockMaps())
    {
        if (bm->isModified())
            return true;
    }

    return false;
}


String Project::location() const
{
    return location_;
}

String Project::fullPath()
{
    return location_ + fileName_;
}

QString Project::trimmedName() const
{
    return GetFileName(fileName_).CString();
}

QString Project::displayName() const
{
    const QString displayName{ DQ(GetAttribute("DisplayName").GetString()) };
    return displayName.isEmpty() ? trimmedName() : displayName;
}

String Project::resourceFolder(bool absolute) const
{
    return (absolute ? location_ : "") + resourceFolder_;
}

String Project::command() const
{
    return GetAttribute("Command").GetString();
}

bool Project::inResourceFolder(const String& fileName) const
{
    if (fileName.StartsWith(resourceFolder(true)))
        return true;

    return false;
}

String Project::localizedPath(const String& path) const
{
    if (inResourceFolder(path))
        return path.Replaced(resourceFolder(true), "");
    else
        return path.Replaced(location_, "");
}

ModifiedBox::ModifiedBox(Context* context, QWidget* parent): QMessageBox(parent), Object(context)
{
    setWindowTitle("Unsaved changes");
    setWindowIcon(QIcon(":/Edddy"));
    setIcon(QMessageBox::Information);
    setText("The current project has been modified.");
    setInformativeText("Do you want to save your changes?");
    setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
    setDefaultButton(QMessageBox::Save);
    QString changeList{ "Modified files:\n" };

    Project* p{ MC->CurrentProject() };
    if (p->Unsaved())
    {
        changeList.append(p->GetAttribute("FileName").GetString().CString());
        changeList.append('\n');
    }

    for (BlockMap* bm: EM->GetBlockMaps())
    {
        if (bm->isModified())
        {
            changeList.append(bm->name().CString());
            changeList.append('\n');
        }
    }

    for (Blockset* bs: EM->GetBlocksets())
    {
        if (bs->isModified())
        {
            changeList.append(bs->name_.CString());
            changeList.append('\n');
        }
    }

    setDetailedText(changeList);
}
