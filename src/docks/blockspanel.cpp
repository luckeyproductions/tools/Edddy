/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QMessageBox>

#include "../blockmap/blockmap.h"
#include "../blockmap/blockinstance.h"
#include "../dialogs/blockdialog.h"
#include "../blocklistitem.h"
#include "../mainwindow.h"
#include "../editmaster.h"
#include "../project.h"
#include "projectpanel.h"

#include "blockspanel.h"

BlocksPanel::BlocksPanel(Context* context, QWidget* parent): DryWidget(context, parent),
    splitter_{ nullptr },
    blocksetList_{ new QListWidget{} },
    blockList_{ new QListWidget{} },
    addBlocksetButton_{      new QPushButton{ "Add set" } },
    deleteBlocksetButton_{   new QPushButton{ "Delete set" } },
    previousBlocksetButton_{ new QPushButton{ "Previous set" } },
    nextBlocksetButton_{     new QPushButton{ "Next set" } },
    addBlockButton_{         new QPushButton{ "Add" } },
    editBlockButton_{        new QPushButton{ "Edit" } },
    removeBlockButton_{      new QPushButton{ "Remove" } },
    zoomSlider_{ nullptr },
    rotationSlider_{ nullptr }
{
    setObjectName("blocksPanel");
    setWindowTitle("Blocks");
    context_->RegisterSubsystem(this);

    QVBoxLayout* mainLayout{ new QVBoxLayout{} };
    mainLayout->setMargin(2);
    splitter_ = new QSplitter{ this };
    splitter_->setOrientation(Qt::Vertical);

    createTopHalf();
    createBottomHalf();

    mainLayout->addWidget(splitter_);
    setLayout(mainLayout);
    setMinimumSize(128, 192);

    connectSignals();
    handleZoomSliderValueChanged(-1);
    setEnabled(false);
    updateButtons();
}

void BlocksPanel::createTopHalf()
{
    QWidget* topHalf{ new QWidget{} };
    QVBoxLayout* topLayout{ new QVBoxLayout{} };
    topLayout->setMargin(0);

    QHBoxLayout* topSetsRow{ new QHBoxLayout{} };
    topSetsRow->addWidget(addBlocksetButton_);
    topSetsRow->addWidget(deleteBlocksetButton_);
    topLayout->addLayout(topSetsRow);

    blocksetList_->setSortingEnabled(true);
    blocksetList_->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    blocksetList_->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
    blocksetList_->setMinimumHeight(96);
    topLayout->addWidget(blocksetList_);

    topHalf->setLayout(topLayout);
    splitter_->addWidget(topHalf);
}

void BlocksPanel::createBottomHalf()
{
    QWidget* bottomHalf{ new QWidget{} };
    QVBoxLayout* bottomLayout{ new QVBoxLayout{} };
    bottomLayout->setMargin(0);

    QHBoxLayout* bottomSetsRow{ new QHBoxLayout{} };
    bottomSetsRow->addWidget(previousBlocksetButton_);
    bottomSetsRow->addWidget(nextBlocksetButton_);
    bottomLayout->addLayout(bottomSetsRow);

    const int blockZoom{ 68 };
    blockList_->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    blockList_->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    blockList_->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
    blockList_->setMovement(QListWidget::Static);
    blockList_->setFlow(QListWidget::LeftToRight);
    blockList_->setWrapping(true);
    blockList_->setResizeMode(QListWidget::Adjust);
    blockList_->setSpacing(5);
    blockList_->setGridSize(QSize(blockZoom, blockZoom));
    blockList_->setViewMode(QListWidget::IconMode);
    blockList_->setUniformItemSizes(true);
    blockList_->setWordWrap(true);
    blockList_->setSelectionRectVisible(true);
    blockList_->setSelectionMode(QListWidget::ExtendedSelection);
    bottomLayout->addWidget(blockList_);

    QHBoxLayout* bottomBlocksRow{ new QHBoxLayout{} };
    bottomBlocksRow->addWidget(addBlockButton_);
    bottomBlocksRow->addWidget(editBlockButton_);
    bottomBlocksRow->addWidget(removeBlockButton_);
    bottomLayout->addLayout(bottomBlocksRow);

    QVBoxLayout* slidersColumn{ new QVBoxLayout{} };
    for (bool zoom: { true, false })
    {
        QHBoxLayout* sliderRow{ new QHBoxLayout{} };

        for (int i{ 0 }; i < 3; ++i)
        {
            if (i == 1)
            {
                QSlider* slider{ new QSlider{} };
                slider->setOrientation(Qt::Horizontal);
                slider->setTickPosition(QSlider::TicksBothSides);

                if (zoom)
                {
                    zoomSlider_ = slider;
                    slider->setMinimum(32);
                    slider->setMaximum(128);
                    slider->setValue(blockZoom);
                    slider->setPageStep(12);
                    slider->setTickInterval(12);
                }
                else
                {
                    rotationSlider_ = slider;
                    slider->setMinimum(0);
                    slider->setMaximum(360);
                    slider->setValue(135);
                    slider->setPageStep(10);
                    slider->setTickInterval(45);
                }

                sliderRow->addWidget( slider );
            }
            else
            {
                QLabel* iconLabel{ new QLabel{} };
                iconLabel->setFixedSize({ 16, 16 });
                iconLabel->setScaledContents(true);
                iconLabel->setEnabled(false);

                switch ((i / 2) + (!zoom * 2))
                {
                case 0: iconLabel->setPixmap({ ":/Minus" }); break;
                case 1: iconLabel->setPixmap({ ":/Plus" });  break;
                case 2: iconLabel->setPixmap({ ":/Redo" });  break;
                case 3: iconLabel->setPixmap({ ":/Undo" });  break;
                default: break;
                }

                sliderRow->addWidget(iconLabel);
            }
        }

        slidersColumn->addLayout(sliderRow);
    }

    bottomLayout->addLayout(slidersColumn);
    bottomHalf->setLayout(bottomLayout);
    splitter_->addWidget(bottomHalf);
}

void BlocksPanel::connectSignals()
{
    connect(addBlocksetButton_, SIGNAL(clicked(bool)), MainWindow::mainWindow_, SLOT(newBlockset()));
    connect(deleteBlocksetButton_, SIGNAL(clicked(bool)), this, SLOT(deleteBlockset()));
    connect(blocksetList_, SIGNAL(currentRowChanged(int)), this, SLOT(handleBlocksetListCurrentItemChanged()));
    connect(blocksetList_, SIGNAL(itemSelectionChanged()), this, SLOT(handleBlocksetListCurrentItemChanged()));
    connect(blocksetList_, SIGNAL(itemChanged(QListWidgetItem*)), this, SLOT(handleBlocksetListItemChanged(QListWidgetItem*)));

    connect(nextBlocksetButton_, SIGNAL(clicked(bool)), this, SLOT(nextBlockset()));
    connect(previousBlocksetButton_, SIGNAL(clicked(bool)), this, SLOT(previousBlockset()));

    connect(blockList_, SIGNAL(currentRowChanged(int)), this, SLOT(handleBlockListCurrentRowChanged(int)));
    connect(blockList_, SIGNAL(itemSelectionChanged()), this, SLOT(handleBlockListItemSelectionChanged()));
    connect(addBlockButton_, SIGNAL(clicked(bool)), this, SLOT(editBlock()));
    connect(editBlockButton_, SIGNAL(clicked(bool)), this, SLOT(editBlock()));
    connect(removeBlockButton_, SIGNAL(clicked(bool)), this, SLOT(removeBlock()));
    connect(zoomSlider_, SIGNAL(valueChanged(int)), this, SLOT(handleZoomSliderValueChanged(int)));
    connect(rotationSlider_, SIGNAL(valueChanged(int)), this, SLOT(updateBlockPreviews()));
}

void BlocksPanel::deleteBlockset()
{
    QListWidgetItem* item{ blocksetList_->currentItem() };
    Blockset* bs{ extractPointer<Blockset>(item) };
    if (bs)
    {
        QMessageBox* confirm{ new QMessageBox{ this } };
        confirm->setWindowTitle("Delete file");
        confirm->setWindowIcon(QIcon(":/Edddy"));
        confirm->setIcon(QMessageBox::Warning);
        confirm->setText("Are you sure you want to delete this blockset?");
        confirm->setInformativeText(bs->name_.CString());
        confirm->setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
        confirm->setDefaultButton(QMessageBox::Cancel);

        if (confirm->exec() == QMessageBox::Cancel)
            return;

        if (bs->savedFullPath_.IsEmpty())
            EM->DeleteBlockset(bs);
        else if (EM->DeleteBlockset(bs->savedFullPath_))
            GetSubsystem<ProjectPanel>()->updateBrowser();
    }
}

void BlocksPanel::handleCurrentBlocksetChanged(Blockset* currentBlockset)
{
    blockList_->clear();

    if (!currentBlockset)
    {
        blocksetList_->setCurrentRow(-1);
        deleteBlocksetButton_->setEnabled(false);
    }
    else
    {
        for (int i{ 0 }; i < blocksetList_->count(); ++i)
        {
            QListWidgetItem* item{ blocksetList_->item(i) };

            if (currentBlockset == extractPointer<Blockset>(item))
            {
                blocksetList_->setCurrentItem(item);
                break;
            }
        }

        if (currentBlockset->blocks().Size())
        {
            for (Block* b: *currentBlockset)
            {
                BlockListItem* blockItem{ new BlockListItem{ context_, b } };
                blockItem->setSizeHint(blockList_->gridSize());

                blockList_->addItem(blockItem);
            }
        }

        updateBlockList();
        deleteBlocksetButton_->setEnabled(true);
    }
}

void BlocksPanel::handleBlockListCurrentRowChanged(int currentRow)
{
    if (currentRow > -1)
        EM->SetCurrentBlock(extractPointer<Block>(blockList_->item(currentRow)));
    else
        EM->SetCurrentBlock(nullptr);

    updateButtons();
}

void BlocksPanel::updateBlockList()
{
    Block* currentBlock{ EM->GetCurrentBlock() };
    QListWidget* list{ blockList_ };

    if (currentBlock == nullptr)
    {
        if (list->currentRow() != -1 || !list->selectedItems().isEmpty())
            list->setCurrentRow(-1, QItemSelectionModel::Clear);
    }
    else for (int b{ 0 }; b < list->count(); ++b)
    {
        Block* block{ extractPointer<Block>(list->item(b)) };
        if (block == currentBlock)
        {
            if (list->currentRow() != b)
                list->setCurrentRow(b);

            break;
        }
    }

    updateButtons();
}

void BlocksPanel::handleBlockListItemSelectionChanged()
{
    if (blockList_->selectedItems().isEmpty())
        EM->SetCurrentBlock(nullptr);
}

void BlocksPanel::editBlock()
{
    Vector<Block*> blocks{};

    if (!addBlockButton_->hasFocus())
    {
        for (QListWidgetItem* item: blockList_->selectedItems())
            blocks.Push(extractPointer<Block>(item));
    }

    Block* currentBlock{ EM->GetCurrentBlock() };
    BlockDialog* blockDialog{ new BlockDialog{ context_, blocks, this } };
    blockDialog->deleteLater();
    EM->SetCurrentBlock(currentBlock);
}

void BlocksPanel::removeBlock()
{
    int row{ blockList_->currentRow() };

    Vector<Block*> blocks{};
    for (QListWidgetItem* item: blockList_->selectedItems())
    {
        Block* block{ extractPointer<Block>(item) };
        blocks.Push(block);

        const int itemRow{ blockList_->row(item) };
        if (itemRow < row)
            row = itemRow;
    }

    QString infoText{};
    for (unsigned b{ 0u }; b < blocks.Size(); ++b)
    {
        Block* block{ blocks.At(b) };
        infoText.append(block->name().CString());
        if (b < blocks.Size() - 1)
            infoText.append(", ");
    }

    if (!(QApplication::keyboardModifiers() & Qt::ShiftModifier))
    {
        QMessageBox* confirm{ new QMessageBox{ this } };
        confirm->setWindowTitle(QString{ "Remove block" } + (blocks.Size() > 1 ? "s" : ""));
        confirm->setWindowIcon(QIcon(":/Edddy"));
        confirm->setIcon(QMessageBox::Warning);
        confirm->setText(QString{ "Are you sure you want to delete "} + (blocks.Size() == 1 ? "this block?" : "these blocks?"));
        confirm->setInformativeText(infoText);
        confirm->setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
        confirm->setDefaultButton(QMessageBox::Cancel);

        if (confirm->exec() == QMessageBox::Cancel)
            return;
    }

    Blockset* currentSet{ EM->GetCurrentBlockset() };

    for (Block* block: blocks)
    {
        for (BlockMap* map: EM->GetBlockMaps())
        {
            for (BlockInstance* instance: map->GetInstancesOfBlock(block))
                instance->SetBlock(nullptr, Quaternion::IDENTITY);
        }

        currentSet->RemoveBlock(block);
    }

    handleCurrentBlocksetChanged(currentSet);

    if (row < blockList_->count())
        blockList_->setCurrentRow(row);
    else
        blockList_->setCurrentRow(blockList_->count() - 1);
}

void BlocksPanel::updateBlocksetList()
{
    Blockset* currentSet{ EM->GetCurrentBlockset() };
    const Vector<Blockset*>& blocksets{ EM->GetBlocksets() };
    Project* project{ MC->CurrentProject() };

    blocksetList_->clear();

    for (Blockset* bs: blocksets)
    {
        QString name{ bs->name_.CString() };
        QListWidgetItem* listItem{ new QListWidgetItem{ blocksetList_ } };

        listItem->setIcon(QIcon{ ":/Blockset" });
        listItem->setText(bs->DisplayName());
        listItem->setData(StringRole, name);
        listItem->setData(PointerRole, { QMetaType::QObjectStar, &bs });
        listItem->setFlags(listItem->flags() | Qt::ItemIsEditable);

        if (FS->FileExists(bs->savedFullPath_))
            listItem->setToolTip(DQ(project->localizedPath(bs->savedFullPath_)));
        else
            listItem->setToolTip("Unsaved blockset");

        blocksetList_->addItem(listItem);
        if (currentSet == bs)
            blocksetList_->setCurrentItem(listItem);
    }

    updateButtons();
}

void BlocksPanel::handleBlocksetListItemChanged(QListWidgetItem* item)
{
    QString newText{ item->text() };

    if (newText.right(1) == "*")
        newText.chop(1);

    if (newText == item->data(StringRole).toString()) // No need for validation
        return;

    auto restoreText = [=](){ item->setText(item->data(StringRole).toString()); };

    if (newText.contains('.'))
    {
#if QT_VERSION < QT_VERSION_CHECK(5, 15, 0)
        const QStringList splitResults{ newText.split('.', QString::SkipEmptyParts) };
#else
        const QStringList splitResults{ newText.split('.', Qt::SkipEmptyParts) };
#endif

        if (!splitResults.isEmpty())
        {
            newText = splitResults.first();
        }
        else
        {
            restoreText();
            return;
        }
    }

    newText.replace('/', "");
    newText.replace('*', "");

    if (newText.isEmpty())
    {
        restoreText();
        return;
    }

    newText.append(".ebs");

    if (item->data(StringRole).toString() != newText)
    {
        QListWidget* listWidget{ item->listWidget() };

        for (int i{ 0 }; i < listWidget->count(); ++i)
        {
            QListWidgetItem* otherItem{ listWidget->item(i) };
            const QString otherItemText{ otherItem->data(StringRole).toString() };

            if (item != otherItem && otherItemText == newText)
            {
                restoreText();
                return;
            }
        }

        item->setData(StringRole, newText);

        Blockset* bs{ extractPointer<Blockset>(item) };
        if (bs)
        {
            bs->setName(newText);
            newText = bs->DisplayName();
        }
    }

    if (item->text() != newText)
        item->setText(newText);
}

void BlocksPanel::updateButtons()
{
    bool currentBlockset{ !blocksetList_->selectedItems().isEmpty() };
    bool currentBlock{ blockList_->currentItem() != nullptr };

    for (QPushButton* button: { deleteBlocksetButton_, addBlockButton_ })
        button->setEnabled(currentBlockset);

    for (QPushButton* button: { previousBlocksetButton_, nextBlocksetButton_ })
        button->setEnabled(blocksetList_->count() > 0);

    for (QPushButton* button: { editBlockButton_, removeBlockButton_ })
        button->setEnabled(currentBlock);
}

void BlocksPanel::handleBlocksetListCurrentItemChanged()
{
    QListWidgetItem* item{ blocksetList_->selectedItems().isEmpty() ? nullptr : blocksetList_->currentItem() };

    if (!item)
        EM->SetCurrentBlockset(nullptr);
    else
        EM->SetCurrentBlockset(extractPointer<Blockset>(item));

    updateButtons();
}

void BlocksPanel::handleZoomSliderValueChanged(int value)
{
    if (value == -1)
        value = zoomSlider_->value();

    const QSize gridSize{ value + value / 8, value + 16 };
    const QSize iconSize{ value, value };
    blockList_->setGridSize(gridSize);
    blockList_->setIconSize(iconSize);

    for (int b{ 0 }; b < blockList_->count(); ++b)
        blockList_->item(b)->setSizeHint(gridSize);
}

int BlocksPanel::rotationSliderValue()
{
    return rotationSlider_->value();
}

void BlocksPanel::updateBlockPreviews()
{
    for (int b{ 0 }; b < blockList_->count(); ++b)
        static_cast<BlockListItem*>(blockList_->item(b))->UpdateBlockPreview();
}

void BlocksPanel::nextBlockset()
{
    const int row{ (blocksetList_->currentRow() + 1) % blocksetList_->count() };
    blocksetList_->setCurrentRow(row);
}

void BlocksPanel::previousBlockset()
{
    const int rowCount{ blocksetList_->count() };
    const int row{ (blocksetList_->currentRow() - 1 + rowCount) % rowCount };
    blocksetList_->setCurrentRow(row);
}

void BlocksPanel::resizeEvent(QResizeEvent* /*event*/)
{
    if ((width() * 1.f / height()) < 1.f)
    {
        splitter_->setOrientation(Qt::Vertical);
        blockList_->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        blockList_->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        blockList_->setFlow(QListWidget::LeftToRight);
    }
    else
    {
        splitter_->setOrientation(Qt::Horizontal);
        blockList_->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        blockList_->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        blockList_->setFlow(QListWidget::TopToBottom);
    }
}
