/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MAPPANEL_H
#define MAPPANEL_H

#include <QHBoxLayout>
#include <QFormLayout>

#include <QWidget>
#include <QLineEdit>
#include <QSpinBox>

#include "../drywidget.h"

class MapPanel: public DryWidget
{
    Q_OBJECT
    DRY_OBJECT(MapPanel, DryWidget);

public:
    explicit MapPanel(Context* context, QWidget* parent = nullptr);
    void updateInfo();
    void updateAttributeInfo(const String& name);

private slots:
    void resetMapName();
    void handleResizeMapActionTriggered();
    void handleMapNameEditingFinished();
    void updateAttributeInfo();
    void syncAttribute(const Variant& variant);

private:
    void updateSizeInfo();
    QHBoxLayout* CreateMapSizeRow();

    QLineEdit* nameEdit_;
    QSpinBox* widthBox_;
    QSpinBox* heightBox_;
    QSpinBox* depthBox_;

    QAction* resizeMapAction_;

    QFormLayout* attributesForm_;
};

#endif // MAPPANEL_H
