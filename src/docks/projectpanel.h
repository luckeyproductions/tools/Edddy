/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef PROJECTPANEL_H
#define PROJECTPANEL_H

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>
#include <QLineEdit>

#include "../drywidget.h"

class ProjectPanel: public DryWidget
{
    Q_OBJECT
    DRY_OBJECT(ProjectPanel, DryWidget)

public:
    explicit ProjectPanel(Context* context, QWidget* parent = nullptr);

    void updatePanel(bool browser = true);
    void updateBrowser();
    void focusBrowser();
    bool isShowingAll() const;

private slots:
    void onNameButtonClicked();
    void onCommandEditButtonClicked();
    void onFolderEditFinished();
    void onFolderResetClicked();
    void onMapAttributesButtonClicked();

    void resourceBrowserItemActivated(QTreeWidgetItem* item);
    void showBrowserMenu(const QPoint& pos);

    void editBrowserItem();
    void deleteSelectedFile();
    void onFolderPickClicked();

    void updateFolderIcon(QTreeWidgetItem* item);
    void hideFolders(QList<QTreeWidgetItem*> items);
    void unhideFolders();
    void updateFolderVisibility();

    void resizeEvent(QResizeEvent* event) override;

private:
    void buildTree(QTreeWidgetItem* treeItem);
    void storeExpandedFolders(StringVector& expanded);
    void collectChildren(QList<QTreeWidgetItem*>& children, QTreeWidgetItem* item, bool recursive) const;
    QList<QTreeWidgetItem*> allItems(bool onlyTopLevel = false) const;
    QStringList expandedPaths() const;

    QPushButton* projectNameButton_;
    QLineEdit* commandEdit_;
    QLineEdit* resourcesEdit_;
    QPushButton* resourcesResetButton_;

    QVBoxLayout* mainLayout_;
    QFrame* line_;
    QVBoxLayout* bottomHalf_;

    QTreeWidget* resourceBrowser_;
    QAction* showAllAction_;
    const QList<unsigned> editableTypeHashes_;
};

#endif // PROJECTPANEL_H
