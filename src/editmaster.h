/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef EDITMASTER_H
#define EDITMASTER_H

#include "luckey.h"

#define EM GetSubsystem<EditMaster>()

DRY_EVENT(E_CURRENTBLOCKCHANGED, CurrentBlockChanged)
{
    DRY_PARAM(P_BLOCK, Block*);
}

DRY_EVENT(E_CURRENTMAPCHANGED, CurrentMapChanged)
{
    DRY_PARAM(P_MAP, BlockMap*);
}

DRY_EVENT(E_CURRENTTOOLCHANGED, CurrentToolChanged)
{
    DRY_PARAM(P_TOOL, Tool*);
}

class EdddyCursor;
class Block;
class Blockset;
class BlockInstance;
class Tool;

struct BlockChange{
    BlockInstance* instance_;
    Pair<Block*, Block*> blockChange_;
    Pair<Quaternion, Quaternion> blockRotation_;
};

typedef Vector<BlockChange> UndoStep;

class EditMaster: public QObject, public Object
{
    Q_OBJECT
    DRY_OBJECT(EditMaster, Object);

public:
    EditMaster(Context* context);

    QString validateMapName(const QString& name) const;
    bool checkMapNameConflict(const QString& fileName) const;

    EdddyCursor* GetCursor() const { return cursor_; }
    BlockMap* NewMap(const IntVector3& mapSize, const Vector3& blockSize, const String& name = "");
    BlockMap* LoadMap(const String& fileName);

    Blockset* NewBlockset(const String& name);
    Blockset* LoadBlockset(const String& fileName);
    Blockset* GetCurrentBlockset() const { return currentBlockset_; }
    Blockset* getBlocksetByIndex(unsigned index) const { return blocksets_.At(index); }

    void SetActiveBlockMap(BlockMap* map);
    void SetActiveBlockMap(int index);
    BlockMap* GetActiveBlockMap() const { return activeBlockMap_; }
    BlockMap* GetBlockMap(int index);
    const Vector<BlockMap*>& GetBlockMaps() const { return blockMaps_; }

    unsigned activeLayer() const { return activeLayer_; }
    void setActiveLayer(unsigned layer);

    void SetCurrentBlockset(Blockset* blockSet);
    const Vector<Blockset*>& GetBlocksets() const { return blocksets_; }
    unsigned globalBlocksetIndex(Block* block);

    void NextBlock();
    void PreviousBlock();
    void SetCurrentBlock(unsigned index, Blockset* blockSet);
    void SetCurrentBlock(unsigned index);
    void SetCurrentBlock(Block* block);

    Block* GetBlock(unsigned index) const;
    Block* GetCurrentBlock() const;
    void PutBlock(const IntVector3& coords, const Quaternion& rotation, Block* block);
    void PutBlock(const IntVector3& coords);
    void PutBlock();
    void PickBlock();
    void ClearBlock(const IntVector3& coords);
    void ClearBlock();

    void SetTool(Tool* tool);
    Tool* GetTool() const { return currentTool_; }
    StringHash GetLastToolType() const { return lastUsedTool_; }

    void ApplyTool(bool shiftDown, bool ctrlDown, bool altDown);
    void ReleaseTool();
    void Undo();
    void Redo();

    void OpenMap(BlockMap* map);
    
    bool DeleteBlockMap(const String& filename);
    void DeleteBlockMap(BlockMap* bm);
    bool DeleteBlockset(const String& filename);
    void DeleteBlockset(Blockset* bs);

    bool digging() const;

signals:
    void activeLayerChanged(unsigned layer);

public slots:
    void NextBlockset();
    void PreviousBlockset();

    void pickLayer();

    void forceToolPreviewUpdate();

private slots:
    void nextMap();
    void previousMap();

private:
    void CreateTools();

    EdddyCursor* cursor_;
    Vector<BlockMap*> blockMaps_;
    BlockMap* activeBlockMap_;
    unsigned activeLayer_;

    Vector<Blockset*> blocksets_;
    int currentBlockIndex_;
    Block* currentBlock_;
    int currentBlocksetIndex_;
    Blockset* currentBlockset_;
    Tool* currentTool_;
    StringHash lastUsedTool_;
};

#endif // EDITMASTER_H
