/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef BLOCKMAP_H
#define BLOCKMAP_H

#include <QUndoStack>
#include <map>

#include "../project.h"
#include "freeblock.h"
#include "heightprofile.h"

#define NUM_LAYERS 5

class Block;
class GridBlock;
class View3D;
class Centers;
class TileInstance;

typedef HashMap<IntVector2, GridBlock*> Sheet;
typedef HashMap<int, Sheet> GridMap;

class BlockMap: public QObject, public Component
{
    Q_OBJECT
    DRY_OBJECT(BlockMap, Component)

    friend class EditMaster;

public:
    BlockMap(Context* context);

    void OnNodeSet(Node* node) override;
    bool Save();
    String SaveTemp(const String& tempPath, const PODVector<Blockset*>& skip = {}) const;
    bool LoadXML(const XMLElement& source) override;
    bool SaveXML(XMLElement& dest) const;
    void UpdateViews() const;

    bool resetName()
    {
        if (name_ == savedName_ || savedName_.IsEmpty())
            return false;

        name_ = savedName_;
        return true;
    }

    void setName(const String& name)
    {
        name_ = name;

        if (!fullPath_.IsEmpty() && fullPath_.Contains('/'))
            fullPath_ = fullPath_.Substring(0, fullPath_.FindLast('/') + 1).Append(name_);
        else
            fullPath_ = name_;
    }

    String name() const { return name_; }
    QString displayName() const;
    String shortName() const { return name_.Substring(0, name_.Length() - 4); }
    String fullPath() const { return fullPath_; }
    String savedFullPath() const { return savedFullPath_; }

    Scene* GetScene() const { return scene_; }
    const QColor GetBackgroundQColor() const;

    void SetMapSize(int w, int h, int d);
    void SetMapSize(const IntVector3& size);
    void SetBlockSize(const Vector3& size);

    const IntVector3& GetMapSize() const { return mapSize_; }
    int GetMapWidth()       const { return mapSize_.x_; }
    int GetMapHeight()      const { return mapSize_.y_; }
    int GetMapDepth()       const { return mapSize_.z_; }

    const Vector3& GetBlockSize() const { return blockSize_; }
    float GetBlockWidth()  const { return blockSize_.x_; }
    float GetBlockHeight() const { return blockSize_.y_; }
    float GetBlockDepth()  const { return blockSize_.z_; }

    Vector3 coordsToPosition(const IntVector3& coords) const { return { coords * blockSize_ }; }

    void setAttributes(const MapAttributes& mapAttributes);
    const MapAttributes& attributes() const { return attributes_; }

    Vector3 GetCenter() const;
    HashSet<IntVector3> GetAllCoords() const;
    HashSet<IntVector3> GetTileCornersCoords() const;
    std::map<TileInstance*, HashSet<IntVector3> > GetTilesAndCorners() const;

    PODVector<TileInstance*> GetTileInstances(const IntVector3& coords, unsigned layer = M_MAX_UNSIGNED);
    BlockInstance* GetBlockInstance(const IntVector3& coords, unsigned layer) const;
//    BlockInstance* GetBlockInstance(const Ray& ray) { return nullptr; }
    Vector<BlockInstance*> GetInstancesOfBlock(Block* block) const;
    unsigned blocksetID(Block* block) const;

    void SetGridBlock(const IntVector3& coords, const Quaternion& rotation, unsigned layer, Block* block);
    void SetProfile(const IntVector3& coords, unsigned layer, const HeightProfile& elevation);
    Vector<Blockset*> GetUsedBlocksets() const;
    Blockset* GetBlocksetByIndex(unsigned index) const { return GetUsedBlocksets().At(index); }
    Vector<GridBlock*> GetOccupiedGridBlocks() const;
    bool Contains(const IntVector3& coords) const;

    QUndoStack& undoStack() { return undoStack_; }
    bool isModified() const;
    StaticModelGroup* GetActiveCenterGroup()   const { return activeCenterGroup_; }
    StaticModelGroup* GetInactiveCenterGroup() const { return inactiveCenterGroup_; }
    StaticModelGroup* GetSideGroup()           const { return sideGroup_; }

    void updateCenters();

private:
    void saveAttributes(XMLElement& dest) const;
    void loadAttributes(const XMLElement& source);

    void CreateCorners();
    int  GetBlocksetId(Block* block, HashMap<unsigned, Blockset*>& blockSetsById) const;
    void UpdateCorners();

    void updateCount(Block* oldBlock, Block* newBlock);

    String name_;
    String savedName_;
    String fullPath_;
    String savedFullPath_;

    MapAttributes attributes_;

    StaticModelGroup* activeCenterGroup_;
    StaticModelGroup* inactiveCenterGroup_;
    StaticModelGroup* sideGroup_;
    Centers* centerBillboards_;

    HashMap<int, GridMap> gridLayers_;

    Scene* scene_;
    Zone*  zone_;
    PODVector<Node*> corners_;

    IntVector3 mapSize_;
    Vector3 blockSize_;

    QUndoStack undoStack_;

    std::map<Blockset*, unsigned> blocksetUseCount_;
};

#endif // BLOCKMAP_H
