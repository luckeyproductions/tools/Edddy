/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef GRIDBLOCK_H
#define GRIDBLOCK_H

#include "blockinstance.h"

class GridBlock: public BlockInstance
{
    DRY_OBJECT(GridBlock, BlockInstance);

public:
    static bool withinLock(const IntVector3& coords);

    GridBlock(Context* context);

    void Init(const IntVector3& coords, unsigned layer = 0u);

    const IntVector3& GetCoords() const { return coords_; }
    bool withinLock() const;

    virtual BlockType type() const { return BLOCK_BLOCK; }

private:
    void SetCoords(const IntVector3& coords);

    IntVector3 coords_;
};

#endif // GRIDBLOCK_H
