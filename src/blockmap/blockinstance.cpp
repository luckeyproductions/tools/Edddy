/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "blockmap.h"

#include "blockinstance.h"

BlockInstance::BlockInstance(Context* context): Component(context),
    blockMap_{ nullptr },
    block_{ nullptr },
    layer_{ 0u }
{
}

void BlockInstance::OnNodeSet(Node *node)
{
    if (!node)
        return;

    node_->SetTags({ "Block" });

    blockMap_ = node_->GetParent()->GetComponent<BlockMap>();
    blockNode_ = node_->CreateChild("BLOCK");
    blockModel_ = blockNode_->CreateComponent<StaticModel>();
}

void BlockInstance::SetBlock(Block* block, Quaternion rotation)
{
    blockNode_->SetRotation(rotation);

    if (block)
    {
        if (block_ != block)
        {
            UnsubscribeFromEvent(block_, E_BLOCKMODIFIED);
            SubscribeToEvent(block, E_BLOCKMODIFIED, DRY_HANDLER(BlockInstance, update));

            block_ = block;
        }
    }
    else
    {
        block_ = nullptr;
        UnsubscribeFromEvent(E_BLOCKMODIFIED);
    }

    update();
}

void BlockInstance::update(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    update();
}

void BlockInstance::update()
{
    if (!block_)
    {
        blockModel_->SetModel(nullptr);
        return;
    }

    blockModel_->SetModel(block_->model());

    if (block_->material())
        blockModel_->SetMaterial(block_->material());
    else for (unsigned i{ 0u }; i < block_->numGeometries(); ++i)
        blockModel_->SetMaterial(i, block_->material(i));
}
