/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef BLOCKINSTANCE_H
#define BLOCKINSTANCE_H

#include "block.h"

class BlockInstance: public Component
{
    DRY_OBJECT(BlockInstance, Component);

public:
    BlockInstance(Context* context);

    void OnNodeSet(Node* node) override;
    void SetBlock(Block* block, Quaternion rotation);
    Block* GetBlock() const { return block_; }
    Quaternion GetRotation() const { return blockNode_->GetRotation(); }
    unsigned GetLayer() const { return layer_; }
    Model* model() const { return blockModel_->GetModel(); }

    void update(StringHash eventType, VariantMap& eventData);

protected:
    virtual void update();

    BlockMap* blockMap_;

    Block* block_;
    Node* blockNode_;
    StaticModel* blockModel_;
    unsigned layer_;

};

#endif // BLOCKINSTANCE_H
