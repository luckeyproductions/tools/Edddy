/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../editmaster.h"
#include "../docks/blockspanel.h"
#include "blockset.h"

#include "block.h"

void Block::RegisterAttributes(Context* context)
{
    DRY_ATTRIBUTE("id", unsigned, id_, 0, AM_FILE);
    DRY_ATTRIBUTE("name", String, name_, "", AM_FILE);
//    DRY_ATTRIBUTE("material", String, material_, "", AM_FILE);
}

Block::Block(Context* context, Blockset* blockset): QObject(), Serializable(context),
    blockset_{ blockset },
    id_{ NOID },
    name_{ "" },
    materials_{},
    previewScene_{ new Scene{ context } },
    previewNode_{},
    previewModel_{},
    image_{},
    renderTexture_{ new Texture2D{ context } }
{
    createPreviewRenderer();
}

void Block::setModel(Model* model)
{
    previewModel_->SetModel(model);

    const BoundingBox bb{ previewModel_->GetBoundingBox() };
    const PODVector<float> bbSize{ (bb.Size() * Vector3{ 1.f, .8f, 1.f }).Data(), 3u };
    const float scale{ 1.f / Max(bbSize.Begin(), bbSize.End()) };
    previewNode_->SetScale(scale);
    previewModel_->GetNode()->SetPosition(-bb.Center());

    applyMaterials();
}

void Block::setMaterial(Material* material)
{
    setMaterial(SINGLE, material);
}

void Block::setMaterial(unsigned index, Material* material)
{
    if (index == SINGLE)
    {
        materials_.Clear();
    }
    else
    {
        if (index >= numGeometries())
        {
            return;
        }
        else if (materials_.Contains(SINGLE))
        {
            for (unsigned m{ 0 }; m < numGeometries(); ++m)
                materials_[m] = materials_[SINGLE];

            materials_.Erase(SINGLE);
        }
    }
    
    if (material)
        materials_[index] = material->GetName();
    else
        materials_.Erase(index);

    applyMaterials();
}

void Block::setMaterials(const Materials& materials)
{
    if (materials == materials_)
        return;

    materials_ = materials;
    applyMaterials();
}

Model* Block::model() const
{
    return previewModel_->GetModel();
}

String Block::modelName() const
{
    if (model())
        return model()->GetName();
    else
        return "";
}
unsigned Block::numGeometries() const
{
    if (model())
        return model()->GetNumGeometries();
    else
        return 0;
}

void Block::applyMaterials()
{
    if (materials_.Contains(SINGLE))
    {
        previewModel_->SetMaterial(material(SINGLE).Get());
    }
    else for (unsigned g{ 0u }; g < numGeometries(); ++g)
    {
        if (materials_.Contains(g))
            previewModel_->SetMaterial(g, material(g));
        else
            previewModel_->SetMaterial(g, MAT_NONE);
    }

    updatePreview();
}

Material* Block::material() const
{
    return material(SINGLE);
}

SharedPtr<Material> Block::material(unsigned index) const
{
    if (materials_.Contains(index))
    {
        ResourceCache* cache{ GetSubsystem<ResourceCache>() };
        const String materialName{ *materials_[index] };
        if (!materialName.IsEmpty())
            return SharedPtr<Material>(cache->GetResource<Material>(materialName));
        else
            return nullptr;
    }
    else
    {
        return nullptr;
    }
}

Image* Block::image()
{
    if (!renderTexture_)
        return nullptr;

    image_ = renderTexture_->GetImage();
    return image_;
}

bool Block::SaveXML(XMLElement& dest) const
{
    XMLElement blockElem{ dest.CreateChild("block") };
    blockElem.SetUInt("id", id_);
    blockElem.SetString("name", name_);
    blockElem.SetString("model", modelName());

    if (materials_.Size())
    {
        if (materials_.Keys().Front() == SINGLE)
        {
            if (!materials_.Values().Front().IsEmpty())
                blockElem.SetString("material", materials_.Values().Front());
        }
        else
        {
            for (const Materials::KeyValue& mat: materials_)
            {
                if (mat.second_.IsEmpty())
                    continue;

                XMLElement materialsElement{ blockElem.CreateChild("material") };
                materialsElement.SetUInt("index", mat.first_);
                materialsElement.SetString("name", mat.second_);
            }
        }
    }

    return true;
}

void Block::createPreviewRenderer()
{
    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    previewScene_->CreateComponent<Octree>();

    Node* lightNode{ previewScene_->CreateChild("Light") };
    lightNode->SetPosition({ -.5f, 5.f, -2.f });
    lightNode->LookAt(Vector3::ZERO);
    Light* previewLight{ lightNode->CreateComponent<Light>() };
    previewLight->SetLightType(LIGHT_DIRECTIONAL);

    Node* cameraNode{ previewScene_->CreateChild("Camera") };
    cameraNode->SetPosition({ 0.f, 2.f, -3.f });
    cameraNode->LookAt(Vector3::ZERO);
    Camera* previewCamera{ cameraNode->CreateComponent<Camera>() };
    previewCamera->SetFov(23.f);

    previewNode_ = previewScene_->CreateChild("Preview");
    previewModel_ = previewNode_->CreateChild("Graphics")->CreateComponent<StaticModel>();

    renderTexture_->SetSize(128, 128, Graphics::GetRGBAFormat(), TEXTURE_RENDERTARGET);
    RenderSurface* renderSurface{ renderTexture_->GetRenderSurface() };
    renderSurface->SetUpdateMode(SURFACE_MANUALUPDATE);
    RenderPath* renderPath{ new RenderPath() };
    renderPath->Load(cache->GetResource<XMLFile>("RenderPaths/ForwardTransparent.xml"));
    SharedPtr<Viewport> viewport{ new Viewport(context_, previewScene_, previewCamera, renderPath)};
    renderSurface->SetViewport(0, viewport);
}

void Block::updatePreview()
{
    previewNode_->SetRotation({ 1.f * GetSubsystem<BlocksPanel>()->rotationSliderValue(), Vector3::DOWN });

    renderTexture_->GetRenderSurface()->QueueUpdate();
    MC->RequestUpdate();
}
