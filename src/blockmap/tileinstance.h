/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef TILEINSTANCE_H
#define TILEINSTANCE_H

#include "heightprofile.h"
#include "../cyberplasm/witch.h"

#include "gridblock.h"

class TileInstance: public GridBlock
{
    DRY_OBJECT(TileInstance, GridBlock);

public:
    TileInstance(Context* context);

    BlockType type() const override { return BLOCK_TILE; }

    void setProfile(const HeightProfile& profile)
    {
        profile_ = profile;
        update();
    }

    const HeightProfile& heightProfile() const { return profile_; }
    HashSet<IntVector3> cornerCoordinates() const;
    IntVector3 cornerCoordinate(int index) const;
    Witch::Rune cornerRune(int index, bool global) const;

    int indexFromCornerCoords(const IntVector3& coords);

    bool isWithinBounds(const IntVector3& coords);

protected:
    void update() override;

private:
    void DrawNormals(StringHash eventType, VariantMap& eventData);
    void generateTile();

    HeightProfile profile_;
};

#endif // TILEINSTANCE_H
