/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QPainter>
#include "blockmap/block.h"
#include "blocklistitem.h"

BlockListItem::BlockListItem(Context* context, Block* block): QListWidgetItem(), Object(context),
    block_{ block }
{
    setTextAlignment(Qt::AlignCenter);
    setText(block_->name().CString());
    setToolTip(text());
    setData(ID, block_->id());
    setData(PointerRole, QVariant(QMetaType::QObjectStar, &block));

    UpdateBlockPreview();
}

void BlockListItem::UpdateBlockPreview()
{
    block_->updatePreview();

    SubscribeToEvent(E_ENDRENDERING, DRY_HANDLER(BlockListItem, UpdateIcon));
}

void BlockListItem::UpdateIcon(const StringHash, VariantMap&)
{
    Image* image{ block_->image() };

    if (!image)
        return;

    setIcon(ImageToIcon(image));

    UnsubscribeFromEvent(E_ENDRENDERING);
}

QIcon BlockListItem::ImageToIcon(Image* image)
{
    QImage qImage{ image->GetData(),
                   image->GetWidth(),
                   image->GetHeight(),
                   QImage::Format_RGBA8888};

    QPixmap pixmap{ qImage.size() };
    pixmap.fill(Qt::transparent);

    if (!qImage.width())
        return pixmap;

    QPainter{ &pixmap }.drawImage(QPoint(), qImage);

    return { pixmap };
}
