/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "mainwindow.h"
#include "view3d.h"
#include "inputmaster.h"
#include "edddycursor.h"
#include "blockmap/gridblock.h"
#include "blockmap/blockinstance.h"
#include "blockmap/tileinstance.h"
#include "blockmap/blockmap.h"
#include "blockmap/blockset.h"
#include "docks/blockspanel.h"
#include "docks/mappanel.h"
#include "docks/projectpanel.h"
#include "project.h"
#include "edddyevents.h"
#include "undostack/gridblockcommand.h"
#include "tools/brush.h"
#include "tools/fill.h"
#include "tools/dig.h"

#include "editmaster.h"

EditMaster::EditMaster(Context* context): QObject(), Object(context),
    cursor_{ nullptr },
    blockMaps_{},
    activeBlockMap_{ nullptr },
    activeLayer_{ 0u },
    currentBlockIndex_{ -1 },
    currentBlock_{ nullptr },
    currentBlocksetIndex_{ -1 },
    currentBlockset_{ nullptr },
    currentTool_{ nullptr },
    lastUsedTool_{}
{
    CreateTools();

    Node* cursorNode{ new Node{ context_ } };
    cursor_ = cursorNode->CreateComponent<EdddyCursor>();
}

QString EditMaster::validateMapName(const QString& name) const
{
    QString mapName{ name };

    if (mapName.contains('.'))
        mapName = mapName.split('.').first();

    if (mapName.contains('/'))
        mapName.replace('/', "");

    if (!mapName.isEmpty())
        mapName.append(".emp");

    return mapName;
}

bool EditMaster::checkMapNameConflict(const QString& fileName) const
{
    for (BlockMap* bm: GetBlockMaps()) ///Only includes open maps!
    {
        if (bm->fullPath().CString() == fileName)
            return true;
    }

    return false;
}

void EditMaster::CreateTools()
{
    new Brush{ context_ };
    new Fill{ context_ };
    new Dig{ context_ };
}

BlockMap* EditMaster::NewMap(const IntVector3& mapSize, const Vector3& blockSize, const String& name)
{
    BlockMap* newBlockMap{ (new Scene{ context_ })->CreateComponent<BlockMap>() };

    newBlockMap->setName(name);
    newBlockMap->SetBlockSize(blockSize);
    newBlockMap->SetMapSize(mapSize);

    blockMaps_.Push(newBlockMap);

    return newBlockMap;
}

BlockMap* EditMaster::LoadMap(const String& fullPath)
{
    BlockMap* map{ nullptr };
    const String name{ fullPath.Split('/').Back() };

    for (BlockMap* bm: blockMaps_)
    {
        if (fullPath == bm->savedFullPath_)
        {
            map = bm;
            break;
        }
    }

    if (!map)
    {
        ResourceCache* cache{ GetSubsystem<ResourceCache>() };
        XMLFile* mapXML{ cache->GetResource<XMLFile>(fullPath) };

        if (!mapXML)
            return nullptr;

        XMLElement rootElem{ mapXML->GetRoot("blockmap") };
        if (rootElem)
        {
            BlockMap* blockMap{ (new Scene{ context_ })->CreateComponent<BlockMap>() };
            blockMap->fullPath_ = fullPath;

            if (blockMap->LoadXML(rootElem))
            {
                blockMap->savedFullPath_ = blockMap->fullPath_;
                blockMap->savedName_ = blockMap->name_ = name;
                blockMap->undoStack().clear();
                blockMaps_.Push(blockMap);

                map = blockMap;
            }
        }
    }

    if (map)
        OpenMap(map);

    return map;
}

void EditMaster::OpenMap(BlockMap* map)
{
    for (View3D* view: View3D::views_)
        view->UpdateMapBox();

    View3D::Active()->SetMap(map);

    MainWindow::mainWindow_->updateMapActions();
}

Blockset* EditMaster::NewBlockset(const String& name)
{
    Blockset* newBlockset{ new Blockset{ context_ } };
    newBlockset->name_ = name;

    blocksets_.Push(newBlockset);
    GetSubsystem<BlocksPanel>()->updateBlocksetList();

    return newBlockset;
}

Blockset* EditMaster::LoadBlockset(const String& fullPath)
{
    for (Blockset* bs: blocksets_)
    {
        if (bs->savedFullPath_ == fullPath)
            return bs;
    }

    ResourceCache* cache{ GetSubsystem<ResourceCache>() };
    XMLFile* blockSetXML{ cache->GetResource<XMLFile>(fullPath) };
    if (!blockSetXML)
        return nullptr;

    XMLElement rootElem{ blockSetXML->GetRoot("blockset") };
    Blockset* newBlockset{ new Blockset{ context_ } };
    newBlockset->LoadXML(rootElem);
    newBlockset->savedFullPath_ = newBlockset->fullPath_ = fullPath;
    newBlockset->savedName_ = newBlockset->name_ = GetFileNameAndExtension(fullPath);

    blocksets_.Push(newBlockset);

    return newBlockset;
}

void EditMaster::SetActiveBlockMap(BlockMap* map)
{
    if (!map || activeBlockMap_ == map)
        return;

    activeBlockMap_ = map;
    lastUsedTool_ = {};

    GetSubsystem<MapPanel>()->updateInfo();
    GetSubsystem<MainWindow>()->setActiveUndoStack((map ? &map->undoStack() : nullptr));
}

BlockMap* EditMaster::GetBlockMap(int index)
{
    if (!blockMaps_.IsEmpty()
     && index >= 0
     && index < static_cast<int>(blockMaps_.Size()))
    {
        return blockMaps_.At(index);
    }
    else
    {
        return nullptr;
    }
}

void EditMaster::setActiveLayer(unsigned layer)
{
    if (activeLayer_ == layer)
        return;

    activeLayer_ = layer;
    emit activeLayerChanged(activeLayer_);
    cursor_->SendEvent(E_CURSORSTEP);
    forceToolPreviewUpdate();
}

void EditMaster::SetActiveBlockMap(int index)
{
    if (!blockMaps_.IsEmpty()
        && index >= 0
        && index < static_cast<int>(blockMaps_.Size()))
    {
        SetActiveBlockMap(blockMaps_.At(index));
    }
}

void EditMaster::SetCurrentBlockset(Blockset* blockSet)
{
    if (currentBlockset_ == blockSet)
        return;

    currentBlockset_ = blockSet;

    if (currentBlockset_)
        currentBlocksetIndex_ = blocksets_.IndexOf(currentBlockset_);
    else
        currentBlocksetIndex_ = -1;

    GetSubsystem<BlocksPanel>()->handleCurrentBlocksetChanged(currentBlockset_);
}

unsigned EditMaster::globalBlocksetIndex(Block* block)
{
    for (Blockset* blockset: blocksets_)
    {
        const Vector<Block*>& blocks{ blockset->blocks() };
        if (blocks.Contains(block))
            return blocksets_.IndexOf(blockset);
    }

    return M_MAX_UNSIGNED;
}

void EditMaster::NextBlockset()
{
    if (!blocksets_.Size())
        return;

    SetCurrentBlockset(blocksets_.At(++currentBlocksetIndex_ %= blocksets_.Size()));
}

void EditMaster::PreviousBlockset()
{
    if (!blocksets_.Size())
        return;

    if (currentBlocksetIndex_-- <= 0)
        currentBlocksetIndex_ = blocksets_.Size() - 1;

    SetCurrentBlockset(blocksets_.At(currentBlocksetIndex_));
}

void EditMaster::pickLayer()
{
    setActiveLayer(std::stoi(sender()->objectName().right(1).toStdString()));
}

void EditMaster::SetCurrentBlock(unsigned index, Blockset* blockSet)
{
    if (!blockSet)
    {
        SetCurrentBlock(nullptr);
        return;
    }

    SetCurrentBlockset(blockSet);

    if (index >= currentBlockset_->blocks().Size())
    {
        SetCurrentBlock(nullptr);
        return;
    }

    currentBlockIndex_ = index;
    currentBlock_ = currentBlockset_->blocks()[currentBlockIndex_];

    VariantMap eventData{};
    eventData[CurrentBlockChanged::P_BLOCK] = currentBlock_;

    SendEvent(E_CURRENTBLOCKCHANGED, eventData);
    GetSubsystem<BlocksPanel>()->updateBlockList();

    MC->RequestUpdate();
}

void EditMaster::SetCurrentBlock(unsigned index)
{
    SetCurrentBlock(index, currentBlockset_);
}

void EditMaster::SetCurrentBlock(Block* block)
{
    if (currentBlock_ == block)
        return;

    if (block == nullptr)
    {
        currentBlock_ = nullptr;

        VariantMap eventData{};
        eventData[CurrentBlockChanged::P_BLOCK] = currentBlock_;

        SendEvent(E_CURRENTBLOCKCHANGED, eventData);
        GetSubsystem<BlocksPanel>()->updateBlockList();
    }
    else
    {
        SetCurrentBlockset(block->blockset());
        SetCurrentBlock(currentBlockset_->blocks().IndexOf(block));
    }
}

void EditMaster::NextBlock()
{
    if (!currentBlockset_)
        return;

    SetCurrentBlock(++currentBlockIndex_ %= currentBlockset_->blocks().Size());
}

void EditMaster::PreviousBlock()
{
    if (!currentBlockset_)
        return;

    if (currentBlockIndex_-- <= 0)
        currentBlockIndex_ = currentBlockset_->blocks().Size() - 1;

    SetCurrentBlock(currentBlockIndex_);
}

Block* EditMaster::GetBlock(unsigned index) const
{
    if (index >= currentBlockset_->blocks().Size())
        return nullptr;
    else
        return currentBlockset_->blocks()[index];
}

Block* EditMaster::GetCurrentBlock() const
{
    return currentBlock_;
}

void EditMaster::PickBlock()
{
    EdddyCursor* cursor{ EdddyCursor::cursor_ };

    if (!GetActiveBlockMap() || cursor->IsHidden())
        return;

    BlockInstance* blockInstance{ GetActiveBlockMap()->GetBlockInstance(cursor->GetCoords(), activeLayer_) };
    if (blockInstance && blockInstance->GetBlock())
    {
        cursor->SetRotation(blockInstance->GetRotation());
        SetCurrentBlock(blockInstance->GetBlock());
    }
    else
    {
        SetCurrentBlock(nullptr);
    }

    //Clear last used tool
    lastUsedTool_ = {};
}

void EditMaster::PutBlock(const IntVector3& coords, const Quaternion& rotation, Block* block)
{
    if (!activeBlockMap_)
        return;

    GridBlockCommand* command{ new GridBlockCommand{ activeBlockMap_, coords, activeLayer_ } };
    command->setNewState(block, rotation);

    if (BlockInstance* instance{ activeBlockMap_->GetBlockInstance(coords, activeLayer_) })
    {
        GridBlock* gridBlock{ static_cast<GridBlock*>(instance) };
        if (gridBlock->type() == BLOCK_BLOCK)
            command->setOldState(gridBlock->GetBlock(), gridBlock->GetRotation());
        else if (gridBlock->type() == BLOCK_TILE)
            command->setOldState(gridBlock->GetBlock(), gridBlock->GetRotation(),
                                 gridBlock->Cast<TileInstance>()->heightProfile());
    }

    if (command->isChange())
    {
        activeBlockMap_->undoStack().push(command);
        activeBlockMap_->UpdateViews();
    }
    else
    {
        delete command;
    }
}

void EditMaster::PutBlock(const IntVector3& coords)
{
    EdddyCursor* cursor{ EdddyCursor::cursor_ };

    PutBlock(coords, cursor->GetRotation(), currentBlock_);
}

void EditMaster::PutBlock()
{
    EdddyCursor* cursor{ EdddyCursor::cursor_ };

    if (cursor->IsHidden())
        return;

    PutBlock(cursor->GetCoords());
}

void EditMaster::ClearBlock(const IntVector3& coords)
{
    PutBlock(coords, Quaternion::IDENTITY, nullptr);
}

void EditMaster::ClearBlock()
{
    ClearBlock(EdddyCursor::cursor_->GetCoords());
}

void EditMaster::SetTool(Tool* tool)
{
    if (!tool || tool == currentTool_)
        return;

    lastUsedTool_ = StringHash{};

    currentTool_ = tool;
    currentTool_->UpdatePreview();

    VariantMap eventData{};
    eventData[CurrentToolChanged::P_TOOL] = tool;
    SendEvent(E_CURRENTTOOLCHANGED, eventData);
    cursor_->SendEvent(E_CURSORSTEP);

    cursor_->GetBlockMap()->UpdateViews();
}

void EditMaster::ApplyTool(bool shiftDown, bool ctrlDown, bool altDown)
{
    EdddyCursor* cursor{ EdddyCursor::cursor_ };

    if (cursor->IsHidden() || !currentTool_ || !activeBlockMap_)
        return;

    currentTool_->Apply(shiftDown, ctrlDown, altDown);
    currentTool_->UpdatePreview(shiftDown, ctrlDown, altDown);

    lastUsedTool_ = currentTool_->GetType();
}

void EditMaster::ReleaseTool()
{
    currentTool_->Release();
}

void EditMaster::Undo()
{
    if (activeBlockMap_)
    {
        QUndoStack& stack{ activeBlockMap_->undoStack() };
        if (stack.canUndo())
        {
            stack.undo();
            forceToolPreviewUpdate();
        }
    }
}

void EditMaster::Redo()
{
    if (activeBlockMap_)
    {
        QUndoStack& stack{ activeBlockMap_->undoStack() };
        if (stack.canRedo())
        {
            stack.redo();
            forceToolPreviewUpdate();
        }
    }
}

void EditMaster::forceToolPreviewUpdate()
{
    if (currentTool_)
        currentTool_->UpdatePreview(true);

    activeBlockMap_->UpdateViews();
}

bool EditMaster::DeleteBlockMap(const String& filename)
{
    if (filename.IsEmpty())
        return false;

    for (BlockMap* bm: blockMaps_)
    {
        if (bm->savedFullPath() == filename)
        {
            DeleteBlockMap(bm);
            return FS->Delete(filename);
        }
    }

    return false;
}

void EditMaster::DeleteBlockMap(BlockMap* bm)
{
    blockMaps_.Remove(bm);

    if (bm->isModified())
        MainWindow::mainWindow_->updateWindowTitle();

    View3D::updateMapBoxes();
    delete bm;
}

bool EditMaster::DeleteBlockset(const String& filename)
{
    /// Should check maps for using this blockset first
    if (filename.IsEmpty())
        return false;

    for (Blockset* bs: blocksets_)
    {
        if (bs->savedFullPath_ == filename)
        {
            DeleteBlockset(bs);
            return FS->Delete(filename);
        }
    }

    return false;
}

void EditMaster::DeleteBlockset(Blockset* bs)
{
    /// Should check maps for using this blockset first
    const bool modified{ bs->isModified() };

    blocksets_.Remove(bs);
    GetSubsystem<BlocksPanel>()->updateBlocksetList();

    if (modified)
        MainWindow::mainWindow_->updateWindowTitle();

    delete bs;
}

bool EditMaster::digging() const
{
    return currentTool_ && currentTool_->GetType() == "Dig";
}

void EditMaster::nextMap()
{
    if (blockMaps_.Size() <= 1u)
        return;

    View3D* activeView{ View3D::Active() };
    if (activeView)
    {
        unsigned index{ blockMaps_.IndexOf(activeBlockMap_) };
        activeView->SetMap(blockMaps_.At(++index % blockMaps_.Size()));
    }
}

void EditMaster::previousMap()
{
    if (blockMaps_.Size() <= 1u)
        return;

    View3D* activeView{ View3D::Active() };
    if (activeView)
    {
        unsigned index{ blockMaps_.IndexOf(activeBlockMap_) + blockMaps_.Size()};
        activeView->SetMap(blockMaps_.At(--index % blockMaps_.Size()));
    }
}
