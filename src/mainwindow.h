/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "luckey.h"
#include "docks/drydockwidget.h"
#include "noprojectwidget.h"

#include <QMainWindow>
#include <QListWidget>
#include <QCheckBox>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QStackedWidget>
#include <QUndoView>

class Blockset;
class Block;

#define MW GetSubsystem<MainWindow>()


class MainWindow: public QMainWindow, public Object
{
    Q_OBJECT
    DRY_OBJECT(MainWindow, Object)

public:
    static MainWindow* mainWindow_;

    explicit MainWindow(Context* context);
    ~MainWindow();

    template <class T>
    DryDockWidget* createDockWidget(Qt::DockWidgetArea area)
    {
        T* widget{ new T{ context_ } };
        context_->RegisterSubsystem(widget);
        DryDockWidget* dockWidget{ new DryDockWidget{ widget, this } };

        if (area != Qt::NoDockWidgetArea)
            addDockWidget(area, dockWidget);

        return dockWidget;
    }

    void createLayerButtons();

    void setAxisLock(std::bitset<3> lock);
    void setPlayCommandEnabled(bool enabled);

    void loadSettings();
    void initialize();
    void handleCurrentProjectChanged();

    void updateMapActions();
    void updateWindowTitle();

    void setActiveUndoStack(QUndoStack* stack) { undoView_->setStack(stack); }
    QMenu* mapMenu() const { return menuMap_; }
    QAction* playAction() const { return actionPlay_; }

    QString getSaveFilename(const TypeInfo* typeInfo, const QString name = "");

public slots:
    void handleFileModifiedStateChange();
    void playLevel(bool game = false);
    void playGame() { playLevel(true); }

protected slots:
    void closeEvent(QCloseEvent* event) override;
    void changeEvent(QEvent* event) override;

private slots:
    void newProject();
    void openProject();
    void saveProject();

    void newMap();
    void saveMap();

    void undo();
    void redo();

    void pickBrush();
    void pickFill();
    void pickDig();
    void splitView();
    void actionLockAxisTriggered();

    void about();

    void newBlockset();
    void toggleFlyMode();
    void layerBoxClicked();
    void activeLayerChanged(unsigned layer);

private:
    void setupUi();
    void createAxisLockActions();
    void createMenuBar();
    void createToolBar();
    void createStatusBar();
    void createPanels();
    void createCentralStack();
    void connectSignals();
    void createUndoRedoActions();
    void createPickLayerActions();

    void retranslateUi();

//    void restoreGeometry();

    QVector<QCheckBox*> layerBoxes_;

    QStackedWidget* centralStackWidget_;
    NoProjectWidget* noProjectWidget_;
    QUndoView* undoView_;

    QMenuBar* menuBar_;
    QMenu* menuMap_;
    QMenu* menuFile_;
    QMenu* menuEdit_;
    QMenu* menuTools_;
    QMenu* menuHelp_;
    QToolBar* mainToolBar_;
    QStatusBar* statusBar_;

    QAction* actionQuit_;
    QAction* actionNewMap_;
    QAction* actionSaveMap_;
    QAction* actionUndo_;
    QAction* actionRedo_;
    QAction* actionBrush_;
    QAction* actionFill_;
    QAction* actionDig_;
    QAction* actionOpenProject_;
    QAction* actionLockX_;
    QAction* actionLockY_;
    QAction* actionLockZ_;
    QAction* actionAboutEdddy_;
    QAction* actionSaveProject_;
    QAction* actionSplitView_;
    QAction* actionNewProject_;
    QAction* actionFly_;
    QAction* actionPlay_;
};

#endif // MAINWINDOW_H
