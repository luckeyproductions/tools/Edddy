/* Edddy
// Copyright (C) 2016-2024 LucKey Productions (luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef CENTERS_H
#define CENTERS_H


#include "luckey.h"

class Centers: public Component
{
    DRY_OBJECT(Centers, Component)

public:
    Centers(Context* context);

    void UpdateBillboards();

protected:
    void OnSceneSet(Scene* scene) override;
    void OnNodeSet(Node* node) override;

    Vector<BillboardSet*> billboards_;
    BlockMap* blockMap_;

private:
    void HandleCursorStep(StringHash eventType, VariantMap& eventData);
};

#endif // CENTERS_H
